﻿using System.Globalization;
using Airlines.Persistence.Basic.Exceptions;
using Airlines.Persistence.Basic.Airports;
using Airlines.Persistence.Basic.Airports.Validation;
using Airlines.Persistence.Basic.Airlines;
using Airlines.OLD.Persistence.Basic.Flight;
using Airlines.Persistence.Aircrafts;
using Airlines.Persistence;

namespace FileReaders;
public class FileReader
{
    public static List<Airport> ReadAirportsFile(string path)
    {
        List<Airport> objects = [];

        try
        {
            if (!File.Exists(path))
            {
                throw new MissingFileException($"{path} not exist");
            }

            using StreamReader reader = new StreamReader(path);
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();

                if (!isHeader && line != null)
                {
                    var values = line.Split(',').Select(value => value.Trim()).ToArray();
                    if (values.Length == 4)
                    {
                        var airport = Airport.CreateAirport(values[0], values[1], values[2], values[3]);
                        objects.Add(airport);
                    }
                    else
                    {
                        throw new MissingFileException("Error: Line doesn't have enough values.");
                    }
                }
                isHeader = false;
            }
        }
        catch (MissingFileException ex)
        {
            Console.WriteLine($"Missing File! {ex.Message}");
        }

        return objects;
    }

    public static List<Airline> ReadAirlinesFile(string path)
    {
        List<Airline> objects = [];

        try
        {
            if (!File.Exists(path))
            {
                throw new MissingFileException($"{path} not exist");
            }

            using StreamReader reader = new StreamReader(path);
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();

                if (!isHeader && line != null)
                {
                    var airline = Airline.CreateAirline(line);
                    objects.Add(airline);
                }
                isHeader = false;
            }
        }
        catch (MissingFileException ex)
        {
            Console.WriteLine($"Missing File! {ex.Message}");
        }

        return objects;
    }

    public static Dictionary<string, List<object>> ReadAircraftsFile(string path)
    {
        List<CargoAircraft> cargoObjects = [];
        List<PassengerAircraft> passengerObjects = [];
        List<PrivateAircraft> privateObjects = [];
        Dictionary<string, List<object>> listOfAircrafts = [];

        try
        {

            if (!File.Exists(path))
            {
                throw new MissingFileException($"{path} not exist");
            }

            using StreamReader reader = new StreamReader(path);
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (!isHeader && line != null)
                {
                    var values = line.Split(',').Select(value => value.Trim()).ToArray();
                    var model = values[0];
                    var weight = values[1].Equals("-") ? 0 : double.Parse(values[1]);
                    var volume = values[2].Equals("-") ? 0 : double.Parse(values[2], NumberStyles.Any, CultureInfo.InvariantCulture);
                    var seats = values[3].Equals("-") ? 0 : int.Parse(values[3]); ;

                    if (weight > 0 && volume > 0 && seats == 0)
                    {
                        var newCargoAircraft = CargoAircraft.CreateCargoAircraft(model, weight, volume, "cargo");
                        cargoObjects.Add(newCargoAircraft);
                    }
                    else if (seats > 0 && volume > 0 && weight > 0)
                    {
                        var newPassengerAircraft = PassengerAircraft.CreatePassengerAircraft(model, weight, volume, seats, "passenger"); ;
                        passengerObjects.Add(newPassengerAircraft);
                    }
                    else if (weight == 0 && volume == 0 && seats > 0)
                    {
                        var newPrivateAircraft = PrivateAircraft.CreatePrivateAircraft(model, seats, "private");
                        privateObjects.Add(newPrivateAircraft);
                    }
                }
                isHeader = false;
            }
        }
        catch (MissingFileException ex)
        {
            Console.WriteLine($"Missing File! {ex.Message}");
        }

        listOfAircrafts.Add("cargo", cargoObjects.Cast<object>().ToList());
        listOfAircrafts.Add("passenger", passengerObjects.Cast<object>().ToList());
        listOfAircrafts.Add("private", privateObjects.Cast<object>().ToList());

        return listOfAircrafts;
    }

    public static List<Flight> ReadFlightsFile(string path)
    {
        List<Flight> objects = [];

        try
        {
            if (!File.Exists(path))
            {
                throw new MissingFileException($"{path} not exist");
            }
            using StreamReader reader = new StreamReader(path);
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (!isHeader && line != null)
                {
                    var values = line.Split(',').Select(value => value.Trim()).ToArray();
                    if (values.Length == 6)
                    {
                        var flight = Flight
                            .CreateFlight(values[0], values[1], values[2], values[3], int.Parse(values[4]), double.Parse(values[5], NumberStyles.Any, CultureInfo.InvariantCulture));
                        objects.Add(flight);
                    }
                    else
                    {
                        throw new MissingFileException("Error: Line doesn't have enough values.");
                    }
                }
                isHeader = false;
            }
        }
        catch (MissingFileException ex)
        {
            Console.WriteLine($"Missing File! {ex.Message}");
        }

        return objects;
    }

    public static Tree ReadFlightsRouteFile(string path, List<Flight> flights)
    {
        Tree newTree = new Tree();

        try
        {
            if (!File.Exists(path))
            {
                throw new MissingFileException($"{path} not exist");
            }
            using StreamReader reader = new StreamReader(path);
            bool isHeader = true;
            string start = string.Empty;
            string lastAdded = string.Empty;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (!isHeader && line != null)
                {
                    var values = line.Split(',').Select(value => value.Trim()).ToArray();
                    if (AirportValidator.AirportValidation(values[0]))
                    {
                        start = values[0];
                        newTree.Insert(values[0]);
                    }
                    else if (Tree.DoesFlightExist(flights, values[0]))
                    {
                        var airportArrival = Tree.GetArrivalAirport(flights, values[0]);
                        var airportDeparture = Tree.GetDepartureAirport(flights, values[0]);
                        if (airportArrival == start)
                        {
                            lastAdded = airportDeparture;
                            newTree.Insert(airportDeparture);
                        }
                        else if (lastAdded == airportArrival && newTree.Root != null)
                        {
                            foreach (var flight in newTree.Root.Children)
                            {
                                if (flight.Data == lastAdded)
                                {
                                    flight.Children.Add(new Node(airportDeparture));
                                }
                            }
                        }
                    }
                }
                isHeader = false;
            }
        }
        catch (MissingFileException ex)
        {
            Console.WriteLine($"Missing File! {ex.Message}");
        }

        return newTree;
    }

    internal static Graph ReadFlightsGraphFile(string path)
    {
        var graph = new Graph();
        try
        {
            if (!File.Exists(path))
            {
                throw new MissingFileException($"{path} not exist");
            }
            using StreamReader reader = new StreamReader(path);
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (!isHeader && line != null)
                {
                    var values = line.Split(',').Select(value => value.Trim()).ToArray();
                    if (values.Length == 6 && AirportValidator.AirportValidation(values[1]) && AirportValidator.AirportValidation(values[2]))
                    {
                        graph.AddEdge(values[1], values[2], int.Parse(values[4]), double.Parse(values[5], NumberStyles.Any, CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        throw new MissingFileException("Error: Line doesn't have enough values.");
                    }
                }
                isHeader = false;
            }
        }
        catch (MissingFileException ex)
        {
            Console.WriteLine($"Missing File! {ex.Message}");
        }
        return graph;
    }
}
