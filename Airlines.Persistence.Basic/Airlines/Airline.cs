﻿using Microsoft.IdentityModel.Tokens;

namespace Airlines.Persistence.Basic.Airlines;
public class Airline
{
    private string _name = string.Empty;

    private Airline(string name) => Name = name;


    public static Airline CreateAirline(string name) => new(name);

    public string Name
    {
        get => _name;
        set => _name = !value.IsNullOrEmpty() ? value : string.Empty;
    }

    public override string ToString() => Name;
}
