﻿using Microsoft.Extensions.Configuration;

namespace Airlines.Persistence.Basic.DataContext;

public class ConfigManager
{
    private static readonly IConfigurationRoot _configuration;
    static ConfigManager()
    {

        string basePath = Path.Combine(AppContext.BaseDirectory, "..", "..", "..", "..", "Airlines.Persistence.Basic");
        _configuration = new ConfigurationBuilder()
            .SetBasePath(basePath)
            .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
            .Build();
    }

    internal static string? GetConnectionString(string name)
        => _configuration.GetConnectionString(name);
}

