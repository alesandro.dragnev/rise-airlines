﻿#nullable disable
using Airlines.Persistence.Basic.Models;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Basic.DataContext;

public partial class RISEAirlinesContext : DbContext
{
    public RISEAirlinesContext()
    {
    }

    public RISEAirlinesContext(DbContextOptions<RISEAirlinesContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Airline> Airlines { get; set; }

    public virtual DbSet<Airport> Airports { get; set; }

    public virtual DbSet<Flight> Flights { get; set; }

    /*    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string key = ConfigManager.GetConnectionString("Local");
            _ = optionsBuilder.UseSqlServer(key);
        }*/


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        _ = modelBuilder.Entity<Airline>(entity =>
        {
            _ = entity.HasKey(e => e.ID).HasName("PK__Airlines__3214EC27EE80058D");
        });

        _ = modelBuilder.Entity<Airport>(entity =>
        {
            _ = entity.HasKey(e => e.ID).HasName("PK__Airports__3214EC2701D16EC5");
        });

        _ = modelBuilder.Entity<Flight>(entity =>
        {
            _ = entity.HasKey(e => e.ID).HasName("PK__Flights__3214EC2746E40DCD");

            _ = entity.HasOne(d => d.Arrival).WithMany(p => p.FlightArrivals).HasConstraintName("FK_ArrivalAirport");

            _ = entity.HasOne(d => d.Departure).WithMany(p => p.FlightDepartures).HasConstraintName("FK_DepartureAirport");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}