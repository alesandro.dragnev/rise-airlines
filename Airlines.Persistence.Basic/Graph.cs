﻿namespace Airlines.Persistence;

public class Vertex(string data)
{
    public string Data { get; set; } = data;
    public List<Edge> Children { get; set; } = [];
}

public class Edge(Vertex destination, double time, double price)
{
    public Vertex Destination { get; set; } = destination;
    public double Time { get; set; } = time;
    public double Price { get; set; } = price;
}

public class Graph
{
    public List<Vertex> _vertexList;

    public Graph() => _vertexList = [];

    public void AddEdge(string departure, string destination, double price, double time)
    {
        var newVertexDeparture = new Vertex(departure);
        var newVertexDestination = new Vertex(destination);

        Vertex? existingVertex = _vertexList.FirstOrDefault(v => v.Data == departure);
        if (existingVertex != null)
        {
            Edge? existingEdge = existingVertex.Children.FirstOrDefault(e => e.Destination.Data == destination);
            if (existingEdge != null)
            {
                existingEdge.Time = time;
                existingEdge.Price = price;
            }
            else
            {
                existingVertex.Children.Add(new Edge(newVertexDestination, time, price));
            }
        }
        else
        {
            newVertexDeparture.Children.Add(new Edge(newVertexDestination, time, price));
            _vertexList.Add(newVertexDeparture);
        }
    }
}
