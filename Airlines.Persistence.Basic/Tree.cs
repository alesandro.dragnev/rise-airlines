﻿using Airlines.OLD.Persistence.Basic.Flight;

namespace Airlines.Persistence;
public class Node(string item)
{
    public string Data = item;
    public List<Node> Children = [];
}

public class Tree
{
    public Node? Root;

    public Tree()
    {
    }

    public static bool IsDataValid(string data)
    {
        if (!string.IsNullOrEmpty(data) && data.Length > 0)
        {
            foreach (char c in data)
            {
                if (!char.IsLetter(c) && !char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    public void Insert(string data)
        => Root = IsDataValid(data) ? InsertRec(Root, data)
                  : InsertRec(Root, string.Empty);

    public static bool DoesFlightExist(List<Flight> flights, string flightID)
    {
        foreach (var flight in flights)
        {
            return flight.Id == flightID;
        }
        return false;
    }

    public static string GetArrivalAirport(List<Flight> flights, string flightID)
    {
        foreach (var flight in flights)
        {
            if (flight.Id == flightID)
            {
                return flight.DepartureAirport;
            };
        }
        return string.Empty;
    }

    public bool AlreadyInserted(string flightID)
    {
        if (Root != null)
        {
            foreach (var flight in Root.Children)
            {
                return flight.Data == flightID;
            }
        }
        return false;
    }

    public Node? GetNode(string flightID)
    {
        if (Root != null)
        {
            foreach (var flight in Root.Children)
            {
                if (flight.Data == flightID)
                {
                    return flight;
                };
            }
        }
        return null;
    }

    public static string GetDepartureAirport(List<Flight> flights, string flightID)
    {
        foreach (var flight in flights)
        {
            if (flight.Id == flightID)
            {
                return flight.ArrivalAirport;
            };
        }
        return string.Empty;
    }

    internal Node InsertRec(Node? root, string data)
    {
        if (root == null)
        {
            root = new Node(data);
            return root;
        }

        int index = 0;
        while (index < root.Children.Count && string.Compare(data, root.Children[index].Data) > 0)
        {
            if (AlreadyInserted(data))
            {
                var node = GetNode(data);

                node?.Children.Insert(0, new Node(data));
            }
            index++;
        }

        root.Children.Insert(index, new Node(data));

        return root;
    }
}

