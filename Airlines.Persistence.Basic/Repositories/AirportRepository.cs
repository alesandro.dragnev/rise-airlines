﻿using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.DataContext;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Airlines.Persistence.Basic.Repositories;
public class AirportRepository(RISEAirlinesContext context) : IAirportRepository
{
    private readonly RISEAirlinesContext _context = context;

    public List<Airport> GetAllAirports()
    {
        try
        {

            return [.. _context.Airports];
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines from the database: {ex.Message}");
            return [];
        }
    }
    public Airport? GetAirportByCode(string code)
    {
        try
        {

            var query = _context.Airports.FirstOrDefault(airport => airport.Code == code);

            return query;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines from the database: {ex.Message}");
            return null;
        }
    }
    public Airport? GetAirportById(int id)
    {
        try
        {
            var query = _context.Airports.FirstOrDefault(airport => airport.ID == id);

            return query;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines from the database: {ex.Message}");
            return null;
        }
    }
    public List<Airport> GetAirportsByCity(string city)
    {
        try
        {

            var query = _context.Airports
                .Where(airport => airport.City == city);

#pragma warning disable IDE0305 // Simplify collection initialization
            return query.ToList();
#pragma warning restore IDE0305 // Simplify collection initialization
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return [];
        }
    }
    public List<Airport> GetAirportsByCountry(string country)
    {
        try
        {

            var query = _context.Airports
                .Where(airport => airport.Country == country);

#pragma warning disable IDE0305 // Simplify collection initialization
            return query.ToList();
#pragma warning restore IDE0305 // Simplify collection initialization
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return [];
        }
    }
    public int GetAirportCount()
    {
        try
        {
            return _context.Airports.Count();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return 0;
        }
    }
    public bool Exist(string code) => GetAirportByCode(code) != null;
    public bool AddNewAirport(Airport airport)
    {
        try
        {
            if (!Exist(airport.Code))
            {
                _ = _context.Add(airport);
                _ = _context.SaveChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return false;
    }
    public bool UpdateAirport(Airport airport)
    {
        try
        {
            _ = _context.Update(airport);
            _ = _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return false;
    }
    public bool DeleteAirportByCode(string code)
    {
        try
        {
            var airport = GetAirportByCode(code);

            if (airport != null)
            {
                _ = _context.Remove(airport);
                _ = _context.SaveChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return false;
    }
    public bool DeleteAirportById(int id)
    {
        try
        {
            var airport = GetAirportById(id);

            if (airport != null)
            {
                _ = _context.Remove(airport);
                _ = _context.SaveChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return false;
    }

    public List<Airport> GetAirportsByFilter(string filter, string value)
    {
        try
        {
            var model = new Airport();
            var airports = GetAllAirports();
            var filteredAirports = airports.Where(airport =>
            {
                var propertyValue = airport.GetType().GetProperty(filter)?.GetValue(airport)?.ToString();
                return propertyValue == value;
            });
            return filteredAirports.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return [];
    }
}
