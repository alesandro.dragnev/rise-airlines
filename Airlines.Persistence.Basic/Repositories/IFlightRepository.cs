﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repositories;
public interface IFlightRepository
{
    List<Flight> GetAllFlights();
    Flight? GetFlightsById(int id);
    int GetFlightCount();
    Flight? GetFlightByFlightNumber(string flightNumber);
    List<Flight> GetAllFlightsByArrivalID(int id);
    List<Flight> GetAllFlightsByDepartureID(int id);
    bool Exist(int id);
    bool AddNewFlight(Flight flight);
    bool UpdateFlight(Flight flight);
    bool DeleteFlightById(int id);
    bool DeleteFlightByFlightNumber(string flightNumber);
    List<Flight> GetFlightsByFilter(string filter, string value);
}