﻿using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.Exceptions;
using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repositories;
public class FlightRepository(RISEAirlinesContext context) : IFlightRepository
{
    private readonly RISEAirlinesContext _context = context;

    public List<Flight> GetAllFlights()
    {
        try
        {
            var flightsWithAirportCodes = _context.Flights
                .Join(_context.Airports, f => f.DepartureID, fa => fa.ID, (f, fa) => new { Flight = f, DepartureAirport = fa })
                .Join(_context.Airports, fa => fa.Flight.ArrivalID, ta => ta.ID, (fa, ta) => new { fa.Flight, fa.DepartureAirport, ArrivalAirport = ta })
                .Select(x => new Flight
                {
                    ID = x.Flight.ID,
                    Flight_Number = x.Flight.Flight_Number,
                    Airline = x.Flight.Airline,
                    DepartureID = x.Flight.DepartureID,
                    ArrivalID = x.Flight.ArrivalID,
                    Departure_date = x.Flight.Departure_date,
                    Arrival_date = x.Flight.Arrival_date,
                    Departure = new Airport { ID = x.DepartureAirport.ID, Code = x.DepartureAirport.Code },
                    Arrival = new Airport { ID = x.ArrivalAirport.ID, Code = x.ArrivalAirport.Code }
                })
                .ToList();


            return flightsWithAirportCodes;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return [];
        }
    }

    public int GetFlightCount()
    {
        try
        {
            return _context.Flights.Count();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return 0;
        }
    }

    public Flight? GetFlightsById(int id)
    {
        try
        {

            var query = _context.Flights.FirstOrDefault(flight => flight.ID == id);

            return query;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return null;
        }
    }

    public Flight? GetFlightByFlightNumber(string flightNumber)
    {
        try
        {
            var query = _context.Flights.FirstOrDefault(flight => flight.Flight_Number == flightNumber);

            return query;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return null;
        }
    }

    public List<Flight> GetAllFlightsByArrivalID(int id)
    {
        try
        {
            var query = _context.Flights
                .Where(flight => flight.ArrivalID == id);

#pragma warning disable IDE0305 // Simplify collection initialization
            return query.ToList();
#pragma warning restore IDE0305 // Simplify collection initialization
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return [];
        }
    }

    public List<Flight> GetAllFlightsByDepartureID(int id)
    {
        try
        {
            var query = _context.Flights
                .Where(flight => flight.DepartureID == id);

#pragma warning disable IDE0305 // Simplify collection initialization
            return query.ToList();
#pragma warning restore IDE0305 // Simplify collection initialization
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return [];
        }
    }

    public bool Exist(int id) => GetFlightsById(id) != null;

    public bool AddNewFlight(Flight flight)
    {
        try
        {

            if (!Exist(flight.ID))
            {
                _ = _context.Add(flight);
                _ = _context.SaveChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return false;
    }
    public bool UpdateFlight(Flight flight)
    {
        try
        {
            _ = _context.Update(flight);
            _ = _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return false;
    }
    public bool DeleteFlightById(int id)
    {
        try
        {
            var flight = GetFlightsById(id);

            if (flight != null)
            {
                _ = _context.Remove(flight);
                _ = _context.SaveChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return false;
    }

    public bool DeleteFlightByFlightNumber(string flightNumber)
    {
        try
        {
            var flight = GetFlightByFlightNumber(flightNumber);

            if (flight != null)
            {
                _ = _context.Remove(flight);
                _ = _context.SaveChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }

        return false;
    }

    public List<Flight> GetFlightsByFilter(string filter, string value)
    {
        try
        {
            var model = new Flight();
            var flights = GetAllFlights();
            var filteredFlights = flights.Where(flight =>
            {
                var propertyValue = flight.GetType().GetProperty(filter);
                var searchValue = propertyValue?.GetValue(flight)?.ToString();
                return searchValue == value;
            });
            return filteredFlights.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return [];
    }
}
