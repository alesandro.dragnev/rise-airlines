﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repositories;
public interface IAirlinesRepository
{
    List<Airline> GetAllAirlines();
    Airline? GetAirlineById(int id);
    int GetAirlineCount();
    List<Airline> GetAirlinesBySize(int fleetSize);
    Airline? GetAirlinesByName(string name);
    bool Exist(int id);
    bool AddNewAirline(Airline airline);
    bool UpdateAirline(Airline airline);
    bool DeleteAirlineById(int id);
    bool DeleteAirlineByName(string name);
    List<Airline> GetAirlinesByFilter(string filter, string value);
}