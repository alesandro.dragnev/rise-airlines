﻿using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.Models;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Basic.Repositories;
public class AirlineRepository(RISEAirlinesContext context) : IAirlinesRepository
{
    private readonly RISEAirlinesContext _context = context;

    public List<Airline> GetAllAirlines()
    {
        try
        {
            return [.. _context.Airlines];
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines from the database: {ex.Message}");
            return [];
        }
    }
    public Airline? GetAirlineById(int id)
    {
        try
        {

            return _context.Airlines.FirstOrDefault(airline => airline.ID == id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by id from the database: {ex.Message}");
            return null;
        }
    }
    public int GetAirlineCount()
    {
        try
        {
            return _context.Airlines.Count();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines count from the database: {ex.Message}");
            return 0;
        }
    }
    public Airline? GetAirlinesByName(string name)
    {
        try
        {

            var query = _context.Airlines.FirstOrDefault(airline => airline.Name == name);

            return query;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by name from the database: {ex.Message}");
            return null;
        }
    }
    public List<Airline> GetAirlinesBySize(int fleetSize)
    {
        try
        {

            var query = _context.Airlines
                .Where(airline => airline.Fleet_size == fleetSize);

#pragma warning disable IDE0305 // Simplify collection initialization
            return query.ToList();
#pragma warning restore IDE0305 // Simplify collection initialization
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
            return [];
        }
    }
    public bool Exist(int id) => GetAirlineById(id) != null;
    public bool AddNewAirline(Airline airline)
    {
        try
        {

            if (!Exist(airline.ID))
            {
                _ = _context.Add(airline);
                _ = _context.SaveChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error adding airlines in the database: {ex.Message}");
        }
        return false;
    }
    public bool UpdateAirline(Airline airline)
    {
        try
        {
            _ = _context.Update(airline);
            _ = _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating airlines in the database: {ex.Message}");
        }
        return false;
    }
    public bool DeleteAirlineById(int id)
    {
        try
        {
            var airline = GetAirlineById(id);

            if (airline != null)
            {
                _ = _context.Remove(airline);
                _ = _context.SaveChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error with deleting by id airlines in the database: {ex.Message}");
        }

        return false;
    }
    public bool DeleteAirlineByName(string name)
    {
        try
        {

            var airline = GetAirlinesByName(name);

            if (airline != null)
            {
                _ = _context.Remove(airline);
                _ = _context.SaveChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error with deleting by name airlines in the database: {ex.Message}");
        }
        return false;
    }
    public List<Airline> GetAirlinesByFilter(string filter, string value)
    {
        try
        {
            var model = new Airline();
            var airlines = GetAllAirlines();
            var filteredAirlines = airlines.Where(airline =>
            {
                var propertyValue = airline.GetType().GetProperty(filter)?.GetValue(airline)?.ToString();
                return propertyValue == value;
            });
            return filteredAirlines.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines by size from the database: {ex.Message}");
        }
        return [];
    }
}
