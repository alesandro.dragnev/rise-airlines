﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repositories;
public interface IAirportRepository
{
    List<Airport> GetAllAirports();
    Airport? GetAirportById(int id);
    int GetAirportCount();
    Airport? GetAirportByCode(string code);
    List<Airport> GetAirportsByCountry(string country);
    List<Airport> GetAirportsByCity(string city);
    bool Exist(string code);
    bool AddNewAirport(Airport airport);
    bool UpdateAirport(Airport airport);
    bool DeleteAirportById(int id);
    bool DeleteAirportByCode(string code);
    List<Airport> GetAirportsByFilter(string filter, string value);
}