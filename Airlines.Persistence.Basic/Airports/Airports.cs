﻿using Airlines.Persistence.Basic.Airports.Validation;
namespace Airlines.Persistence.Basic.Airports;
public class Airport
{
    private string _id = string.Empty;
    private string _name = string.Empty;
    private string _city = string.Empty;
    private string _country = string.Empty;

    private Airport(string id, string name, string city, string country)
    {
        ID = id;
        Name = name;
        City = city;
        Country = country;
    }

    public static Airport CreateAirport(string id, string name, string city, string country)
       => new(id, name, city, country);

    public string ID
    {
        get => _id;
        set => _id = AirportValidator.AirportValidation(value) ? value : string.Empty;

    }
    public string Name
    {
        get => _name;
        set => _name = AirportValidator.NameValidation(value) ? value : string.Empty;

    }
    public string City
    {
        get => _city;
        set => _city = AirportValidator.CityValidation(value) ? value : string.Empty;

    }
    public string Country
    {
        get => _country;
        set => _country = AirportValidator.CountryValidation(value) ? value : string.Empty;

    }

    public override string ToString() => $"{ID} | {Name} | {City} | {Country}";

}

