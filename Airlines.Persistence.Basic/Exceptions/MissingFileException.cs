﻿namespace Airlines.Persistence.Basic.Exceptions;
public class MissingFileException(string message) : Exception(message)
{
}
