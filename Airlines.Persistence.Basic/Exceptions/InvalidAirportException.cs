﻿namespace Airlines.Persistence.Basic.Exceptions;
public class InvalidAirportException(string message) : Exception(message)
{
}
