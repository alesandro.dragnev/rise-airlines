﻿namespace Airlines.Persistence.Basic.Exceptions;
public class InvalidAirlineNameException(string message) : Exception(message)
{
}
