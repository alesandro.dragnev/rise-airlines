﻿namespace Airlines.Persistence.Basic.Exceptions;
public class InvalidFlightCodeException(string message) : Exception(message)
{
}
