﻿namespace Airlines.Persistence.Basic.Exceptions;
public class InvalidInputException(string message) : Exception(message)
{

}
