﻿namespace Airlines.Persistence.Aircrafts;
public class PassengerAircraft : AircraftModel
{
    public double? CargoWeight { get; set; }
    public double? CargoVolume { get; set; }
    public int? Seats { get; set; }

    private PassengerAircraft(string name, double weight, double volume, int seats, string model) : base(name, model)
    {
        CargoWeight = weight;
        CargoVolume = volume;
        Seats = seats;
    }
    public static PassengerAircraft CreatePassengerAircraft(string name, double weight, double volume, int seats, string model)
        => new(name, weight, volume, seats, model);


    public override string ToString() => base.ToString() + $" with {Seats} seats, {CargoWeight} weight and {CargoVolume} volume";
}

