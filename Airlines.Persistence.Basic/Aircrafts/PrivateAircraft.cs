﻿namespace Airlines.Persistence.Aircrafts;
public class PrivateAircraft : AircraftModel
{
    public int? Seats { get; set; }

    private PrivateAircraft(string name, int seats, string model) : base(name, model)
        => Seats = seats;

    public static PrivateAircraft CreatePrivateAircraft(string name, int seats, string model)
        => new(name, seats, model);

    public override string ToString() => base.ToString() + $" with {Seats} seats";
}
