﻿namespace Airlines.Persistence.Aircrafts;
public class CargoAircraft : AircraftModel
{
    public double? CargoWeight { get; set; }
    public double? CargoVolume { get; set; }

    private CargoAircraft(string name, double weight, double volume, string model) : base(name, model)
    {
        CargoWeight = weight;
        CargoVolume = volume;
    }

    public static CargoAircraft CreateCargoAircraft(string name, double weight, double volume, string model)
        => new(name, weight, volume, model);

    public override string ToString() => base.ToString() + $" {CargoWeight} weight and {CargoVolume} volume";
}
