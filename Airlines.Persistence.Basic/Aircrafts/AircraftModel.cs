﻿namespace Airlines.Persistence.Aircrafts;
public class AircraftModel
{
    public string Model { get; set; }
    public string Type { get; }

    protected AircraftModel(string name, string model)
    {
        Model = name;
        Type = model;
    }

    public static AircraftModel CreateAircraftModel(string name, string model) => new(name, model);

    public override string ToString() => $"{Model}";
}
