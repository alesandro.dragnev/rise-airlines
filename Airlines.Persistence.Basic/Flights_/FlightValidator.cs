﻿
using Airlines.Persistence.Basic.Exceptions;

namespace Airlines.OLD.Persistence.Basic.Flight;
public static class FlightValidator
{
    public static bool ValidateFlight(Flight flight)
    {
        try
        {
            return flight != null
                   && FlightValidation(flight.Id);

        }
        catch (InvalidAirportException ex)
        {
            Console.WriteLine($"{ex.Message}");
            return false;
        }
    }

    public static bool FlightValidation(string check)
        => !string.IsNullOrEmpty(check) && check.All(char.IsLetterOrDigit) && check.Length == 5;

    public static bool AircraftValidation(string check)
        => !string.IsNullOrEmpty(check);

    public static bool ExistingAirportValidation(string check)
        => !string.IsNullOrEmpty(check) && check.All(char.IsLetter) && check.Length >= 2 && check.Length <= 4;
}
