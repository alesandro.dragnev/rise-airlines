﻿

namespace Airlines.OLD.Persistence.Basic.Flight;
public class Flight
{
    private string _id = string.Empty;
    private string _departureAirport = string.Empty;
    private string _arrivalAirport = string.Empty;
    private string _aircraft = string.Empty;
    private int _price;
    private double _time;

    private Flight(string id, string departureAirport, string arrivalAirport, string aircraft, int price, double time)
    {
        Id = id;
        DepartureAirport = departureAirport;
        ArrivalAirport = arrivalAirport;
        Aircraft = aircraft;
        Price = price;
        Time = time;
    }


    public static Flight CreateFlight(string id, string departureAirport, string arrivalAirport, string aircraft, int price, double time)
         => new(id, departureAirport, arrivalAirport, aircraft, price, time);

    public string Id
    {
        get => _id;
        set => _id = FlightValidator.FlightValidation(value) ? value : string.Empty;

    }

    public string DepartureAirport
    {
        get => _departureAirport;
        set => _departureAirport = FlightValidator.ExistingAirportValidation(value) ? value : string.Empty;

    }

    public string ArrivalAirport
    {
        get => _arrivalAirport;
        set => _arrivalAirport = FlightValidator.ExistingAirportValidation(value) ? value : string.Empty;
    }

    public string Aircraft
    {
        get => _aircraft;
        set => _aircraft = FlightValidator.AircraftValidation(value) ? value : string.Empty;
    }
    public int Price
    {
        get => _price;
        set => _price = value > 0 ? value : 0;
    }

    public double Time
    {
        get => _time;
        set => _time = value > 0 ? value : 0;
    }

    public override string ToString() => $"{Id} from {DepartureAirport} to {ArrivalAirport} with {Aircraft}. Price {Price}$ - Duration {Time}h.";
}
