-- Flights for tomorow
SELECT f.ID, f.Flight_Number, 
       fa.[Name] AS FromAirport,
	   ta.[Name] AS ToAirport, 
       fa.City AS FromCity,
	   ta.City AS ToCity, 
       fa.Country AS FromCountry,
	   ta.Country AS ToCountry, 
       a.[Name] AS Airline,
	   a.Founded AS AirlineFounded,
	   a.[Description] AS AirlineDescription, 
       f.Departure_date, f.Arrival_date
FROM dbo.Flights AS f
JOIN dbo.Airports AS fa 
ON f.DepartureID = fa.ID
JOIN dbo.Airports AS ta 
ON f.ArrivalID = ta.ID
JOIN dbo.Airlines AS a 
ON f.Airline = a.Name
WHERE CONVERT(DATE, f.Departure_date) = DATEADD(DAY, 1, CONVERT(DATE, GETDATE()));

-- Already departed flights
SELECT COUNT(*) as AlreadyDepartedCount FROM Flights
WHERE Flights.Departure_date < GETDATE();

--Fartherest flight
SELECT TOP 1 f.ID, f.Flight_Number, 
       fa.[Name] AS FromAirport,
	   ta.[Name] AS ToAirport, 
       fa.City AS FromCity,
	   ta.City AS ToCity, 
       fa.Country AS FromCountry,
	   ta.Country AS ToCountry, 
       f.Departure_date,
	   f.Arrival_date,
       DATEDIFF(MINUTE, f.Departure_date, f.Arrival_date) AS DurationMinutes
FROM dbo.Flights AS f
JOIN dbo.Airports AS fa 
ON f.DepartureID = fa.ID
JOIN dbo.Airports AS ta 
ON f.ArrivalID = ta.ID
ORDER BY DurationMinutes DESC;

-- All flights for checking if the result is right
SELECT * FROM Flights

-- All Airlines for checking if the result is right
SELECT * FROM Airlines

