ALTER TABLE Flights
ADD CONSTRAINT CHK_Flight_time CHECK (Departure_date < Arrival_date);

ALTER TABLE Flights
ADD CONSTRAINT CHK_Airports CHECK (ArrivalID != DepartureID)

ALTER TABLE Flights
ADD CONSTRAINT CHK_Possible_FL_time CHECK (Departure_date >= GETDATE() AND Arrival_date > GETDATE());

ALTER TABLE Flights
ADD CONSTRAINT CHK_FlightNumber UNIQUE (Flight_Number)