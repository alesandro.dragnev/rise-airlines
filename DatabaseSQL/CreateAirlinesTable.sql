CREATE TABLE Airlines (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Name VARCHAR(100),
    Founded DATE,
    Fleet_size INT,
    Description TEXT
);