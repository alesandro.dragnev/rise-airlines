CREATE NONCLUSTERED INDEX IX_Airlines_Name ON Airlines(Name);

CREATE NONCLUSTERED INDEX IX_Airports_Name ON Airports (Name);

CREATE NONCLUSTERED INDEX IX_Airports_Code ON Airports(Code);

CREATE NONCLUSTERED INDEX IX_Flight_FlightNumber ON Flights (Flight_number);

CREATE NONCLUSTERED INDEX IX_Flight_DeparuteAirportId ON Flights (DepartureID);

CREATE NONCLUSTERED INDEX IX_Flight_ArrivalAirportId ON Flights (ArrivalID);