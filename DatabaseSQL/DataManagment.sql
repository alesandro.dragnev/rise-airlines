INSERT INTO Airports (Name, Country, City, Code, Runways_count, Founded)
VALUES ('John F. Kennedy International Airport', 'United States', 'New York City', 'JFK', 4, '1948-07-01');

INSERT INTO Airports (Name, Country, City, Code, Runways_count, Founded)
VALUES ('Los Angeles International Airport', 'United States', 'Los Angeles', 'LAX', 4, '1930-09-30');

INSERT INTO Flights (Flight_Number, Airline, DepartureID, ArrivalID, Departure_date, Arrival_date)
VALUES
('FL123','WIZZ',1,2, GETDATE(),GETDATE()),
('FL124','RYAN',2,1, GETDATE(),GETDATE());

UPDATE Flights
SET Arrival_date = GETDATE()
WHERE Flight_Number = 'FL124';

DELETE FROM Flights WHERE Flight_Number = 'FL351';

SELECT * FROM Flights