﻿CREATE TABLE Airports (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Name VARCHAR(100),
    Country VARCHAR(100),
    City VARCHAR(100),
    Code VARCHAR(3),
    Runways_count INT,
    Founded DATE
);

