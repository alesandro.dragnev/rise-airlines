CREATE TABLE Flights (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Flight_Number VARCHAR(10),
    Airline VARCHAR(100),
    DepartureID INT,
    ArrivalID INT,
    Departure_date DATETIME,
    Arrival_date DATETIME,
    CONSTRAINT FK_DepartureAirport FOREIGN KEY (DepartureID) REFERENCES Airports(ID),
	CONSTRAINT FK_ArrivalAirport FOREIGN KEY (ArrivalID) REFERENCES Airports(ID)
);