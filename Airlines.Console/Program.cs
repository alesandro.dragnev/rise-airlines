﻿using Airlines.Business;
using Airlines.DataPrinter;
using CommandProcessors;

namespace Main;

public class Program
{
    public static void Search(Dictionary<string, List<object>> data)
    {
        Console.WriteLine("--------------");
        Console.WriteLine("Enter a command: \n" +
            "| search <search term> |\n" +
            "| sort (airports/airlines) (ascending/descending) |\n"
            + "| exist (airline name)  | \n"
            + "| list (name) (city/country) | \n"
            + "| route add (Flight ID) | \n"
            + "| route remove | \n"
            + "| route print | \n"
            + "| route find (FlightID) | \n"
            + "| route check (Arrival) (Departure) | \n"
            + "| route search (Arrival) (Departure) (time | price | shortest|) | \n"
            + "| reserve cargo <FlightID> <Cargo Weight> <Cargo Volume> | \n"
            + "| reserve ticket <FlightID> <Seats> <Small Baggage Count> <Large Baggage Count> | \n"
            + "| batch start | \n"
            + "| (done) for exit| ");

        var word = true;
        while (word)
        {
            word = CommandProcessor.ProcessCommand(data);
        }
    }

    private static void Main()
    {
        GetData getData = new GetData();
        DataPrinter.PrintDictionary(getData.listOfObjects);

        Search(getData.listOfObjects);
    }
}
