using Airlines.Business.Commands.Search;
using Airlines.Business.Commands.Sort;
using Airlines.Business.Commands.Exist;
using Airlines.Business.Commands.List;
using Airlines.Business.Commands.Route;
using Airlines.Business.Commands.Reserve;
using Airlines.Business.Commands.Batch;
using Airlines.Persistence.Basic.Exceptions;
using FileReaders;
using Airlines.OLD.Persistence.Basic.Flight;
using Airlines.Persistence.Basic.Airports;
using Airlines.Persistence.Aircrafts;
using Airlines.Business;

namespace CommandProcessors;

public class CommandProcessor
{
    public static bool ProcessCommand(Dictionary<string, List<object>> dict)
    {
        var flights = dict["flights"].Cast<Flight>().ToList();
        var cargoAircrafts = dict["cargo"].Cast<CargoAircraft>().ToList();
        var passengerAircrafts = dict["passenger"].Cast<PassengerAircraft>().ToList();
        var airportList = dict["airports"].Cast<Airport>().ToList();
        var tokens = ReadLine().Split(' ');
        var action = tokens[0].ToLower();
        var graph = GetData.graph;
        var flightTree = GetData.tree;

        try
        {
            switch (action)
            {
                case "search":
                    Search.SearchCommand(dict, string.Join(" ", tokens[1..])).Execute();
                    break;
                case "sort":
                    Sort.SortCommand(dict, tokens[1], tokens[2]).Execute();
                    Console.WriteLine("Sorted");
                    break;
                case "exist":
                    Exist.ExistCommand(dict, tokens[1]).Execute();
                    break;
                case "list":
                    List.ListCommand(airportList, tokens[1], tokens[2]).Execute();
                    break;
                case "route":
                    switch (tokens[1])
                    {
                        case "add":
                            RouteAddCommand.AddRouteCommand(tokens[2], flights).Execute();
                            break;
                        case "remove":
                            RouteRemoveCommand.RemoveRouteCommand(flights).Execute();
                            break;
                        case "print":
                            RoutePrintCommand.PrintRouteCommand(flights).Execute();
                            break;
                        case "new":
                            RouteNewCommand.NewRouteCommand(flights).Execute();
                            break;
                        case "find":
                            RouteFind.FindRouteCommand(flights, flightTree, tokens[2]).Execute();
                            break;
                        case "check":
                            RouteCheck.CheckRouteCommand(flights, graph, tokens[2], tokens[3]).Execute();
                            break;
                        case "search":
                            RouteSearch.SearchRouteCommand(flights, graph, tokens[2], tokens[3], tokens[4]).Execute();
                            break;
                        default:
                            throw new InvalidInputException("Invalid Input!");
                    }
                    break;
                case "reserve":
                    switch (tokens[1])
                    {
                        case "cargo":
                            ReserveCargo.ReserveCargoCommand(flights, tokens[2], cargoAircrafts, double.Parse(tokens[3]),
                                double.Parse(tokens[4]), "cargo").Execute();
                            break;
                        case "ticket":
                            ReserveTicket.ReserveTicketCommand(flights, tokens[2], passengerAircrafts, double.Parse(tokens[4]),
                                double.Parse(tokens[5]), int.Parse(tokens[3]), "passenger").Execute();
                            break;
                        default:
                            throw new InvalidInputException("Invalid Input!");
                    }
                    break;
                case "batch":
                    if (tokens[1] == "start")
                    {
                        Console.Clear();
                        var batchMode = string.Empty;
                        while (batchMode != "run")
                        {
                            Console.WriteLine("Add command: ");
                            batchMode = ReadLine();
                            if (batchMode == "run")
                            {
                                break;
                            }
                            Batch.AddCommand(batchMode);
                        }
                        Batch.RunBatch(dict);
                        Console.WriteLine("Batch mode ended!");
                    }
                    else
                    {
                        throw new InvalidInputException("Invalid Input!");
                    }
                    break;
                case "done":
                    return false;
                default:
                    throw new InvalidInputException("Not existing command!");
            }
        }
        catch (InvalidInputException ex)
        {
            Console.WriteLine($"{ex.Message}");
        }

        return true;
    }

    public static string ReadLine()
    {
        var element = Console.ReadLine();
        return element ?? "";
    }
}
