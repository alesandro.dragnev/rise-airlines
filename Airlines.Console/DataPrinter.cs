﻿using System.Linq.Expressions;

namespace Airlines.DataPrinter;
public static class DataPrinter
{
    public static void PrintDictionary<TKey, TValue>(Dictionary<TKey, List<TValue>> dictionary)
    where TKey : notnull
    {
        foreach (var kvp in dictionary)
        {
            Console.WriteLine("--------------");
            Console.WriteLine($" - {kvp.Key}: {kvp.Value.Count} elements");
            foreach (var value in kvp.Value)
            {
                Console.WriteLine($"{value}");
            }
        }
    }

    public static void PrintData<T, TProperty>(IEnumerable<T> getData, Expression<Func<T, TProperty>> propertySelector)
    {
        Console.WriteLine("------------");
        foreach (var obj in getData)
        {
            var propertyValue = propertySelector.Compile()(obj);
            Console.WriteLine(propertyValue);
        }
    }
}
