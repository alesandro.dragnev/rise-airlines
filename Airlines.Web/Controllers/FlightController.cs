﻿using Airlines.Business.DTOs;
using Airlines.Business.Services;
using Microsoft.AspNetCore.Mvc;
using Airlines.Business.Validation;
using Microsoft.IdentityModel.Tokens;

namespace Airlines.Web.Controllers;
public class FlightController(IFlightService flightService, IAirportService airportService) : Controller
{
    public IFlightService FlightService = flightService;
    public IAirportService _airportService = airportService;

    // GET: FlightController
    public ActionResult Index()
    {
        var viewModel = FlightService.GetFlights();
        return View(viewModel);
    }

    // GET: FlightController/Details/5
    public ActionResult Details(int id)
    {
        var viewModel = FlightService.GetFlightsById(id);
        return View("Details", viewModel);
    }


    // POST: FlightController/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(FlightDTO model, string arrival, string departure)
    {

        var airports = _airportService.GetAirports();
        var arrivalAirport = _airportService.GetAirportByCode(arrival);
        var departureAirport = _airportService.GetAirportByCode(departure);

        if (arrivalAirport != null && departureAirport != null)
        {
            model.ArrivalID = arrivalAirport.ID;
            model.DepartureID = departureAirport.ID;

        }
        else
        {
            return RedirectToAction(nameof(Index));
        }


        FlightService.AddToFlight(model);
        return RedirectToAction(nameof(Index));

    }


    // GET: FlightController/Edit/5
    public ActionResult Edit(int id)
    {
        var viewModel = FlightService.GetFlightsById(id);
        return View("Edit", viewModel);
    }

    // POST: FlightController/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(FlightDTO model)
    {
        if (ModelState.IsValid)
        {
            FlightService.UpdateFlight(model);
            return RedirectToAction(nameof(Index));
        }

        return View("Details", model);
    }


    // GET: FlightController/Delete/5
    public ActionResult Delete(int id)
    {
        var viewModel = FlightService.GetFlightsById(id);
        return View("Delete", viewModel);
    }

    // POST: FlightController/Delete/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteFlight(int flID)
    {
        FlightService.DeleteFlight(flID);
        return RedirectToAction(nameof(Index));
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Search(string filter, string value)
    {
        var viewModel = FlightService.SearchFlight(filter, value);

        if (viewModel.Count > 0)
        {
            return View("Index", viewModel);
        }
        else
        {
            viewModel = FlightService.GetFlights();
        }
        return View("Index", viewModel);
    }
}
