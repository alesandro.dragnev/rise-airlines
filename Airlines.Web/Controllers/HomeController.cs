using Airlines.Business.Services;
using Airlines.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Airlines.Web.Controllers;
public class HomeController(IFlightService flightService, IAirportService airportService, IAirlineService airlineService) : Controller
{
    /*private readonly ILogger<HomeController> _logger = logger;*/
    public IFlightService FlightService = flightService;
    public IAirportService _airportService = airportService;
    public IAirlineService _airlineService = airlineService;

    public IActionResult Index()
    {
        var airportLenght = _airportService.GetAirportCount();
        var flightLenght = FlightService.GetFlightCount();
        var airlineLenght = _airlineService.GetAirlineCount();

        var displayNumbers = new CombinedViewModel(flightLenght, airportLenght, airlineLenght);

        return View(displayNumbers);
    }

    public IActionResult Privacy() => View();

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error() => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
}
