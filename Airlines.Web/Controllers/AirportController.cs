﻿using Microsoft.AspNetCore.Mvc;
using Airlines.Business.DTOs;
using Airlines.Business.Services;
using Airlines.Business.Validation;
using Microsoft.IdentityModel.Tokens;
#pragma warning disable IDE0060 // Remove unused parameter

namespace Airlines.Web.Controllers;
public class AirportController(IAirportService airportService) : Controller
{
    public IAirportService _airportService = airportService;

    // GET: AirportController
    public ActionResult Index()
    {
        List<AirportDTO> viewModel = _airportService.GetAirports();
        return View(viewModel);
    }

    // GET: AirportController/Details/5
    public ActionResult Details(int id)
    {
        var model = _airportService.GetAirportByID(id);
        return View("Details", model);
    }

    // GET: AirportController/Create
    public ActionResult Create() => View();

    // POST: AirportController/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(AirportDTO model)
    {
        if (AirportValidator.ValidateAirport(model) && ModelState.IsValid)
        {
            _airportService.AddToAirports(model);

            return RedirectToAction(nameof(Index));
        }
        return View(model);

    }

    // GET: AirportController/Edit/5
    public ActionResult Edit(int id)
    {
        var viewModel = _airportService.GetAirportByID(id);
        return View("Edit", viewModel);
    }

    // POST: AirportController/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(int id, IFormCollection collection)
    {
        try
        {
            var viewModel = _airportService.GetAirportByID(id);
            return RedirectToAction(nameof(Index));
        }
        catch
        {
            return View();
        }
    }

    // GET: AirportController/Delete/5
    public ActionResult Delete(int id)
    {
        var viewModel = _airportService.GetAirportByID(id);
        return View("Delete", viewModel);
    }

    // POST: AirportController/Delete/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteAirport(int id)
    {
        _airportService.DeleteAirport(id);
        return RedirectToAction(nameof(Index));
    }

    // POST: AirportController/Search/
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Search(string filter, string value)
    {
        var viewModel = _airportService.SearchAirport(filter, value);
        if (viewModel.Count > 0)
        {
            return View("Index", viewModel);
        }
        else
        {
            viewModel = _airportService.GetAirports();
        }
        return View("Index", viewModel);
    }
}
