﻿using Airlines.Business.DTOs;
using Airlines.Business.Services;
using Airlines.Business.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.IdentityModel.Tokens;
using System.Threading.Tasks;

namespace Airlines.Web.Controllers;
public class AirlineController(IAirlineService airlineService) : Controller
{
    public IAirlineService _airlineService = airlineService;

    // GET: AirlineController
    public ActionResult Index()
    {
        var viewModel = _airlineService.GetAirlines();
        return View(viewModel);
    }

    // GET: AirlineController/Details/5
    public ActionResult Details(int id)
    {
        var viewModel = _airlineService.GetAirlineByID(id);
        return View("Details", viewModel);
    }

    // GET: AirlineController/Create
    public ActionResult Create() => View();

    // POST: AirlineController/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(AirlineDTO model)
    {
        if (model.Founded < new DateOnly(2024, 1, 1))
        {
            ModelState.AddModelError("Founded", "The date should be earlier than now");
        }

        if (model.Name.Length > 6 && !model.Name.IsNullOrEmpty())
        {
            ModelState.AddModelError("Name", "Name must be no longer than 6 and must be filled");
        }

        if (model.Fleet_size > 0)
        {
            ModelState.AddModelError("Fleet_sie", "Fleet size must be greater than 0");
        }

        if (model.Description.Length > 50)
        {
            ModelState.AddModelError("Description", "Description must be longer than 50 chars");
        }
        if (ModelState.IsValid)
        {

            _airlineService.AddToAirlines(model);
            return RedirectToAction(nameof(Index));
        }
        var viewModel = _airlineService.GetAirlines();
        return RedirectToAction("Index");
    }

    // GET: AirlineController/Edit/5
    public ActionResult Edit(int id)
    {
        var viewModel = _airlineService.GetAirlineByID(id);
        return View("Edit", viewModel);
    }

    // POST: AirlineController/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult EditAirline(AirlineDTO model)
    {
        if (ModelState.IsValid && AirlinesValidator.AirlineValidation(model))
        {
            _airlineService.UpdateAirline(model);
            return RedirectToAction(nameof(Index));
        }

        return View(model);
    }



    // GET: AirlineController/Delete/5
    public ActionResult Delete(int id)
    {
        var viewModel = _airlineService.GetAirlineByID(id);
        return View("Delete", viewModel);
    }

    // POST: AirlineController/Delete/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteAirline(int id)
    {

        _airlineService.DeleteAirline(id);
        return RedirectToAction(nameof(Index));
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Search(string filter, string value)
    {
        var viewModel = _airlineService.SearchAirline(filter, value);
        if (viewModel.Count > 0)
        {
            return View("Index", viewModel);
        }
        else
        {
            viewModel = _airlineService.GetAirlines();
        }
        return View("Index", viewModel);
    }
}
