﻿namespace Airlines.Web.Models;

public class Search
{
    public string Filter;
    public string Value;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public Search()
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    {

    }
    public Search(string value, string filter)
    {
        Filter = filter;
        Value = value;
    }
}
