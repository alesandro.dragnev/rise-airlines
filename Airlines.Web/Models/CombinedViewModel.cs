﻿namespace Airlines.Web.Models;

public class CombinedViewModel(int flightLenght, int airportsLenght, int airlinesLenght)
{
    public int FlightsLenght { get; set; } = flightLenght;
    public int AirlinesLenght { get; set; } = airlinesLenght;
    public int AirportsLenght { get; set; } = airportsLenght;
}
