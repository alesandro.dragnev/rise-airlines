const submitButton = document.getElementById('submit-button');
const flightValid = document.getElementById('flight-number-valid');
const airlineValid = document.getElementById('airline-valid');
const departureValid = document.getElementById('departure-code-valid');
const arrivalValid = document.getElementById('arrival-code-valid');
const departureDateValid = document.getElementById('departure-date-valid');
const arrivalDateValid = document.getElementById('arrival-date-valid');
const flightNumber = document.getElementById('flight_number');
const departureDate = document.getElementById('departure_date');
const arrivalDate = document.getElementById('arrival_date');

function disableButton() {
    submitButton.disabled = true;
    submitButton.style.backgroundColor = '#E5E5E5';
}

function enableButton() {
    submitButton.disabled = false;
    submitButton.style.backgroundColor = '#FCA311';
    submitButton.style.cursor = "pointer";
}

function checkFlightNumber() {
    const flightNumberValue = flightNumber.value.trim();
    if (flightNumberValue.length !== 5) {
        flightValid.classList.remove('hidden');
        return false;
    } else {
        flightValid.classList.add('hidden');
    }
    return true;
}

function checkAirline() {
    const airlineValue = airline.value.trim();
    if (airlineValue === '') {
        airlineValid.classList.remove('hidden');;
        return false;
    } else {
        airlineValid.classList.add('hidden');
    }
    return true;
}

function checkDepartureCode() {
    const departureValue = departure.value.trim();
    if (departureValue.length < 1 || departureValue.length > 3) {
        departureValid.classList.remove('hidden');
        return false;
    } else {
        departureValid.classList.add('hidden');
    }
    return true;
}

function checkArrivalCode() {
    const arrivalValue = arrival.value.trim();
    if (arrivalValue.length < 1 || arrivalValue.length > 3) {
        arrivalValid.classList.remove('hidden');
        return false;
    } else {
        arrivalValid.classList.add('hidden');
    }
    return true;
}

function checkDepartureDate() {
    const departureDateValue = new Date(departureDate.value.trim()).getTime();
    const arrivalDateValue = new Date(arrivalDate.value.trim()).getTime();
    if (departureDateValue >= arrivalDateValue) {
        departureDateValid.classList.remove('hidden');
        return false;
    } else {
        departureDateValid.classList.add('hidden');
    }
    return true;
}

function checkArrivalDate() {
    const departureDateValue = new Date(departureDate.value.trim()).getTime();
    const arrivalDateValue = new Date(arrivalDate.value.trim()).getTime();
    if (departureDateValue >= arrivalDateValue) {
        arrivalDateValid.classList.remove('hidden');
        return false;
    } else {
        arrivalDateValid.classList.add('hidden');
    }
    return true;
}

function checkFormValidity() {
    if (
        checkFlightNumber() &&
        checkAirline() &&
        checkDepartureCode() &&
        checkArrivalCode() &&
        checkDepartureDate() &&
        checkArrivalDate()
    ) {
        enableButton();
    } else {
        disableButton();
    }
}

disableButton();

flightNumber.addEventListener('input', checkFormValidity);
departureDate.addEventListener('input', checkFormValidity);
arrivalDate.addEventListener('input', checkFormValidity);


