const buttonShowMore = document.getElementById('show-more');
const buttonScrollTop = document.getElementById('scroll-top');
const table = document.getElementById('table');
const searchBar = document.getElementById('search-bar');
const form = document.getElementById('form');

buttonScrollTop.style.display = "none";

function showForm() {
    if (form.classList.contains('hidden')) {
        table.classList.add('hidden');
        searchBar.classList.add('hidden');
        form.classList.remove('hidden');
        buttonShowMore.classList.add('hidden');
    } else {
        table.classList.remove('hidden');
        searchBar.classList.remove('hidden');
        form.classList.add('hidden');
        buttonShowMore.classList.remove('hidden');
    }
}


for (let i = 2; i < table.rows.length; i++) {
    table.rows[i].style.display = 'none';
}

function showMore() {
    if (table.rows[2].style.display === 'none') {
        for (let i = 2; i < table.rows.length; i++) {
            table.rows[i].style.display = '';
        }
    }
    else {
        for (let i = 2; i < table.rows.length; i++) {
            table.rows[i].style.display = 'none';
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    var loadingOverlay = document.getElementById('loading-overlay');

    loadingOverlay.style.display = 'block';
    setTimeout(function () {
        loadingOverlay.style.opacity = '0';
        setTimeout(function () {
            loadingOverlay.style.display = 'none';
        }, 500);
    }, 1000);

    table.addEventListener("scroll", function() {
        if (table.scrollHeight > 300) {
            buttonScrollTop.style.display = "";
        }
    });

    function scrollTop() {
        table.style.scrollBehavior = "smooth";
        table.scrollTop = 0;
    }
    buttonScrollTop.addEventListener("click", scrollTop);
});

