const submitButton = document.getElementById('submit-button');
const nameValid = document.getElementById('name-valid');
const countyValid = document.getElementById('country-valid');
const cityValid = document.getElementById('city-valid');
const codeValid = document.getElementById('code-valid');
const runwaysCountValid = document.getElementById('runways-count-valid');
const foundedValid = document.getElementById('founded-valid');
const name = document.getElementById('name');
const country = document.getElementById('country');
const city = document.getElementById('city');
const code = document.getElementById('code');
const runways = document.getElementById('runways_count');
const founded = document.getElementById('founded');

function disableButton() {
    submitButton.disabled = true;
    submitButton.style.backgroundColor = '#E5E5E5';
}

function enableButton() {
    submitButton.disabled = false;
    submitButton.style.backgroundColor = '#FCA311';
    submitButton.style.cursor = "pointer";
}

function checkName() {
    const nameValue = name.value.trim();
    if (nameValue === "") {
        nameValid.classList.remove('hidden');
        return false;
    } else {
        nameValid.classList.add('hidden');
    }
    return true;
}

function checkCountry() {
    const countryValue = country.value.trim();
    if (countryValue === '') {
        countyValid.classList.remove('hidden');
        return false;
    } else {
        countyValid.classList.add('hidden');
    }
    return true;
}

function checkCity() {
    const cityValue = city.value.trim();
    if (cityValue === "") {
        cityValid.classList.remove('hidden');
        return false;
    } else {
        cityValid.classList.add('hidden');
    }
    return true;
}

function checkCode() {
    const codeValue = code.value.trim();
    if (codeValue.length !== 3) {
        codeValid.classList.remove('hidden');
        return false;
    } else {
        codeValid.classList.add('hidden');
    }
    return true;
}

function checkRunwaysCount() {
    const runwaysValue = runways.value.trim();
    if (runwaysValue === '') {
        runwaysCountValid.classList.remove('hidden');
        return false;
    } else {
        runwaysCountValid.classList.add('hidden');
    }
    return true;
}

function checkFounded() {
    const foundedValue = new Date(founded.value.trim()).getTime();
    if (isNaN(foundedValue) || foundedValue >= Date.now()) {
        foundedValid.classList.remove('hidden');
        return false;
    } else {
        foundedValid.classList.add('hidden');
    }
    return true;
}

function checkFormValidity() {
    if (
        checkName() &&
        checkCountry() &&
        checkCity() &&
        checkCode() &&
        checkRunwaysCount() &&
        checkFounded()
    ) {
        enableButton();
    } else {
        disableButton();
    }
}

disableButton();

name.addEventListener('input', checkFormValidity);
country.addEventListener('input', checkFormValidity);
city.addEventListener('input', checkFormValidity);
code.addEventListener('input', checkFormValidity);
runways.addEventListener('input', checkFormValidity);
founded.addEventListener('input', checkFormValidity);
