const submitButton = document.getElementById('submit-button');
const nameValid = document.getElementById('name-valid');
const fleetValid = document.getElementById('fleet-valid');
const descriptionValid = document.getElementById('description-valid');
const foundedValid = document.getElementById('founded-valid');
const name = document.getElementById('name');
const fleet = document.getElementById('fleet_size');
const description = document.getElementById('description');
const founded = document.getElementById('founded');

function disableButton() {
    submitButton.disabled = true;
    submitButton.style.backgroundColor = '#E5E5E5';
}

function enableButton() {
    submitButton.disabled = false;
    submitButton.style.backgroundColor = '#FCA311';
    submitButton.style.cursor = "pointer";
}

function checkName() {
    const nameValue = name.value.trim();
    if (nameValue === "") {
        nameValid.classList.remove('hidden');
        return false;
    } else {
        nameValid.classList.add('hidden');
    }
    return true;
}

function checkFleet() {
    const fleetValue = fleet.value.trim();
    if (fleetValue === '') {
        fleetValid.classList.remove('hidden');
        return false;
    } else {
        fleetValid.classList.add('hidden');
    }
    return true;
}

function checkDescription() {
    const descriptionValue = description.value.trim();
    if (descriptionValue === '') {
        descriptionValid.classList.remove('hidden');
        return false;
    } else {
        descriptionValid.classList.add('hidden');
    }
    return true;
}

function checkFounded() {
    const foundedValue = founded.value.trim();
    if (foundedValue === "") {
        foundedValid.classList.remove('hidden');
        return false;
    } else {
        foundedValid.classList.add('hidden');
    }
    return true;
}

function checkFormValidity() {
    if (
        checkName() &&
        checkFleet() &&
        checkDescription() &&
        checkFounded()
    ) {
        enableButton();
    } else {
        disableButton();
    }
}

name.addEventListener('input', checkFormValidity);
fleet.addEventListener('input', checkFormValidity);
description.addEventListener('input', checkFormValidity);
founded.addEventListener('input', checkFormValidity);


disableButton();
