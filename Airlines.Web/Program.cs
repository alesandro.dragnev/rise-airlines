using Airlines.Persistence.Basic.DataContext;
using Airlines.Business.Mappers;
using Airlines.Persistence.Basic.Repositories;
using Airlines.Business.Services;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
#pragma warning disable IDE0058 // Expression value is never used

namespace Airlines.Web;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddControllersWithViews();
        //Airport
        builder.Services.AddScoped<IAirportRepository, AirportRepository>();
        builder.Services.AddScoped<IAirportService, AirportService>();
        builder.Services.AddAutoMapper(typeof(AirportMapper));
        // DB
        builder.Services.AddDbContext<RISEAirlinesContext>(options =>
        options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
        // Airline
        builder.Services.AddScoped<IAirlinesRepository, AirlineRepository>();
        builder.Services.AddScoped<IAirlineService, AirlineService>();
        builder.Services.AddAutoMapper(typeof(AirlineMapper));
        //Flight
        builder.Services.AddScoped<IFlightRepository, FlightRepository>();
        builder.Services.AddScoped<IFlightService, FlightService>();
        builder.Services.AddAutoMapper(typeof(FlightMapper));

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthorization();

        app.MapControllerRoute(
            name: "default",
            pattern: "{controller=Home}/{action=Index}/{id?}");

        app.Run();
    }
}
