﻿using Airlines.Business.DTOs;
using Airlines.Persistence.Basic.Repositories;
using Airlines.Persistence.Basic.Models;
using AutoMapper;
#pragma warning disable CA1822 // Mark members as static

namespace Airlines.Business.Services;
public class AirportService(IAirportRepository airportRepository, IMapper airportMapper) : IAirportService
{

    private readonly IAirportRepository _airportRepository = airportRepository;
    private readonly IMapper _airportMapper = airportMapper;

    public void AddToAirports(AirportDTO airportDto)
    {
        var airport = _airportMapper.Map<Airport>(airportDto);
        _ = _airportRepository.AddNewAirport(airport);
    }
    public void DeleteAirport(int id) => _airportRepository.DeleteAirportById(id);
    public void UpdateAirport(AirportDTO airportDto)
    {
        var airport = _airportMapper.Map<Airport>(airportDto);
        bool exist = _airportRepository.UpdateAirport(airport);
        if (!exist)
        {
            Console.WriteLine("It is not successfully!");
        }
    }
    public void PrintAirports()
    {
        var airportList = _airportRepository.GetAllAirports();
        foreach (var airport in airportList)
        {
            Console.WriteLine(
                $"Airport ID: {airport.ID}, Name: {airport.Name}, " +
                $"Country: {airport.Country}, " +
                $"City: {airport.City}," +
                $"Code: {airport.Code}, " +
                $"Runways: {airport.Runways_count}, " +
                $"Founded: {airport.Founded}"
                );
        }
    }

    public List<AirportDTO> GetAirports()
    {
        var airportList = _airportRepository.GetAllAirports();
        var airports = _airportMapper.Map<List<AirportDTO>>(airportList);

        return airports;
    }

    public List<AirportDTO> GetAirportByID(int id)
    {
        var airport = _airportRepository.GetAirportById(id);
        List<Airport> airportList = [];
        if (airport != null)
        {
            airportList.Add(airport);
        }
        var airports = _airportMapper.Map<List<AirportDTO>>(airportList);

        return airports;
    }

    public int GetAirportCount() => _airportRepository.GetAirportCount();

    public List<AirportDTO> SearchAirport(string filter, string value)
    {
        var searchedAirports = _airportRepository.GetAirportsByFilter(filter, value);
        var airports = _airportMapper.Map<List<AirportDTO>>(searchedAirports);

        return airports;
    }

    public AirportDTO GetAirportByCode(string code)
    {
        var model = _airportRepository.GetAirportByCode(code);
        var airport = _airportMapper.Map<AirportDTO>(model);

        return airport;
    }
}
