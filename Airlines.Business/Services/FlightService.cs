﻿using Airlines.Business.DTOs;
using Airlines.Persistence.Basic.Repositories;
using Airlines.Persistence.Basic.Models;
using AutoMapper;
using Airlines.Business.Mappers;
#pragma warning disable CA1822 // Mark members as static

namespace Airlines.Business.Services;
public class FlightService(IFlightRepository flightRepository, IMapper flightMapper) : IFlightService
{
    private readonly IFlightRepository _flightRepository = flightRepository;
    private readonly IMapper _flightMapper = flightMapper;

    public void AddToFlight(FlightDTO flightDTO)
    {
        var flight = _flightMapper.Map<Flight>(flightDTO);
        bool exist = _flightRepository.AddNewFlight(flight);
        if (!exist)
        {
            Console.WriteLine("It is not successfully!");
        }
    }
    public void DeleteFlight(int id)
    {
        bool exist = _flightRepository.DeleteFlightById(id);
        if (!exist)
        {
            Console.WriteLine("It is not successfully!");
        }
    }
    public void UpdateFlight(FlightDTO flightDTO)
    {
        var flight = _flightMapper.Map<Flight>(flightDTO);
        bool exist = _flightRepository.UpdateFlight(flight);
        if (!exist)
        {
            Console.WriteLine("It is not successfully!");
        }
    }
    public void PrintFlights()
    {
        var flightList = _flightRepository.GetAllFlights();
        foreach (var flight in flightList)
        {
            Console.WriteLine(
                $"Flight ID: {flight.ID}, " +
                $"Flight Number: {flight.Flight_Number}, " +
                $"Arrival: {flight.ArrivalID}, " +
                $"Departure: {flight.DepartureID}, " +
                $"Airline: {flight.Airline}," +
                $"Departure Date: {flight.Departure_date}, " +
                $"Arrival Date: {flight.Arrival_date}"
                );
        }
    }

    public List<FlightDTO> GetFlights()
    {
        var flightList = _flightRepository.GetAllFlights();
        var flights = _flightMapper.Map<List<FlightDTO>>(flightList);

        return flights;
    }

    public List<FlightDTO> GetFlightsById(int id)
    {
        var flight = _flightRepository.GetFlightsById(id);
        List<Flight> flights = [];
        if (flight != null)
        {
            flights.Add(flight);
        }
        var flightList = _flightMapper.Map<List<FlightDTO>>(flights);
        return flightList;
    }

    public int GetFlightCount() => _flightRepository.GetFlightCount();
    public List<FlightDTO> SearchFlight(string filter, string value)
    {
        var searchedFlights = _flightRepository.GetFlightsByFilter(filter, value);
        var flights = _flightMapper.Map<List<FlightDTO>>(searchedFlights);

        return flights;
    }
}
