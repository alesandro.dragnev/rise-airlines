﻿using Airlines.Business.DTOs;

namespace Airlines.Business.Services;
public interface IAirlineService
{
    List<AirlineDTO> GetAirlines();
    void AddToAirlines(AirlineDTO airlineDTO);
    void DeleteAirline(int id);
    void UpdateAirline(AirlineDTO airlineDTO);
    void PrintAirlines();
    List<AirlineDTO> GetAirlineByID(int id);
    int GetAirlineCount();
    List<AirlineDTO> SearchAirline(string filter, string value);
}
