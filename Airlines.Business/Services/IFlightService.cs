﻿using Airlines.Business.DTOs;

namespace Airlines.Business.Services;
public interface IFlightService
{
    void AddToFlight(FlightDTO flightDTO);
    void DeleteFlight(int id);
    void UpdateFlight(FlightDTO flightDTO);
    void PrintFlights();
    List<FlightDTO> GetFlights();
    List<FlightDTO> GetFlightsById(int id);
    int GetFlightCount();
    List<FlightDTO> SearchFlight(string filter, string value);
}
