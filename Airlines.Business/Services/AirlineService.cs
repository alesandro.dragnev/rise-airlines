﻿using Airlines.Business.DTOs;
using Airlines.Business.Mappers;
using Airlines.Business.Services;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repositories;
using AutoMapper;
#pragma warning disable CA1822 // Mark members as static

namespace Airlines.Business.Services;
public class AirlineService(IAirlinesRepository airlineRepository, IMapper airlineMapper) : IAirlineService
{
    private readonly IAirlinesRepository _airlineRepository = airlineRepository;
    private readonly IMapper _airlineMapper = airlineMapper;

    public void AddToAirlines(AirlineDTO airlineDTO)
    {
        var airline = _airlineMapper.Map<Airline>(airlineDTO);
        bool exist = _airlineRepository.AddNewAirline(airline);
        if (!exist)
        {
            Console.WriteLine("It is not successfully!");
        }
    }
    public void DeleteAirline(int id) => _airlineRepository.DeleteAirlineById(id);
    public void UpdateAirline(AirlineDTO airlineDTO)
    {
        var airline = _airlineMapper.Map<Airline>(airlineDTO);
        bool updated = _airlineRepository.UpdateAirline(airline);
        if (!updated)
        {
            Console.WriteLine("Error updating the airline.");
        }
    }

    public void PrintAirlines()
    {
        var airlineList = _airlineRepository.GetAllAirlines();
        foreach (var airline in airlineList)
        {
            Console.WriteLine($"Airport ID: {airline.ID}, Name: {airline.Name}, Founded: {airline.Founded}, ${airline.Fleet_size}");
        }
    }

    public List<AirlineDTO> GetAirlines()
    {
        var airlines = _airlineRepository.GetAllAirlines();
        var airlinesList = _airlineMapper.Map<List<AirlineDTO>>(airlines);
        return airlinesList;
    }

    public List<AirlineDTO> GetAirlineByID(int id)
    {
        var airlines = _airlineRepository.GetAirlineById(id);
        var dTO = _airlineMapper.Map<AirlineDTO>(airlines);
        List<AirlineDTO> airlinesList = [];
        if (airlines != null)
        {
            airlinesList.Add(dTO);
        }
        return airlinesList;
    }

    public int GetAirlineCount() => _airlineRepository.GetAirlineCount();
    public List<AirlineDTO> SearchAirline(string filter, string value)
    {
        var searchedAirlines = _airlineRepository.GetAirlinesByFilter(filter, value);
        var airlines = _airlineMapper.Map<List<AirlineDTO>>(searchedAirlines);

        return airlines;
    }
}
