﻿using Airlines.Business.DTOs;

namespace Airlines.Business.Services;
public interface IAirportService
{
    void AddToAirports(AirportDTO airportDto);
    void DeleteAirport(int id);
    void UpdateAirport(AirportDTO airportDto);
    void PrintAirports();
    List<AirportDTO> GetAirports();
    List<AirportDTO> GetAirportByID(int id);
    int GetAirportCount();
    List<AirportDTO> SearchAirport(string filter, string value);
    AirportDTO GetAirportByCode(string code);
}
