﻿using AutoMapper;
using Airlines.Persistence.Basic.Models;
using Airlines.Business.DTOs;

namespace Airlines.Business.Mappers;
public class AirportMapper : Profile
{

    public AirportMapper() => CreateMap<Airport, AirportDTO>().ReverseMap();
}
