﻿using AutoMapper;
using Airlines.Persistence.Basic.Models;
using Airlines.Business.DTOs;

namespace Airlines.Business.Mappers;
public class AirlineMapper : Profile
{
    public AirlineMapper() => CreateMap<Airline, AirlineDTO>().ReverseMap();
}
