﻿using Airlines.Business.DTOs;
using Airlines.Persistence.Basic.Models;
using AutoMapper;

namespace Airlines.Business.Mappers;
public class FlightMapper : Profile
{
    public FlightMapper() => CreateMap<Flight, FlightDTO>().ReverseMap();
}
