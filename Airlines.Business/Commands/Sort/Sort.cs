﻿using Airlines.Persistence.Basic.Airports;
using Airlines.Persistence.Basic.Airlines;

namespace Airlines.Business.Commands.Sort;
public class Sort : ICommand
{
    private string _dataType = string.Empty;
    private string _sortType = string.Empty;

    private Sort(Dictionary<string, List<object>> dataDict, string dataType, string sortType)
    {
        DataDict = dataDict;
        DataType = dataType;
        SortType = sortType;
    }

    public Dictionary<string, List<object>> DataDict { get; set; } = [];

    private string DataType
    {
        get => _dataType;
        set => _dataType = !string.IsNullOrEmpty(value) ? value.ToLower() : string.Empty;
    }
    private string SortType
    {
        get => _sortType;
        set => _sortType = !string.IsNullOrEmpty(value) ? value.ToLower() : string.Empty;
    }

    internal static Sort SortCommand(Dictionary<string, List<object>> dataDict, string dataType, string sortType)
    {
        var sort = new Sort(dataDict, dataType, sortType);
        return sort;
    }

    private void SortAirportList()
    {
        if (SortType == "ascending")
        {
            var list = SortingExtension.SortAscending(DataDict[DataType], airport => ((Airport)airport).ID);
            DataDict[DataType] = list;
        }
        else if (SortType == "descending")
        {
            var list = SortingExtension.SortDecending(DataDict[DataType], airport => ((Airport)airport).ID);
            DataDict[DataType] = list;
        }
    }

    private void SortAirlineList()
    {
        if (SortType == "ascending")
        {
            var list = SortingExtension.SortAscending(DataDict[DataType], airline => ((Airline)airline).Name);
            DataDict[DataType] = list;
        }
        else if (SortType == "descending")
        {
            var list = SortingExtension.SortDecending(DataDict[DataType], airline => ((Airline)airline).Name);
            DataDict[DataType] = list;
        }
    }

    public void Execute()
    {
        if (DataType == "airlines")
        {
            SortAirlineList();
        }
        else if (DataType == "airports")
        {
            SortAirportList();
        }
    }
}
