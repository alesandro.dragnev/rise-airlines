﻿using Airlines.Persistence.Basic.Exceptions;
using Airlines.OLD.Persistence.Basic.Flight;

namespace Airlines.Business.Commands.Route;
public class RouteAddCommand : Route, ICommand
{
    protected string _flightToAdd = string.Empty;
    private RouteAddCommand(string flightToAdd, List<Flight> flights) : base(flights)
        => FlightToAdd = flightToAdd;

    protected string FlightToAdd
    {
        get => _flightToAdd;
        set => _flightToAdd = !string.IsNullOrEmpty(value) ? value.ToLower() : string.Empty;
    }

    internal static RouteAddCommand AddRouteCommand(string flightToAdd, List<Flight> flights)
    {
        var newRoute = new RouteAddCommand(flightToAdd, flights);

        return newRoute;
    }

    internal void AddRoute()
    {
        string departed = string.Empty;

        try
        {
            if (routeManagement.Count > 0 && routeManagement.Last != null)
            {
                departed = routeManagement.Last.Value.ArrivalAirport;
            }

            foreach (var flight in _flights)
            {
                if (flight.Id.Equals(FlightToAdd.ToUpper()) && !routeManagement.Contains(flight))
                {
                    _ = departed == string.Empty || flight.DepartureAirport.Equals(departed)
                        ? routeManagement.AddLast(flight)
                        : throw new InvalidInputException($"Cannot depart from another airport. Must be {departed}");
                    return;
                }
            }

            throw new InvalidInputException("Already exists or not a valid flight number");
        }
        catch (InvalidInputException)
        {
        }
    }

    public void Execute() => AddRoute();
}
