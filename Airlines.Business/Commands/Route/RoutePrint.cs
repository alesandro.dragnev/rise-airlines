﻿using Airlines.OLD.Persistence.Basic.Flight;

namespace Airlines.Business.Commands.Route;
public class RoutePrintCommand : Route, ICommand
{
    private RoutePrintCommand(List<Flight> flights) : base(flights)
    {
    }

    internal static RoutePrintCommand PrintRouteCommand(List<Flight> flights)
    {
        var newRoute = new RoutePrintCommand(flights);

        return newRoute;
    }

    internal static void PrintRoute()
    {
        foreach (var flight in routeManagement)
        {
            Console.WriteLine($"{flight}");
        }
    }

    public void Execute() => PrintRoute();
}