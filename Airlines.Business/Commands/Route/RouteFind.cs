﻿using Airlines.OLD.Persistence.Basic.Flight;
using Airlines.Persistence;

namespace Airlines.Business.Commands.Route;
public class RouteFind : Route, ICommand
{
    public Tree flightTree;
    public string _find = string.Empty;

    private RouteFind(List<Flight> flights, Tree flightTrees, string find) : base(flights)
    {
        flightTree = flightTrees;
        _find = find.ToUpper();
    }

    internal static RouteFind FindRouteCommand(List<Flight> flights, Tree flightTrees, string find)
    {
        var newRoute = new RouteFind(flights, flightTrees, find);

        return newRoute;
    }

    public static void PrintTree(Node? root, string indent = "")
    {
        if (root != null)
        {
            Console.WriteLine(indent + root.Data);

            indent += "  ";

            foreach (var child in root.Children)
            {
                PrintTree(child, indent);
            }
        }
    }

    public List<Node> FindPathToNode(string targetData)
    {
        List<Node> path = [];
        if (flightTree.Root != null)
        {
            _ = FindPathRecursive(flightTree.Root, targetData, path);
        }
        return path;
    }

    private static bool FindPathRecursive(Node current, string targetData, List<Node> path)
    {
        if (current == null)
        {
            return false;
        }

        path.Add(current);

        if (current.Data == targetData)
        {
            return true;
        }

        foreach (var child in current.Children)
        {
            return FindPathRecursive(child, targetData, path);
        }

        path.RemoveAt(path.Count - 1);
        return false;
    }

    public void PrintPath()
    {
        var path = FindPathToNode(_find);
        foreach (var node in path)
        {
            Console.WriteLine(node.Data);
        }
    }


    public void Execute() => PrintPath();
}
