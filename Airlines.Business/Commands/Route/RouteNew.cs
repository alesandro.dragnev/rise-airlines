﻿using Airlines.OLD.Persistence.Basic.Flight;

namespace Airlines.Business.Commands.Route;
public class RouteNewCommand : Route, ICommand
{
    private RouteNewCommand(List<Flight> flights) : base(flights)
    {
    }

    internal static RouteNewCommand NewRouteCommand(List<Flight> flights)
    {
        var newRoute = new RouteNewCommand(flights);

        return newRoute;
    }

    internal static void NewRoute() => routeManagement.Clear();

    public void Execute() => NewRoute();
}