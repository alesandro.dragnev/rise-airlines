﻿using Airlines.OLD.Persistence.Basic.Flight;
using Airlines.Persistence;

namespace Airlines.Business.Commands.Route;

public class RouteSearch : Route, ICommand
{
    public Graph _graph;
    public string _startAirport = string.Empty;
    public string _departureAirport = string.Empty;
    public string _byWeight = string.Empty;

    private RouteSearch(List<Flight> flights, Graph graph, string startAirport, string departureAirport, string byWeight) : base(flights)
    {
        _graph = graph;
        _startAirport = startAirport.ToUpper();
        _departureAirport = departureAirport.ToUpper();
        _byWeight = byWeight;
    }

    internal static RouteSearch SearchRouteCommand(List<Flight> flights, Graph graph, string startAirport, string departureAirport, string byWeight)
    {
        var newRoute = new RouteSearch(flights, graph, startAirport, departureAirport, byWeight);

        return newRoute;
    }

    public void Execute()
    {
        try
        {
            List<Edge> path;
            if (_byWeight == "time")
            {
                Console.WriteLine("Time");
                path = FindRouteByTime();
            }
            else
            {
                path = _byWeight == "price"
                    ? FindRouteByPrice()
                    : _byWeight == "shortest" ? FindShortestRoute() : throw new ArgumentException("Invalid weight type specified.");
            }

            if (path.Count == 0)
            {
                Console.WriteLine("No route found.");
                return;
            }

            Console.WriteLine($"Route from {_startAirport} to {_departureAirport}:");
            Console.WriteLine($"{"Destination",-20} {"Price",-10} {"Time (Hours)"}");
            foreach (var edge in path)
            {
                Console.WriteLine($"- {edge.Destination.Data,-20} ${edge.Price,-10} {edge.Time,10:F2}");
            }
        }
        catch (ArgumentException ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public List<Edge> FindRouteByTime()
    {
        var pathFinder = new PathFinder(_graph);
        return pathFinder.FindPathByWeight(_startAirport, _departureAirport, "time");
    }

    public List<Edge> FindRouteByPrice()
    {
        var pathFinder = new PathFinder(_graph);
        return pathFinder.FindPathByWeight(_startAirport, _departureAirport, "price");
    }

    public List<Edge> FindShortestRoute()
    {
        var pathFinder = new PathFinder(_graph);
        return pathFinder.FindPathByWeight(_startAirport, _departureAirport, "shortest");
    }
}
