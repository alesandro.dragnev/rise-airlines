﻿using Airlines.Persistence.Basic.Exceptions;
using Airlines.OLD.Persistence.Basic.Flight;

namespace Airlines.Business.Commands.Route;
public class RouteRemoveCommand : Route, ICommand
{
    private RouteRemoveCommand(List<Flight> flights) : base(flights)
    {
    }

    internal static RouteRemoveCommand RemoveRouteCommand(List<Flight> flights)
    {
        var newRoute = new RouteRemoveCommand(flights);
        return newRoute;
    }

    internal static void RemoveRoute()
    {
        if (routeManagement.Count > 0)
        {
            routeManagement.RemoveLast();
        }
        else
        {
            throw new InvalidInputException("Is already empty!");
        }
    }

    public void Execute() => RemoveRoute();
}