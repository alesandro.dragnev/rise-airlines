﻿using Airlines.Persistence;

namespace Airlines.Business.Commands.Route;

public class PathFinder(Graph graph)
{
    private readonly Graph _graph = graph;
    public List<Edge> FindPathByWeight(string departure, string destination, string weightType)
    {
        var visited = new HashSet<Vertex>();
        var distances = new Dictionary<string, double>();
        var previous = new Dictionary<string, Edge?>();

        var priorityQueue = new SortedSet<(double, string)>();

        var startVertex = _graph._vertexList.FirstOrDefault(v => v.Data == departure);
        var endVertex = _graph._vertexList.FirstOrDefault(v => v.Data == destination);

        if (startVertex == null || endVertex == null)
            throw new ArgumentException("Departure or destination not found in the graph.");

        foreach (var vertex in _graph._vertexList)
        {
            distances[vertex.Data] = double.PositiveInfinity;
            previous[vertex.Data] = null;
        }

        distances[departure] = 0;
        _ = priorityQueue.Add((0, departure));

        while (priorityQueue.Count > 0)
        {
            var (dist, currentVertexData) = priorityQueue.First();
            _ = priorityQueue.Remove((dist, currentVertexData));

            var currentVertex = _graph._vertexList.FirstOrDefault(v => v.Data == currentVertexData);
            if (currentVertex == null)
                continue;

            if (currentVertex.Data == destination)
                break;

            _ = visited.Add(currentVertex);

            foreach (var edge in currentVertex.Children)
            {
                var next = edge.Destination;
                if (visited.Contains(next))
                {
                    continue;
                }

                var newDist = distances[currentVertexData] + (weightType == "time" ? edge.Time : edge.Price);

                if (newDist < distances[next.Data])
                {
                    _ = priorityQueue.Remove((distances[next.Data], next.Data));
                    distances[next.Data] = newDist;
                    previous[next.Data] = edge;
                    _ = priorityQueue.Add((newDist, next.Data));
                }
            }
        }
        var path = new List<Edge>();

        var currentData = destination;

        foreach (var kvp in previous)
        {
            var key = kvp.Key;
            var value = kvp.Value;
            var edge = value;

            if (key == currentData && edge != null)
            {
                path.Insert(0, edge);

                currentData = edge.Destination.Data;

                if (currentData == departure)
                {
                    break;
                }
            }
        }

        return path;
    }
}
