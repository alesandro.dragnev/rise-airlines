﻿using Airlines.OLD.Persistence.Basic.Flight;
using Airlines.Persistence;

namespace Airlines.Business.Commands.Route;
public class RouteCheck : Route, ICommand
{
    public Graph _graph;
    public string _startAirport = string.Empty;
    public string _departureAirport = string.Empty;

    private RouteCheck(List<Flight> flights, Graph graph, string startAirport, string departureAirport) : base(flights)
    {
        _graph = graph;
        _startAirport = startAirport.ToUpper();
        _departureAirport = departureAirport.ToUpper();
    }

    internal static RouteCheck CheckRouteCommand(List<Flight> flights, Graph graph, string startAirport, string departureAirport)
    {
        var newRoute = new RouteCheck(flights, graph, startAirport, departureAirport);

        return newRoute;
    }

    public bool CheckAirports()
    {
        foreach (var vertex in _graph._vertexList)
        {
            if (vertex.Data == _startAirport)
            {
                foreach (var child in vertex.Children)
                {
                    return child.Destination.Data == _departureAirport;
                }
            }
        }
        return false;
    }

    public void Execute() => Console.WriteLine(CheckAirports());
}
