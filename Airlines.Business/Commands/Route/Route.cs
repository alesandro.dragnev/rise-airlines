﻿using Airlines.OLD.Persistence.Basic.Flight;

namespace Airlines.Business.Commands.Route;
public class Route
{
    public static LinkedList<Flight> routeManagement = [];
    protected List<Flight> _flights = [];

    protected Route(List<Flight> flights) => _flights = flights;
}
