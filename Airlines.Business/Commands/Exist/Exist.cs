﻿using Airlines.Persistence.Basic.Airlines;

namespace Airlines.Business.Commands.Exist;
public class Exist : ICommand
{
    private string _key = string.Empty;
    private Dictionary<string, List<object>> DataDict { get; set; } = [];

    private Exist(Dictionary<string, List<object>> dataDict, string key)
    {
        DataDict = dataDict;
        Key = key;
    }

    private string Key
    {
        get => _key;
        set => _key = !string.IsNullOrEmpty(value) ? value : string.Empty;
    }

    internal static Exist ExistCommand(Dictionary<string, List<object>> dataDict, string key)
    {
        var exist = new Exist(dataDict, key);
        return exist;

    }

    internal bool ExistingAirline()
    {
        foreach (var kvp in DataDict)
        {
            foreach (var item in kvp.Value)
            {
                if (item is Airline airline && airline.Name.Equals(Key, StringComparison.CurrentCultureIgnoreCase))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void Execute() => Console.WriteLine(ExistingAirline());
}
