﻿namespace Airlines.Business.Commands.Search;
public class Search : ICommand
{
    private string _targetValue = string.Empty;

    private Search(Dictionary<string, List<object>> dict, string targetValue)
    {
        SearchDict = dict;
        TargetValue = targetValue;

    }

    internal static Search SearchCommand(Dictionary<string, List<object>> dict, string targetValue)
    {
        var search = new Search(dict, targetValue);

        return search;
    }

    private Dictionary<string, List<object>> SearchDict { get; set; } = [];

    public string TargetValue
    {
        get => _targetValue;
        set => _targetValue = !string.IsNullOrEmpty(value) ? value : string.Empty;
    }

    internal string SearchInput()
    {
        foreach (var kvp in SearchDict)
        {
            foreach (var item in kvp.Value)
            {
                if (item != null)
                {
                    var v = item.ToString();
                    if (v != null && v.Contains(TargetValue, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return kvp.Key;
                    }
                }
            }
        }

        return string.Empty;
    }

    public void Execute()
    {
        if (!string.IsNullOrEmpty(TargetValue) && !string.IsNullOrEmpty(SearchInput()))
        {
            Console.WriteLine(SearchInput());
        }
        else
        {
            Console.WriteLine("String term is missing!");
        }
    }
}
