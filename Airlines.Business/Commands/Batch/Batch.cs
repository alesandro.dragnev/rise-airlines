﻿using Airlines.Persistence.Aircrafts;
using Airlines.Business.Commands.Reserve;
using Airlines.Business.Commands.Route;
using Airlines.Persistence.Basic.Exceptions;
using Airlines.OLD.Persistence.Basic.Flight;
using Airlines.Persistence.Basic.Airports;

namespace Airlines.Business.Commands.Batch;
public class Batch
{
    public static readonly Queue<string> BatchQueue = [];

    public Batch()
    {

    }

    internal static void AddCommand(string command) => BatchQueue.Enqueue(command);

    internal static void RunBatch(Dictionary<string, List<object>> dict)
    {
        while (BatchQueue.Count > 0)
        {
            var command = BatchQueue.Dequeue();
            _ = ProcessCommand(dict, command);
        }
    }

    internal static void CancelBatch() => BatchQueue.Clear();

    internal static bool ProcessCommand(Dictionary<string, List<object>> dict, string command)
    {
        var flights = dict["flights"].Cast<Flight>().ToList();
        var cargoAircrafts = dict["cargo"].Cast<CargoAircraft>().ToList();
        var passengerAircrafts = dict["passenger"].Cast<PassengerAircraft>().ToList();
        var airportList = dict["airports"].Cast<Airport>().ToList();
        var tokens = command.Split(' ');
        var action = tokens[0].ToLower();

        switch (action)
        {
            case "search":
                Search.Search.SearchCommand(dict, string.Join(" ", tokens[1..])).Execute();
                break;
            case "sort":
                Sort.Sort.SortCommand(dict, tokens[1], tokens[2]).Execute();
                break;
            case "exist":
                Exist.Exist.ExistCommand(dict, tokens[1]).Execute();
                break;
            case "list":
                List.List.ListCommand(airportList, tokens[1], tokens[2]).Execute();
                break;
            case "route":
                ProcessRouteCommand(tokens, flights);
                break;
            case "reserve":
                ProcessReserveCommand(tokens, flights, cargoAircrafts, passengerAircrafts);
                break;
            case "done":
                return false;
            default:
                throw new InvalidInputException("Invalid Input!");
        }

        return true;
    }

    internal static void ProcessRouteCommand(string[] tokens, List<Flight> flights)
    {
        switch (tokens[1])
        {
            case "add":
                RouteAddCommand.AddRouteCommand(tokens[2], flights).Execute();
                break;
            case "remove":
                RouteRemoveCommand.RemoveRouteCommand(flights).Execute();
                break;
            case "print":
                RoutePrintCommand.PrintRouteCommand(flights).Execute();
                break;
            case "new":
                RouteNewCommand.NewRouteCommand(flights).Execute();
                break;
            default:
                throw new InvalidInputException("Invalid Route Command!");
        }
    }

    internal static void ProcessReserveCommand(string[] tokens, List<Flight> flights,
        List<CargoAircraft> cargoAircrafts, List<PassengerAircraft> passengerAircrafts)
    {
        if (tokens.Length < 6)
        {
            throw new InvalidInputException("Invalid command parameters!");
        }
        switch (tokens[1])
        {
            case "cargo":
                ReserveCargo.ReserveCargoCommand(flights, tokens[2], cargoAircrafts, double.Parse(tokens[3]),
                    double.Parse(tokens[4]), "cargo").Execute();
                break;
            case "ticket":
                ReserveTicket.ReserveTicketCommand(flights, tokens[2], passengerAircrafts, double.Parse(tokens[4]),
                    double.Parse(tokens[5]), int.Parse(tokens[3]), "passenger").Execute();
                break;
            default:
                throw new InvalidInputException("Invalid command parameters!");
        }
    }
}