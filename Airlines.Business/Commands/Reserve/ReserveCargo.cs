﻿using Airlines.Persistence.Basic.Exceptions;
using Airlines.OLD.Persistence.Basic.Flight;
using Airlines.Persistence.Aircrafts;

namespace Airlines.Business.Commands.Reserve;
public class ReserveCargo : Reserve, ICommand
{
    private readonly List<CargoAircraft> _aircraftList = [];
    private double _cargoWeight;
    private double _cargoVolume;
    private string _type = string.Empty;

    private ReserveCargo(List<Flight> flights, string flightId, List<CargoAircraft> aircrafts,
        double cargoWeight, double cargoVolume, string type) : base(flights, flightId)
    {
        _aircraftList = aircrafts;
        CargoWeight = cargoWeight;
        CargoVolume = cargoVolume;
        Type = type;
    }

    internal static ReserveCargo ReserveCargoCommand(List<Flight> flights, string flightId, List<CargoAircraft> aircrafts,
        double cargoWeight, double cargoVolume, string type)
    {
        var cargo = new ReserveCargo(flights, flightId, aircrafts, cargoWeight, cargoVolume, type);
        return cargo;

    }

    private string Type
    {
        get => _type;
        set => _type = !string.IsNullOrEmpty(value) ? value.ToLower() : string.Empty;
    }
    private double CargoWeight
    {
        get => _cargoWeight;
        set => _cargoWeight = value is > 0 and not 0 ? value : 0;
    }
    private double CargoVolume
    {
        get => _cargoVolume;
        set => _cargoVolume = value is > 0 and not 0 ? value : 0;
    }

    internal bool IsAircraftCargoPossible()
    {
        try
        {
            foreach (var flight in _flights)
            {
                if (flight.Id.Equals(FlightID.ToUpper()))
                {
                    if (Type == "cargo")
                    {
                        foreach (var aircraft in _aircraftList)
                        {
                            return flight.Aircraft.Contains(aircraft.Model) && aircraft.CargoWeight > CargoWeight
                                && aircraft.CargoVolume > CargoVolume ? true
                                : throw new InvalidInputException($"Aircraft is not {Type}. Imposible!");
                        }
                    }
                }
            }
        }
        catch (InvalidInputException)
        {

        }
        return false;
    }

    public void Execute() => IsAircraftCargoPossible();
}
