﻿using Airlines.OLD.Persistence.Basic.Flight;

namespace Airlines.Business.Commands.Reserve;
public class Reserve
{
    protected static string _flightId = string.Empty;
    protected List<Flight> _flights = [];

    protected Reserve(List<Flight> flights, string flightId)
    {
        _flights = flights;
        FlightID = flightId;
    }

    protected static string FlightID
    {
        get => _flightId;
        set => _flightId = !string.IsNullOrEmpty(value) ? value.ToLower() : string.Empty;
    }

    internal static bool BaggageCalculation(double smallBaggageCount, double largeBaggageCount)
    {
        var smallBaggageMaxWeight = 15;
        var largeBaggageMaxWeight = 30;
        /*        var smallBaggageMaxVolume = 0.045;
                var largeBaggageMaxVolume = 0.090;*/


        return smallBaggageCount < smallBaggageMaxWeight && largeBaggageMaxWeight > largeBaggageCount;
    }
}
