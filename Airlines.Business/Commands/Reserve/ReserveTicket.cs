﻿using Airlines.Persistence.Basic.Exceptions;
using Airlines.OLD.Persistence.Basic.Flight;
using Airlines.Persistence.Aircrafts;

namespace Airlines.Business.Commands.Reserve;
public class ReserveTicket : Reserve, ICommand
{
    private readonly List<PassengerAircraft> _aircraftList = [];
    private double _smallBaggageCount;
    private double _largeBaggageCount;
    private int _seats;
    private string _type = string.Empty;

    private ReserveTicket(List<Flight> flights, string flightId, List<PassengerAircraft> aircrafts, double smallBaggageCount, double largeBaggageCount, int seats, string type) : base(flights, flightId)
    {
        _aircraftList = aircrafts;
        SmallBaggage = smallBaggageCount;
        LargeBaggage = largeBaggageCount;
        Seats = seats;
        Type = type;
    }

    internal static ReserveTicket ReserveTicketCommand(List<Flight> flights, string flightId, List<PassengerAircraft> aircrafts, double smallBaggageCount, double largeBaggageCount, int seats, string type)
    {
        var ticket = new ReserveTicket(flights, flightId, aircrafts, smallBaggageCount, largeBaggageCount, seats, type);

        return ticket;

    }

    private string Type
    {
        get => _type;
        set => _type = !string.IsNullOrEmpty(value) ? value.ToLower() : string.Empty;
    }
    private double SmallBaggage
    {
        get => _smallBaggageCount;
        set => _smallBaggageCount = value is > 0 and not 0 ? value : 0;
    }
    private double LargeBaggage
    {
        get => _largeBaggageCount;
        set => _largeBaggageCount = value is > 0 and not 0 ? value : 0;
    }
    private int Seats
    {
        get => _seats;
        set => _seats = value is > 0 and not 0 ? value : 0;
    }

    internal bool IsAircraftPassengerPossible()
    {
        try
        {
            foreach (var flight in _flights)
            {
                if (flight.Id.Equals(FlightID.ToUpper()))
                {
                    if (Type == "passenger")
                    {
                        foreach (var aircraft in _aircraftList)
                        {
                            return flight.Aircraft.Contains(aircraft.Model) && BaggageCalculation(SmallBaggage, LargeBaggage) && Seats > 0
                                ? true
                                : throw new InvalidInputException($"Aircraft is not {Type}. Impossible!");
                        }
                    }
                }
            }
        }
        catch (InvalidInputException)
        {
        }
        return false;
    }

    public void Execute() => IsAircraftPassengerPossible();
}