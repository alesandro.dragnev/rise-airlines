﻿using Airlines.Persistence.Basic.Airports;

namespace Airlines.Business.Commands.List;
public class List : ICommand
{
    private string _key = string.Empty;
    private string _toSearch = string.Empty;
    private List<Airport> DataList { get; set; } = [];

    private List(List<Airport> dataList, string toSearch, string key)
    {
        DataList = dataList;
        Key = key;
        ToSearch = toSearch;
    }

    internal static List ListCommand(List<Airport> dataList, string toSearch, string key)
    {
        var list = new List(dataList, toSearch, key);

        return list;

    }

    private string Key
    {
        get => _key;
        set => _key = !string.IsNullOrEmpty(value) ? value.ToLower() : string.Empty;
    }

    private string ToSearch
    {
        get => _toSearch;
        set => _toSearch = !string.IsNullOrEmpty(value) ? value.Replace("_", " ").ToLower() : string.Empty;
    }

    internal string ListAirport()
    {
        if (Key == "city")
        {
            foreach (var airport in DataList)
            {
                if (airport.City.Contains(ToSearch, StringComparison.CurrentCultureIgnoreCase))
                {
                    return airport.Name;
                }
            }
        }
        else if (Key == "country")
        {
            foreach (var airport in DataList)
            {
                if (airport.Country.Contains(ToSearch, StringComparison.CurrentCultureIgnoreCase))
                {
                    return airport.Name;
                }
            }
        }
        return "Not existing list!";

    }

    public void Execute() => Console.WriteLine(ListAirport());
}
