﻿namespace Airlines.Business;
public class SearchingExtension
{
    internal static string? BinarySearch(string[] array, string searchString)
    {
        var min = 0; // start of array
        var max = array.Length - 1; // last element position

        while (min <= max)
        {
            var mid = (min + max) / 2;

            var comparisonResult = string.Compare(array[mid], searchString);

            if (comparisonResult == 0)
            {
                // if element is in the middle
                return array[mid];
            }
            else if (comparisonResult < 0)
            {
                // if element is in the right
                min = mid + 1;
            }
            else
            {
                // if element is the left
                max = mid - 1;
            }
        }

        // return null, because the func is string
        return null;
    }

    internal static string? LinearSearch(string[] array, string searchString)
    {
        for (var i = 0; i < array.Length; i++)
        {
            if (searchString == array[i])
            {
                return array[i];
            }
        }

        return null;
    }
}
