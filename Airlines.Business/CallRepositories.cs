﻿using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repositories;

namespace Airlines.Business;

public class CallRepositories(AirportRepository airportRepository, AirlineRepository airlineRepository, FlightRepository flightRepository) : IDisposable
{
    private readonly AirportRepository _airportRepository = airportRepository;
    private readonly AirlineRepository _airlineRepository = airlineRepository;
    private readonly FlightRepository _flightRepository = flightRepository;

    internal List<Airport> GetAirports()
    {
        var airports = _airportRepository.GetAllAirports();
        return airports;
    }

    internal List<Airline> GetAirlines()
    {
        var airlines = _airlineRepository.GetAllAirlines();
        return airlines;
    }

    internal List<Flight> GetFlights()
    {
        var flights = _flightRepository.GetAllFlights();
        return flights;
    }

    /*    internal void PrintData()
        {
            var airlineService = new AirlineService(_airlineRepository, new AirlineMapper());
            airlineService.PrintAirlines();

            var airportService = new AirportService(_airportRepository);
            airportService.PrintAirports();

            var flightService = new FlightService(_flightRepository, new FlightMapper());
            flightService.PrintFlights();
        }*/

    public void Dispose()
    {
    }
}

