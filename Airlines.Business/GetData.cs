﻿using Airlines.Persistence;
using Airlines.Persistence.Basic.Airlines;
using Airlines.Persistence.Basic.Airports;
using Airlines.OLD.Persistence.Basic.Flight;
using FileReaders;

namespace Airlines.Business;
public class GetData
{
    public readonly List<Airport> airports;
    public readonly List<Airline> airlines;
    public readonly List<Flight> flights;
    public readonly Dictionary<string, List<object>> aircrafts;
    public readonly Dictionary<string, List<object>> listOfObjects;
#pragma warning disable CS8618
    public static Graph graph;
    public static Tree tree;
#pragma warning restore CS8618

    public GetData()
    {
        airports = FileReader.ReadAirportsFile(@"../../../FilesToRead/Airports.csv");
        airlines = FileReader.ReadAirlinesFile(@"../../../FilesToRead/Airlines.csv");
        flights = FileReader.ReadFlightsFile(@"../../../FilesToRead/Flights.csv");
        aircrafts = FileReader.ReadAircraftsFile(@"../../../FilesToRead/Aircrafts.csv");
        graph = FileReader.ReadFlightsGraphFile(@"../../../FilesToRead/Flights.csv");
        tree = FileReader.ReadFlightsRouteFile(@"../../../FilesToRead/FlightRoute.csv", flights);

        listOfObjects = new Dictionary<string, List<object>>
        {
            { "airports", airports.Cast<object>().ToList() },
            { "airlines", airlines.Cast<object>().ToList() },
            { "flights", flights.Cast<object>().ToList() },
            { "cargo", aircrafts["cargo"].Cast<object>().ToList() },
            { "passenger", aircrafts["passenger"].Cast<object>().ToList() },
            { "private", aircrafts["private"].Cast<object>().ToList() }
        };
    }
}
