﻿using Airlines.Business.DTOs;
using Airlines.Persistence.Basic.Exceptions;

namespace Airlines.Business.Validation;
public class AirlinesValidator
{
    public static bool ValidateAirline(AirlineDTO airline)
    {
        try
        {
            return !AirlineValidation(airline) ? throw new InvalidAirlineNameException("Airline validation failed.") : true;
        }
        catch (InvalidAirlineNameException ex)
        {
            Console.WriteLine($"{ex.Message}");
            return false;
        }
    }

    public static bool AirlineValidation(AirlineDTO airlineDTO)
    => airlineDTO.Name.Length < 6 && !string.IsNullOrEmpty(airlineDTO.Description) && airlineDTO.Fleet_size > 0 && airlineDTO.Founded is DateOnly;
}
