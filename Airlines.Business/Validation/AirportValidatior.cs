﻿using Airlines.Business.DTOs;
using Airlines.Persistence.Basic.Exceptions;

namespace Airlines.Business.Validation;
public class AirportValidator
{
    public static bool ValidateAirport(AirportDTO airportDTO)
    {
        try
        {
            return AirportValidation(airportDTO.Code) && NameValidation(airportDTO.Name) && CityValidation(airportDTO.City)
                && CountryValidation(airportDTO.Country)
                ? true
                : throw new InvalidAirportException("Airline validation failed.");

        }
        catch (InvalidAirportException ex)
        {
            Console.WriteLine($"{ex.Message}");
            return false;
        }
    }

    public static bool AirportValidation(string check)
    => check.All(char.IsLetter) && check.Length >= 2 && check.Length <= 4 && check != null;
    public static bool NameValidation(string check)
        => !check.All(char.IsDigit) && check != null;
    public static bool CityValidation(string check)
        => !check.All(char.IsDigit) && check != null;
    public static bool CountryValidation(string check)
        => !string.IsNullOrEmpty(check) && !check.All(char.IsDigit);

}
