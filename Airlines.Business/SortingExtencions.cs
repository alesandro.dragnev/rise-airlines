﻿namespace Airlines.Business;
public static class SortingExtension
{
    internal static List<T> SortAscending<T>(this List<T> list, Func<T, string> keySelector) => [.. list.OrderBy(keySelector)];

    internal static List<T> SortDecending<T>(this List<T> list, Func<T, string> keySelector) => [.. list.OrderByDescending(keySelector)];
}
