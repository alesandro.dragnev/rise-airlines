﻿namespace Airlines.Business.DTOs;
public class FlightDTO
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public FlightDTO()
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    {

    }
    public FlightDTO(string flightNumber, string airline, int? departurAirportId, int? arrivalAirportId,
        DateTime? departureDateTime, DateTime? arrivalDateTime, AirportDTO arrival, AirportDTO departure)
    {
        Flight_Number = flightNumber;
        Airline = airline;
        DepartureID = departurAirportId;
        ArrivalID = arrivalAirportId;
        Departure_date = departureDateTime;
        Arrival_date = arrivalDateTime;
        Departure = departure;
        Arrival = arrival;
    }

    public int ID { get; set; }
#pragma warning disable CA1707 // Identifiers should not contain underscores
    public string Flight_Number { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
    public string Airline { get; set; }
    public int? DepartureID { get; set; }
    public int? ArrivalID { get; set; }
#pragma warning disable CA1707 // Identifiers should not contain underscores
    public DateTime? Departure_date { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
#pragma warning disable CA1707 // Identifiers should not contain underscores
    public DateTime? Arrival_date { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
    public virtual AirportDTO? Arrival { get; set; }
    public virtual AirportDTO? Departure { get; set; }
}
