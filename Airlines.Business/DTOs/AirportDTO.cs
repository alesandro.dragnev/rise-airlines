﻿namespace Airlines.Business.DTOs;
public class AirportDTO
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public AirportDTO()
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    {
    }
    public AirportDTO(string name, string country, string city, string code, int? runwaysCount, DateOnly founded)
    {
        Name = name;
        Country = country;
        City = city;
        Code = code;
        Runways_count = runwaysCount;
        Founded = founded;
    }

    public int ID { get; set; }
    public string Name { get; set; }
    public string Country { get; set; }
    public string City { get; set; }
    public string Code { get; set; }
#pragma warning disable CA1707 // Identifiers should not contain underscores
    public int? Runways_count { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
    public DateOnly Founded { get; set; }
}

