# Airline Project

##### Alesandro Dragnev RISE - 2024


## Project Tasks
- Task 1: We need to create an airline system that contains data for airports, airlines and flights. At the start of the project it must be a console application ([Branch Task-1-Airlines-Project-Data](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-1-Airlines-Project-Data?ref_type=heads))
- Task 2: Integrating the project into GitLab 
- Task 3: Implementing a build pipeline
- Task 4: Implementing searching and sorting algorithms (Selection Sort, Bubble Sort, Binary Search) ([Branch Task-4-Airlines-Project-Data](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-4-Airlines-Project-Data?ref_type=heads))
- Task 5: Unit Testing using xUnit ([Branch Task-5-Airlines-Project-Data](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-5-Airlines-Project-Data?ref_type=heads))
- Task 6: Integrating Unit tests into CI Pipeline ([Branch Task-6-Airlines-Project-Data](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-6-Airlines-Project-Data?ref_type=heads))
- Task 7: Refactoring and Static Code Analysis Integration ([Branch Task-7-Airlines-Project-Data](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-7-Airlines-Project-Data?ref_type=heads))
- Task 8: Implementing OOP Concepts and Console Commands ([Branch Task-8-Airlines-Project-Data](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-8-Airlines-Project-Data?ref_type=heads))
- Task 9: Establishing the Business Layer ([Branch Task-9-Business-Layer-Establishment](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-9-Business-Layer-Establishment?ref_type=heads))
- Task 10: Refactoring Data Structures with .NET Collections and LINQ ([Branch Task-10-Refactoring-Data-Structures-LINQ](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-10-Refactoring-Data-Structures-LINQ?ref_type=heads))
- Task 11: Flight Route Management ([Branch Task-11-Flight-Route-Management](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-11-Flight-Route-Management?ref_type=heads))
- Task 12: Efficient Release of File Resources ([Branch Task-12-Efficient-Release](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-12-Efficient-Release?ref_type=heads))
- Task 13: Flight Reservation Features ([Branch Task-13-Flight-Reservation-Features](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-13-Flight-Reservation-Features?ref_type=heads))
- Task 14: Command Batch execution ([Branch Task-14-Command-Batch-Execution](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-14-Command-Batch-Execution?ref_type=heads))
- Task 15: Advanced Error Handling in the Airlines Project ([Branch Task-15-Advanced-Error-Handling](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-15-Advanced-Error-Handling?ref_type=heads))
- Task 16: Flight Route Finder Implementation ([Branch Task-16-Flight-Route-Finder](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-16-Flight-Route-Finder?ref_type=heads))
- Task 17: Testing the Flight Route Finder ([Branch Task-17-Testing-Flight-Route-Finder](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-17-Testing-Flight-Route-Finder?ref_type=heads))
- Task 18: Flight Network ([Branch Task-18-Flight-Networkt](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-18-Flight-Network?ref_type=heads))
- Task 19: Enhanced Flight Route Search ([Branch Task-19-Enhanced-Flight-Route-Search](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-19-Enhanced-Flight-Route-Search?ref_type=heads))
- Task 20: Establishing Persistence Layer ([Branch Task-20-Establishing-Persistence-Layer](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-20-Establishing-Persistence-Layer?ref_type=heads))
- Task 22: Airlines Database Create Table ([Branch Task-22-Airlines-Database-Create-Tables](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-22-Airlines-Database-Create-Tables?ref_type=heads))
- Task 23: Airlines Database PK-Foreign-Key Constraint Indexes performance ([Branch Task-23-Airlines-Database-PK-Foreign-Key-Constraint-Indexes-performance](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-23-Airlines-Database-PK-Foreign-Key-Constraint-Indexes-performance?ref_type=heads))
- Task 24: Airlines Database Join Union Aggregate ([Branch Task-24-Airlines-Database-Join-Union-Aggregate](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-24-Airlines-Database-Join-Union-Aggregate?ref_type=heads))
- Task 25: Airlines Database ORM Generate model ([Task-25-Airlines-Database-ORM-Generate-model](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-25-Airlines-Database-ORM-Generate-model?ref_type=heads))
- Task 26: Airlines Database ORM implement methods ([Branch Task-26-Airlines-Database-ORM-implement-methods](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-26-Airlines-Database-ORM-implement-methods?ref_type=heads))
- Task 27: Mock Testing ([Branch Task-27-Mock-Testing](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-27-Mock-Testing?ref_type=heads))
- Task 28: WEB Creating Basic HTML Pages([Branch Task-28-WEB-Creating-Basic-HTML-Pages](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-28-WEB-Creating-Basic-HTML-Pages?ref_type=heads))
- Task 29: WEB Styling HTML ([Branch Task-29-WEB-Styling-HTML](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-29-WEB-Styling-HTML?ref_type=heads))
- Task 30: WEB Adapting Pages MVC Controller Connection ([Branch Task-30-WEB-Adapting-Pages-MVC-Controller-Connection](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-30-WEB-Adapting-Pages-MVC-Controller-Connection?ref_type=heads))
- Task 31: Docker Container ([Branch Task-31-Docker-Container](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-31-Docker-Container?ref_type=heads))
- Task 32: WEB Enhance MVC functionality ([Branch Task-32-WEB-Enhance-MVC-functionality](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-32-WEB-Enhance-MVC-functionality?ref_type=heads))
- Task 33: WEB Implementing basic JavaScript Client validation Animation ([Branch Task-33-WEB-Implementing-basic-JavaScript-Client-validation-Animation](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-33-WEB-Implementing-basic-JavaScript-Client-validation-Animation?ref_type=heads))
- Task 34: WEB Decoupling Flight Route Management ASP.NET Web API Airlines ([Branch Task-34-WEB-Decoupling-Flight-Route-Management-ASP.NET-Web-API-Airlines](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-34-WEB-Decoupling-Flight-Route-Management-ASP.NET-Web-API-Airlines?ref_type=heads))
- Task 35: WEB Implementing JS HTTP requests async programming Airlines Project ([Branch Task-35-WEB-Implementing-JS-HTTP-requests-async-programming-Airlines-Project](https://gitlab.com/alesandro.dragnev/rise-airlines/-/tree/Task-35-WEB-Implementing-JS-HTTP-requests-async-programming-Airlines-Project?ref_type=heads))