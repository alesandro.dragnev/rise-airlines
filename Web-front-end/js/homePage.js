import { getOnlyAPI } from "./httpRequests.js";

const airport = document.getElementById('airport-count');
const airline = document.getElementById('airline-count');
const flight = document.getElementById('flight-count');

const flights = await getOnlyAPI("https://localhost:7153/Flight");
const flightCount = flights.length;
flight.innerText = flightCount;


const airlines= await getOnlyAPI("https://localhost:7153/Airline");
const airlineCount = airlines.length;
airline.innerText = airlineCount;


const airports = await getOnlyAPI("https://localhost:7153/Airport");
const airportsCount = airports.length;
airport.innerText = airportsCount;