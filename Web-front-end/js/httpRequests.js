let dataList = [];
const flightProperties = [
  "flight_Number",
  "airline",
  "departure",
  "arrival",
  "arrival_date",
  "departure_date",
];
const airlineProperties = ["name", "fleet_size", "founded", "description"];
const airportProperties = [
  "name",
  "country",
  "city",
  "code",
  "runways_count",
  "founded",
];

// http requests
export async function getFromAPI(link, render) {
  fetch(link, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .then((data) => {
      dataList = data;
      render();
    })
    .catch((error) => console.error("Error:", error));
}

export function postToAPI(link, data) {
  fetch(link, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      console.log("Success!");
    })
    .catch((error) => console.error("Error:", error));
}

export function deleteFromAPI(link) {
  fetch(link, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      } else {
        console.log("Success!");
      }
    })
    .catch((error) => console.error("Error:", error));
}
export function UpdateAPI(link, data) {
  fetch(link, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      } else {
        console.log("Success!");
      }
    })
    .catch((error) => console.error("Error:", error));
}

export async function getOnlyAPI(link) {
  try {
    const response = await fetch(link, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    const data = await response.json();
    console.log("Data fetched from API:", data);
    return data;
  } catch (error) {
    console.error("Error:", error);
    throw error;
  }
}

// buttons for edit & delete
function createDeleteButton(link, obj) {
  const deleteButton = document.createElement("button");
  deleteButton.innerHTML = "Delete";
  deleteButton.id = obj.id;
  deleteButton.addEventListener("click", function (event) {
    deleteFromAPI(link);
    location.reload();
  });
  deleteButton.classList.add("button");
  return deleteButton;
}

function createEditButton(obj, props) {
  const editButton = document.createElement("button");
  editButton.innerHTML = "Edit";
  editButton.id = obj.id;
  editButton.addEventListener("click", function (event) {
    event.preventDefault();
    editData(obj, props);
  });
  editButton.classList.add("button");
  return editButton;
}

// Edit func
function editData(data, props) {
  const form = document.getElementById("form");
  const table = document.getElementById("table");
  const searchBar = document.getElementById("search-bar");
  const buttonShowMore = document.getElementById("show-more");
  const legend = form.querySelector("legend");
  legend.innerText = "Edit Row";
  table.classList.add("hidden");
  searchBar.classList.add("hidden");
  buttonShowMore.classList.add("hidden");
  form.classList.remove("hidden");

  let hiddenInput = form.querySelector("input[name='id']");
  if (!hiddenInput) {
    hiddenInput = document.createElement("input");
    hiddenInput.type = "hidden";
    hiddenInput.name = "id";
    form.appendChild(hiddenInput);
  }
  hiddenInput.value = data.id;

  for (const prop of props) {
    const input = form.querySelector(`[name=${prop}]`);
    if (prop === "arrival" || prop === "departure") {
      input.value = data[prop].code;
    } else {
      console.log(data[prop], prop);
      input.value = data[prop];
    }
  }
}

// create rows for tables
export function createFlightRow(obj) {
  const tableRow = document.createElement("tr");
  const deleteButton = createDeleteButton(
    `https://localhost:7153/Flight?id=${obj.id}`,
    obj
  );
  const editButton = createEditButton(obj, flightProperties);

  for (const prop of flightProperties) {
    const tableData = document.createElement("td");
    tableData.classList.add("table-row-tag");
    if (prop === "departure" || prop === "arrival") {
      tableData.textContent = obj[prop]?.code ?? "N/A";
    } else {
      tableData.textContent = obj[prop];
    }
    tableRow.appendChild(tableData);
  }

  const buttonTableData = document.createElement("td");
  buttonTableData.classList.add("table-row-tag");
  buttonTableData.appendChild(deleteButton);
  buttonTableData.appendChild(editButton);
  tableRow.appendChild(buttonTableData);

  return tableRow;
}

export function createAirlineRow(obj) {
  const tableRow = document.createElement("tr");
  const deleteButton = createDeleteButton(
    `https://localhost:7153/Airline?id=${obj.id}`,
    obj
  );
  const editButton = createEditButton(obj, airlineProperties);

  for (const prop of airlineProperties) {
    const tableData = document.createElement("td");
    tableData.classList.add("table-row-tag");
    if (prop === "description") {
      const description = document.createElement("div");
      description.classList.add("description-content");
      description.textContent = obj[prop];
      tableData.classList.add("description-cell");
      tableData.appendChild(description);
    } else {
      tableData.textContent = obj[prop];
    }
    tableRow.appendChild(tableData);
  }

  const buttonTableData = document.createElement("td");
  buttonTableData.classList.add("table-row-tag");
  buttonTableData.appendChild(deleteButton);
  buttonTableData.appendChild(editButton);
  tableRow.appendChild(buttonTableData);

  return tableRow;
}

export function createAirportRow(obj) {
  const tableRow = document.createElement("tr");
  const deleteButton = createDeleteButton(
    `https://localhost:7153/Airport?id=${obj.id}`,
    obj
  );
  const editButton = createEditButton(obj, airportProperties);

  for (const prop of airportProperties) {
    const tableData = document.createElement("td");
    tableData.classList.add("table-row-tag");
    tableData.textContent = obj[prop];

    tableRow.appendChild(tableData);
  }

  const buttonTableData = document.createElement("td");
  buttonTableData.classList.add("table-row-tag");
  buttonTableData.appendChild(deleteButton);
  buttonTableData.appendChild(editButton);
  tableRow.appendChild(buttonTableData);

  return tableRow;
}

// render the objects
export function renderFlights() {
  const objTableBody = document.getElementById("table-body");

  objTableBody.innerHTML = "";

  dataList.forEach((obj) => {
    const row = createFlightRow(obj);
    objTableBody.appendChild(row);
  });
}

export function renderAirports() {
  const objTableBody = document.getElementById("table-body");

  objTableBody.innerHTML = "";

  dataList.forEach((obj) => {
    const row = createAirportRow(obj);
    objTableBody.appendChild(row);
  });
}

export function renderAirlines() {
  const objTableBody = document.getElementById("table-body");

  objTableBody.innerHTML = "";

  dataList.forEach((obj) => {
    const row = createAirlineRow(obj);
    objTableBody.appendChild(row);
  });
}
