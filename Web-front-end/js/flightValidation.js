import {
  getFromAPI,
  postToAPI,
  deleteFromAPI,
  UpdateAPI,
  renderFlights,
  getOnlyAPI,
} from "./httpRequests.js";

await getFromAPI("https://localhost:7153/Flight", renderFlights);

const submitButton = document.getElementById("submit-button");
const flightValid = document.getElementById("flight-number-valid");
const airlineValid = document.getElementById("airline-valid");
const departureValid = document.getElementById("departure-code-valid");
const arrivalValid = document.getElementById("arrival-code-valid");
const departureDateValid = document.getElementById("departure-date-valid");
const arrivalDateValid = document.getElementById("arrival-date-valid");
const flightNumber = document.getElementById("flight_number");
const departureDate = document.getElementById("departure_date");
const arrivalDate = document.getElementById("arrival_date");
const arrival = document.getElementById("arrival");
const departure = document.getElementById("departure");
const airline = document.getElementById("airline");
const searchForm = document.getElementById("search-form");

function clearTableRows(tableId) {
  const table = document.getElementById(tableId);
  const tbody = table.getElementsByTagName("tbody")[0];

  while (tbody.rows.length > 0) {
    tbody.deleteRow(0);
  }
}

function disableButton() {
  submitButton.disabled = true;
  submitButton.style.backgroundColor = "#E5E5E5";
}

function enableButton() {
  submitButton.disabled = false;
  submitButton.style.backgroundColor = "#FCA311";
  submitButton.style.cursor = "pointer";
}

function checkFlightNumber() {
  const flightNumberValue = flightNumber.value.trim();
  flightValid.classList.toggle("hidden", flightNumberValue.length !== 5);
  return flightNumberValue;
}

function checkAirline() {
  const airlineValue = airline.value.trim();
  airlineValid.classList.toggle("hidden", airlineValue.length !== "");
  return airlineValue;
}

function checkDepartureCode() {
  const departureValue = departure.value.trim();
  departureValid.classList.toggle("hidden", departureValue.length !== "");
  return departureValue;
}

function checkArrivalCode() {
  const arrivalValue = arrival.value.trim();
  arrivalValid.classList.toggle(
    "hidden",
    arrivalValue.length >= 1 && arrivalValue.length <= 3
  );
  return arrivalValue;
}

function checkDepartureDate() {
  const departureDateValue = new Date(departureDate.value.trim()).getTime();
  const arrivalDateValue = new Date(arrivalDate.value.trim()).getTime();
  departureValid.classList.toggle(
    "hidden",
    departureDateValue >= arrivalDateValue
  );
  return departureDateValue;
}

function checkArrivalDate() {
  const departureDateValue = new Date(departureDate.value.trim()).getTime();
  const arrivalDateValue = new Date(arrivalDate.value.trim()).getTime();
  arrivalValid.classList.toggle(
    "hidden",
    departureDateValue >= arrivalDateValue
  );
  return arrivalDate.value.trim();
}

function checkFormValidity() {
  event.preventDefault();
  if (
    checkFlightNumber() &&
    checkAirline() &&
    checkDepartureCode() &&
    checkArrivalCode() &&
    checkDepartureDate() &&
    checkArrivalDate()
  ) {
    enableButton();
  } else {
    disableButton();
  }
}

disableButton();

flightNumber.addEventListener("input", checkFormValidity);
departureDate.addEventListener("input", checkFormValidity);
arrivalDate.addEventListener("input", checkFormValidity);

const form = document.getElementById("form");
form.addEventListener("submit", function (event) {
  const legend = document.getElementById("form").querySelector("legend");
  event.preventDefault();
  if (legend.innerText === "Add new Flight") {
    let data = {
      flight_Number: flightNumber.value.trim(),
      airline: airline.value.trim(),
      departure_date: departureDate.value.trim(),
      arrival_date: arrivalDate.value.trim(),
    };
    postToAPI(
      `https://localhost:7153/Flight?departure=${departure.value.trim()}&arrival=${arrival.value.trim()}`,
      data
    );
  } else {
    let id = document.querySelector("[name='id']").value;
    let updatedData = {
      flight_Number: flightNumber.value.trim(),
      airline: airline.value.trim(),
      departure_date: departureDate.value.trim(),
      arrival_date: arrivalDate.value.trim(),
    };
    UpdateAPI(
      `https://localhost:7153/Flight?id=${id}&departure=${departure.value.trim()}&arrival=${arrival.value.trim()}`,
      updatedData
    );
  }
  location.reload();
});

searchForm.addEventListener("submit", function (event) {
  event.preventDefault();
  const filter = searchForm.querySelector('select[name="filter"]').value;
  const value = searchForm.querySelector('input[name="value"]').value;
  console.log("Form Data:", filter, value);
  clearTableRows("table");
  getFromAPI(
    `https://localhost:7153/Flight/search?filter=${filter.trim()}&value=${value.trim()}`,
    renderFlights
  );
});
