document.addEventListener('DOMContentLoaded', function () {
    var loadingOverlay = document.getElementById('loading-overlay');
    loadingOverlay.style.display = 'block';
    setTimeout(function () {
        loadingOverlay.style.opacity = '0';
        setTimeout(function () {
            loadingOverlay.style.display = 'none';
        }, 500);
    }, 1000);
});