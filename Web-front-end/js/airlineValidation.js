import {
  getFromAPI,
  postToAPI,
  UpdateAPI,
  renderAirlines,
} from "./httpRequests.js";

await getFromAPI("https://localhost:7153/Airline", renderAirlines);

const submitButton = document.getElementById("submit-button");
const nameValid = document.getElementById("name-valid");
const fleetValid = document.getElementById("fleet-valid");
const descriptionValid = document.getElementById("description-valid");
const foundedValid = document.getElementById("founded-valid");
const name = document.getElementById("name");
const fleet = document.getElementById("fleet_size");
const description = document.getElementById("description");
const founded = document.getElementById("founded");
const form = document.getElementById("form");
const searchForm = document.getElementById("search-form");

function clearTableRows(tableId) {
  const table = document.getElementById(tableId);
  const tbody = table.getElementsByTagName("tbody")[0];

  while (tbody.rows.length > 0) {
    tbody.deleteRow(0);
  }
}

function disableButton() {
  submitButton.disabled = true;
  submitButton.style.backgroundColor = "#E5E5E5";
}

function enableButton() {
  submitButton.disabled = false;
  submitButton.style.backgroundColor = "#FCA311";
  submitButton.style.cursor = "pointer";
}

function checkName() {
  const nameValue = name.value.trim();
  nameValid.classList.toggle("hidden", nameValue !== "");
  return nameValue !== "";
}

function checkFleet() {
  const fleetValue = fleet.value.trim();
  fleetValid.classList.toggle("hidden", fleetValue !== "");
  return fleetValue !== "";
}

function checkDescription() {
  const descriptionValue = description.value.trim();
  descriptionValid.classList.toggle("hidden", descriptionValue !== "");
  return descriptionValue !== "";
}

function checkFounded() {
  const foundedValue = founded.value.trim();
  foundedValid.classList.toggle("hidden", foundedValue !== "");
  return foundedValue !== "";
}

function checkFormValidity() {
  checkName() && checkFleet() && checkDescription() && checkFounded()
    ? enableButton()
    : disableButton();
}

name.addEventListener("input", checkFormValidity);
fleet.addEventListener("input", checkFormValidity);
description.addEventListener("input", checkFormValidity);
founded.addEventListener("input", checkFormValidity);

disableButton();

form.addEventListener("submit", function (event) {
  const legend = document.getElementById("form").querySelector("legend");
  event.preventDefault();
  if (legend.innerText === "Add new Airline") {
    let data = {
      name: name.value.trim(),
      founded: founded.value.trim(),
      fleet_size: fleet.value.trim(),
      description: description.value.trim(),
    };
    console.log(data);
    postToAPI(`https://localhost:7153/Airline`, data);
  } else {
    let id = document.querySelector("[name='id']").value;
    let updatedData = {
      name: name.value.trim(),
      founded: founded.value.trim(),
      fleet_size: fleet.value.trim(),
      description: description.value.trim(),
    };
    UpdateAPI(`https://localhost:7153/Airline?id=${id}`, updatedData);
  }
  location.reload();
});

searchForm.addEventListener("submit", function (event) {
  event.preventDefault();
  const filter = searchForm.querySelector('select[name="filter"]').value;
  const value = searchForm.querySelector('input[name="value"]').value;
  console.log("Form Data:", filter, value);
  clearTableRows("table");
  getFromAPI(
    `https://localhost:7153/Airline/search?filter=${filter.trim()}&value=${value.trim()}`,
    renderAirlines
  );
});
