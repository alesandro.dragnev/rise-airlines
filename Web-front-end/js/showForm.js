const buttonShowMore = document.getElementById('show-more');
const buttonScrollTop = document.getElementById('scroll-top');
const table = document.getElementById('table');
const tableBody = document.getElementById('table-body');
const searchBar = document.getElementById('search-bar');
const form = document.getElementById('form');

buttonScrollTop.style.display = "none";

function hideRows() {
    if (tableBody.rows.length <= 3){
        buttonShowMore.style.display = 'none';
    }
    else {
        for (let i = 3; i < tableBody.rows.length; i++) {
            tableBody.rows[i].style.display = 'none';
        }
    }
}

function showForm() {
    if (form.classList.contains('hidden')) {
        table.classList.add('hidden');
        searchBar.classList.add('hidden');
        form.classList.remove('hidden');
        buttonShowMore.classList.add('hidden');
    } else {
        table.classList.remove('hidden');
        searchBar.classList.remove('hidden');
        form.classList.add('hidden');
        buttonShowMore.classList.remove('hidden');
    }
}

function showMore() {
    if (tableBody.rows[3].style.display === 'none') {
        for (let i = 3; i < tableBody.rows.length; i++) {
            tableBody.rows[i].style.display = '';
        }
    }
    else {
        for (let i = 3; i < tableBody.rows.length; i++) {
            tableBody.rows[i].style.display = 'none';
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    setTimeout(function () {
        hideRows();
    }, 1000);


    table.addEventListener("scroll", function() {
        if (table.scrollHeight > 300) {
            buttonScrollTop.style.display = "";
        }
    });

    function scrollTop() {
        table.style.scrollBehavior = "smooth";
        table.scrollTop = 0;
    }
    buttonScrollTop.addEventListener("click", scrollTop);
});

