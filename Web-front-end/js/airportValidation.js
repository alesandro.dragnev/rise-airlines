import {
  getFromAPI,
  postToAPI,
  UpdateAPI,
  renderAirports,
} from "./httpRequests.js";

await getFromAPI("https://localhost:7153/Airport", renderAirports);

const submitButton = document.getElementById("submit-button");
const nameValid = document.getElementById("name-valid");
const countyValid = document.getElementById("country-valid");
const cityValid = document.getElementById("city-valid");
const codeValid = document.getElementById("code-valid");
const runwaysCountValid = document.getElementById("runways-count-valid");
const foundedValid = document.getElementById("founded-valid");
const name = document.getElementById("name");
const country = document.getElementById("country");
const city = document.getElementById("city");
const code = document.getElementById("code");
const runways = document.getElementById("runways_count");
const founded = document.getElementById("founded");
const searchForm = document.getElementById("search-form");

function clearTableRows(tableId) {
  const table = document.getElementById(tableId);
  const tbody = table.getElementsByTagName("tbody")[0];

  while (tbody.rows.length > 0) {
    tbody.deleteRow(0);
  }
}

function disableButton() {
  submitButton.disabled = true;
  submitButton.style.backgroundColor = "#E5E5E5";
}

function enableButton() {
  submitButton.disabled = false;
  submitButton.style.backgroundColor = "#FCA311";
  submitButton.style.cursor = "pointer";
}

function checkName() {
  const nameValue = name.value.trim();
  nameValid.classList.toggle("hidden", nameValue !== "");
  return nameValue !== "";
}

function checkCountry() {
  const countryValue = country.value.trim();
  countyValid.classList.toggle("hidden", countryValue !== "");
  return countryValue !== "";
}

function checkCity() {
  const cityValue = city.value.trim();
  cityValid.classList.toggle("hidden", cityValue !== "");
  return cityValue !== "";
}

function checkCode() {
  const codeValue = code.value.trim();
  codeValid.classList.toggle("hidden", 2 < codeValue.length < 4);
  return codeValue !== "";
}

function checkRunwaysCount() {
  const runwaysValue = runways.value.trim();
  runwaysCountValid.classList.toggle("hidden", runwaysValue !== "");
  return runwaysValue !== "";
}

function checkFounded() {
  const foundedValue = new Date(founded.value.trim()).getTime();
  foundedValid.classList.toggle(
    "hidden",
    isNaN(foundedValue) || foundedValue >= Date.now()
  );
  return foundedValue !== "";
}

function checkFormValidity() {
  checkName() &&
  checkCountry() &&
  checkCity() &&
  checkCode() &&
  checkRunwaysCount() &&
  checkFounded()
    ? enableButton()
    : disableButton();
}

disableButton();

name.addEventListener("input", checkFormValidity);
country.addEventListener("input", checkFormValidity);
city.addEventListener("input", checkFormValidity);
code.addEventListener("input", checkFormValidity);
runways.addEventListener("input", checkFormValidity);
founded.addEventListener("input", checkFormValidity);

form.addEventListener("submit", function (event) {
  const legend = document.getElementById("form").querySelector("legend");
  event.preventDefault();
  if (legend.innerText === "Add new Airport") {
    let data = {
      name: name.value.trim(),
      country: country.value.trim(),
      city: city.value.trim(),
      code: code.value.trim(),
      runways_count: runways.value.trim(),
      founded: founded.value.trim(),
    };
    console.log(data);
    postToAPI(`https://localhost:7153/Airport`, data);
  } else {
    let id = document.querySelector("[name='id']").value;
    let updatedData = {
      name: name.value.trim(),
      country: country.value.trim(),
      city: city.value.trim(),
      code: code.value.trim(),
      runways_count: runways.value.trim(),
      founded: founded.value.trim(),
    };
    UpdateAPI(`https://localhost:7153/Airport?id=${id}`, updatedData);
  }
  location.reload();
});

searchForm.addEventListener("submit", function (event) {
  event.preventDefault();
  const filter = searchForm.querySelector('select[name="filter"]').value;
  const value = searchForm.querySelector('input[name="value"]').value;
  console.log("Form Data:", filter, value);
  clearTableRows("table");
  getFromAPI(
    `https://localhost:7153/Airport/search?filter=${filter.trim()}&value=${value.trim()}`,
    renderAirports
  );
});
