using Airlines.Business.Mappers;
using Airlines.Business.Services;
using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.Repositories;
using Microsoft.EntityFrameworkCore;
#pragma warning disable IDE0058 // Expression value is never used
namespace Airlines.API;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        builder.Services.AddScoped<IAirportRepository, AirportRepository>();
        builder.Services.AddScoped<IAirportService, AirportService>();
        builder.Services.AddAutoMapper(typeof(AirportMapper));
        // DB
        builder.Services.AddDbContext<RISEAirlinesContext>(options =>
        options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
        // Airline
        builder.Services.AddScoped<IAirlinesRepository, AirlineRepository>();
        builder.Services.AddScoped<IAirlineService, AirlineService>();
        builder.Services.AddAutoMapper(typeof(AirlineMapper));
        //Flight
        builder.Services.AddScoped<IFlightRepository, FlightRepository>();
        builder.Services.AddScoped<IFlightService, FlightService>();
        builder.Services.AddAutoMapper(typeof(FlightMapper));

        builder.Services.AddCors(options =>
        {
            options.AddPolicy(name: "MyOrigin",
                              policy =>
                              {
                                  policy.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod();
                              });
        });

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseCors("MyOrigin");

        app.UseAuthorization();


        app.MapControllers();

        app.Run();
    }
}
