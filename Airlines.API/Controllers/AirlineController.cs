﻿using Airlines.Business.DTOs;
using Airlines.Business.Services;
using Airlines.Business.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Airlines.API.Controllers;
[ApiController]
[Route("[controller]")]
public class AirlineController(IAirlineService airlineService) : ControllerBase
{
    public IAirlineService _airlineService = airlineService;

    [HttpGet]
    public ActionResult<IEnumerable<AirlineDTO>> GetAirlines()
    {
        var viewModel = _airlineService.GetAirlines();
        return Ok(viewModel);
    }

    [HttpGet("{id}")]
    public ActionResult Details(int id)
    {
        var viewModel = _airlineService.GetAirlineByID(id);
        return Ok(viewModel);
    }

    [HttpPost]
    public ActionResult Create([FromBody] AirlineDTO model)
    {
        if (ModelState.IsValid)
        {

            _airlineService.AddToAirlines(model);
            return Ok(model);
        }
        var viewModel = _airlineService.GetAirlines();
        return BadRequest(viewModel);
    }

    [HttpPut]
    public ActionResult EditAirline([FromBody] AirlineDTO model, [FromQuery] int id)
    {
        model.ID = id;


        _airlineService.UpdateAirline(model);
        return Ok(model);

    }

    [HttpDelete]
    public ActionResult DeleteAirline([FromQuery] int id)
    {
        var airline = _airlineService.GetAirlineByID(id);
        if (airline == null)
        {
            return BadRequest("Airline does not exist");
        }
        _airlineService.DeleteAirline(id);
        return Ok();
    }

    [HttpGet("search")]
    public ActionResult<IEnumerable<AirlineDTO>> Search(string filter, string value)
    {
        var viewModel = _airlineService.SearchAirline(filter, value);
        if (viewModel.Count > 0)
        {
            return Ok(viewModel);
        }
        else
        {
            viewModel = _airlineService.GetAirlines();
        }
        return BadRequest(viewModel);
    }
}
