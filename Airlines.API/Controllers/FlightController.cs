﻿using Airlines.Business.DTOs;
using Airlines.Business.Services;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers;
[ApiController]
[Route("[controller]")]
public class FlightController(IFlightService flightService, IAirportService airportService) : ControllerBase
{
    public IFlightService FlightService = flightService;
    public IAirportService _airportService = airportService;

    [HttpGet]
    public ActionResult<IEnumerable<FlightDTO>> GetFlights()
    {
        var flights = FlightService.GetFlights();
        return Ok(flights);
    }

    [HttpGet("{id}")]
    public ActionResult<FlightDTO> GetFlightByID(int id)
    {
        var viewModel = FlightService.GetFlightsById(id);
        return viewModel == null ? (ActionResult<FlightDTO>)NotFound() : (ActionResult<FlightDTO>)Ok(viewModel);
    }

    [HttpPost]
    public ActionResult Create([FromBody] FlightDTO model, [FromQuery] string arrival, [FromQuery] string departure)
    {
        var arrivalAirport = _airportService.GetAirportByCode(arrival);
        var departureAirport = _airportService.GetAirportByCode(departure);

        if (arrivalAirport != null && departureAirport != null)
        {
            model.ArrivalID = arrivalAirport.ID;
            model.DepartureID = departureAirport.ID;
        }
        else
        {
            return NotFound();
        }

        FlightService.AddToFlight(model);
        return Ok();
    }

    [HttpPut]
    public ActionResult Edit([FromBody] FlightDTO model, [FromQuery] int id, [FromQuery] string arrival, [FromQuery] string departure)
    {
        var arrivalAirport = _airportService.GetAirportByCode(arrival);
        var departureAirport = _airportService.GetAirportByCode(departure);

        if (arrivalAirport != null && departureAirport != null)
        {
            model.ArrivalID = arrivalAirport.ID;
            model.DepartureID = departureAirport.ID;
        }
        model.ID = id;

        FlightService.UpdateFlight(model);
        return Ok(model);

    }

    [HttpDelete]
    public ActionResult DeleteFlight([FromQuery] int id)
    {
        var flight = FlightService.GetFlightsById(id);
        if (flight == null)
        {
            return BadRequest("Flight does not exist");
        }
        FlightService.DeleteFlight(id);
        return Ok();
    }

    [HttpGet("search")]
    public ActionResult<IEnumerable<FlightDTO>> Search([FromQuery] string filter, [FromQuery] string value)
    {
        var viewModel = FlightService.SearchFlight(filter, value);

        if (viewModel.Count > 0)
        {
            return Ok(viewModel);
        }
        else
        {
            viewModel = FlightService.GetFlights();
        }
        return StatusCode(404, new { message = "Not found flight with this filter and value", viewModel });
    }
}
