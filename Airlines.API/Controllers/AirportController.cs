﻿using Airlines.Business.DTOs;
using Airlines.Business.Services;
using Airlines.Business.Validation;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers;
[ApiController]
[Route("[controller]")]
public class AirportController(IAirportService airportService) : ControllerBase
{
    public IAirportService _airportService = airportService;

    [HttpGet]
    public ActionResult<IEnumerable<AirportDTO>> GetAirports()
    {
        List<AirportDTO> viewModel = _airportService.GetAirports();
        return Ok(viewModel);
    }

    [HttpGet("{id}")]
    public ActionResult Details(int id)
    {
        var model = _airportService.GetAirportByID(id);
        if (model == null)
        {
            return BadRequest("Airport does not exist.");
        }
        return Ok(model);
    }

    [HttpPost]
    public ActionResult Create([FromBody] AirportDTO model)
    {
        if (AirportValidator.ValidateAirport(model))
        {
            _airportService.AddToAirports(model);

            return Ok(model);
        }
        return StatusCode(500);

    }

    [HttpPut]
    public ActionResult Edit([FromBody] AirportDTO model, [FromQuery] int id)
    {
        model.ID = id;

        if (ModelState.IsValid)
        {
            _airportService.UpdateAirport(model);
            return Ok();
        }

        return BadRequest();
    }


    [HttpDelete]
    public ActionResult DeleteAirport(int id)
    {
        var airport = _airportService.GetAirportByID(id);
        if (airport == null)
        {
            return BadRequest("Airport does not exist");
        }
        _airportService.DeleteAirport(id);
        return Ok();
    }

    [HttpGet("search")]
    public ActionResult<IEnumerable<AirportDTO>> Search(string filter, string value)
    {
        var viewModel = _airportService.SearchAirport(filter, value);
        if (viewModel.Count > 0)
        {
            return Ok(viewModel);
        }
        else
        {
            viewModel = _airportService.GetAirports();
        }
        return StatusCode(404, new { message = "Not found flight with this filter and value", viewModel });
    }
}

/*
    // POST: AirportController/Search/
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Search(string filter, string value)
    {
        var viewModel = _airportService.SearchAirport(filter, value);
        if (viewModel.Count > 0)
        {
            return View("Index", viewModel);
        }
        else
        {
            viewModel = _airportService.GetAirports();
        }
        return View("Index", viewModel);
    }
}
*/