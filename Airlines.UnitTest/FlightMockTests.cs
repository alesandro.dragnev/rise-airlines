﻿using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.Repositories;
using Moq;
using Moq.EntityFrameworkCore;
using Airlines.Persistence.Basic.Models;

namespace Airlines.UnitTest;
public class FlightMockTests
{
    private readonly Mock<RISEAirlinesContext> _contextMock;
    private readonly FlightRepository _flightRepo;

    public FlightMockTests()
    {
        _contextMock = new Mock<RISEAirlinesContext>();
        _flightRepo = new FlightRepository(_contextMock.Object);
    }

    [Fact]
    public void GetAllFlights_ShouldReturnTrue()
    {
        // Arrange
        Airport arrival = new Airport { ID = 1 };
        Airport departure = new Airport { ID = 2 };

        List<Flight> flights = [];
        List<Airport> airports = [];

        Flight flight1 = new Flight
        {
            ID = 1,
            Flight_Number = "FL123",
            DepartureID = 1,
            ArrivalID = 2,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        Flight flight2 = new Flight
        {
            ID = 2,
            Flight_Number = "FL223",
            DepartureID = 2,
            ArrivalID = 1,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        flights.Add(flight1);
        flights.Add(flight2);
        airports.Add(arrival);
        airports.Add(departure);

        List<Flight> expectedFlights = flights;

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Flights).ReturnsDbSet(flights);

        // Act
        List<Flight> allFlights = _flightRepo.GetAllFlights();

        // Assert
        Assert.Equal(expectedFlights[0].Arrival_date, allFlights[0].Arrival_date);
    }

    [Fact]
    public void GetFlightsCount_ShouldReturnTrue()
    {
        // Arrange
        Airport arrival = new Airport { ID = 1 };
        Airport departure = new Airport { ID = 2 };

        List<Flight> flights = [];
        List<Airport> airports = [];

        Flight flight1 = new Flight
        {
            ID = 1,
            Flight_Number = "FL123",
            DepartureID = 1,
            ArrivalID = 2,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        Flight flight2 = new Flight
        {
            ID = 2,
            Flight_Number = "FL223",
            DepartureID = 2,
            ArrivalID = 1,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        flights.Add(flight1);
        flights.Add(flight2);
        airports.Add(arrival);
        airports.Add(departure);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Flights).ReturnsDbSet(flights);

        // Act
        var allFlights = _flightRepo.GetFlightCount();

        // Assert
        Assert.True(allFlights == 2);
    }

    [Fact]
    public void GetFlightsById_ShouldReturnTrue()
    {
        // Arrange
        Airport arrival = new Airport { ID = 1 };
        Airport departure = new Airport { ID = 2 };

        List<Flight> flights = [];
        List<Airport> airports = [];

        Flight flight1 = new Flight
        {
            ID = 1,
            Flight_Number = "FL123",
            DepartureID = 1,
            ArrivalID = 2,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        Flight flight2 = new Flight
        {
            ID = 2,
            Flight_Number = "FL223",
            DepartureID = 2,
            ArrivalID = 1,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        flights.Add(flight1);
        flights.Add(flight2);
        airports.Add(arrival);
        airports.Add(departure);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Flights).ReturnsDbSet(flights);

        // Act
        var allFlights = _flightRepo.GetFlightsById(1);

        // Assert
        Assert.True(allFlights?.Airline == "Boeing747");
    }

    [Fact]
    public void GetAllFlightsByArrivalID_ShouldReturnTrue()
    {
        // Arrange
        Airport arrival = new Airport { ID = 1 };
        Airport departure = new Airport { ID = 2 };

        List<Flight> flights = [];
        List<Airport> airports = [];

        Flight flight1 = new Flight
        {
            ID = 1,
            Flight_Number = "FL123",
            DepartureID = 1,
            ArrivalID = 2,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        Flight flight2 = new Flight
        {
            ID = 2,
            Flight_Number = "FL223",
            DepartureID = 2,
            ArrivalID = 1,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        flights.Add(flight1);
        flights.Add(flight2);
        airports.Add(arrival);
        airports.Add(departure);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Flights).ReturnsDbSet(flights);

        // Act
        var allFlights = _flightRepo.GetFlightByFlightNumber("FL223");

        // Assert
        Assert.True(allFlights?.Flight_Number == "FL223");
    }

    [Fact]
    public void GetFlightsByFlightNumber_ShouldReturnTrue()
    {
        // Arrange
        Airport arrival = new Airport { ID = 1 };
        Airport departure = new Airport { ID = 2 };

        List<Flight> flights = [];
        List<Airport> airports = [];

        Flight flight1 = new Flight
        {
            ID = 1,
            Flight_Number = "FL123",
            DepartureID = 1,
            ArrivalID = 2,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        Flight flight2 = new Flight
        {
            ID = 2,
            Flight_Number = "FL223",
            DepartureID = 2,
            ArrivalID = 1,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        flights.Add(flight1);
        flights.Add(flight2);
        airports.Add(arrival);
        airports.Add(departure);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Flights).ReturnsDbSet(flights);

        // Act
        var allFlights = _flightRepo.GetAllFlightsByArrivalID(1);

        // Assert
        Assert.True(allFlights[0]?.Flight_Number == "FL223");
    }

    [Fact]
    public void GetAllFlightsByDepartureID_ShouldReturnTrue()
    {
        // Arrange
        Airport arrival = new Airport { ID = 1 };
        Airport departure = new Airport { ID = 2 };

        List<Flight> flights = [];
        List<Airport> airports = [];

        Flight flight1 = new Flight
        {
            ID = 1,
            Flight_Number = "FL123",
            DepartureID = 1,
            ArrivalID = 2,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        Flight flight2 = new Flight
        {
            ID = 2,
            Flight_Number = "FL223",
            DepartureID = 2,
            ArrivalID = 1,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        flights.Add(flight1);
        flights.Add(flight2);
        airports.Add(arrival);
        airports.Add(departure);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Flights).ReturnsDbSet(flights);

        // Act
        var allFlights = _flightRepo.GetAllFlightsByDepartureID(2);

        // Assert
        Assert.True(allFlights[0]?.Flight_Number == "FL223");
    }

    [Fact]
    public void Exist_ShouldReturnTrue()
    {
        // Arrange
        Airport arrival = new Airport { ID = 1 };
        Airport departure = new Airport { ID = 2 };

        List<Flight> flights = [];
        List<Airport> airports = [];

        Flight flight1 = new Flight
        {
            ID = 1,
            Flight_Number = "FL123",
            DepartureID = 1,
            ArrivalID = 2,
            Airline = "Boeing747",
            Departure_date = DateTime.Parse("2024-11-11 18:00:00"),
            Arrival_date = DateTime.Parse("2024-11-11 20:00:00"),
            Arrival = arrival,
            Departure = departure
        };

        flights.Add(flight1);
        airports.Add(arrival);
        airports.Add(departure);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Flights).ReturnsDbSet(flights);

        // Act
        var allFlights = _flightRepo.Exist(1);

        // Assert
        Assert.True(allFlights);
    }
}
