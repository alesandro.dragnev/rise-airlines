﻿using Airlines.Business.Commands.Exist;
using Airlines.Persistence.Basic.Airlines;
using Airlines.Business.Validation;
using Airlines.Business.DTOs;

namespace Airlines.UnitTest;
public class ValidationTests
{

    // Airport validation tests
    [Theory]
    [InlineData(4)]
    public void AirportValidation_RandomThreeLettersItem_ShouldReturnTrue(int length)
    {
        // Arrange
        var airportName = RandomGeneratingFunctions.GenerateRandomString(length);

        // Act
        var validationResult = AirportValidator.AirportValidation(airportName);

        // Assert
        Assert.True(validationResult);
    }

    [Fact]
    public void AirportValidation_NumericThreeLettersItem_ShouldReturnTrue()
    {
        // Arrange
        var airportName = "jf3";

        // Act
        var validationResult = AirportValidator.AirportValidation(airportName);

        // Assert
        Assert.False(validationResult);
    }

    [Fact]
    public void AirportValidation_SpecialCharThreeLettersItem_ShouldReturnTrue()
    {
        // Arrange
        var airportName = "@f%";

        // Act
        var validationResult = AirportValidator.AirportValidation(airportName);

        // Assert
        Assert.False(validationResult);
    }

    [Fact]
    public void AirportValidation_EmptyThreeLettersItem_ShouldReturnTrue()
    {
        // Arrange
        var airportName = "";

        // Act
        var validationResult = AirportValidator.AirportValidation(airportName);

        // Assert
        Assert.False(validationResult);
    }

    [Theory]
    [InlineData(1)]
    [InlineData(5)]
    public void AirportValidation_ThreeLettersItem_ShouldReturnFalse(int length)
    {
        // Arrange
        var airportName = RandomGeneratingFunctions.GenerateRandomString(length);

        // Act
        var validationResult = AirportValidator.AirportValidation(airportName);

        // Assert
        Assert.False(validationResult);
    }

    // Airline validation tests
    [Fact]
    public void AirlineValidation_FourLettersItem_ShouldReturnTrue()
    {
        // Arrange
        var airlineDTO = new AirlineDTO("wizz", new DateOnly(2000, 4, 4), 200, "aaaaaaaa");
        // Act
        var validationResult = AirlinesValidator.AirlineValidation(airlineDTO);

        // Assert
        Assert.True(validationResult);
    }

    [Fact]
    public void AirlineValidation_MoreThanSixLettersItem_ShouldReturnTrue()
    {
        // Arrange
        var airlineDTO = new AirlineDTO("wizzaaaaaa", new DateOnly(2000, 4, 4), 200, "aaaaaaaa");

        // Act
        var validationResult = AirlinesValidator.AirlineValidation(airlineDTO);

        // Assert
        Assert.False(validationResult);
    }

    [Fact]
    public void AirlineValidation_IncludingSpecialCharLettersItem_ShouldReturnFalse()
    {
        var airlineDTO = new AirlineDTO("ryan@", new DateOnly(2000, 4, 4), 200, "aaaaaaaa");

        var validationResult = AirlinesValidator.AirlineValidation(airlineDTO);

        Assert.True(validationResult);
    }


    [Fact]
    public void AirlineValidation_FourLettersItem_ShouldReturnFalse()
    {
        // Arrange
        var airlineName = "raynair";
        var airlineDTO = new AirlineDTO(airlineName, new DateOnly(2000, 4, 4), 200, "aaaaaaaa");

        // Act
        var validationResult = AirlinesValidator.AirlineValidation(airlineDTO);

        // Assert
        Assert.False(validationResult);
    }

    // Flight validation tests
    [Fact]
    public void FlightValidation_LettersAndNumberItem_ShouldReturnTrue()
    {
        // Arrange
        var flightNumber = "ds3f5";

        // Act
        var validationResult = FlightValidator.FlightValidation(flightNumber);

        // Assert
        Assert.True(validationResult);
    }

    [Fact]
    public void FlightValidation_SpecialChars_ShouldReturnFalse()
    {
        // Arrange
        var flightNumber = "-------";

        // Act
        var validationResult = FlightValidator.FlightValidation(flightNumber);

        // Assert
        Assert.False(validationResult);
    }

    [Theory]
    [InlineData(60)]
    [InlineData(70)]
    [InlineData(100)]
    public void NameValidation_RandomString_ShouldReturnTrue(int length)
    {
        // Arrange
        var name = RandomGeneratingFunctions.GenerateRandomString(length);

        // Act
        var validationResult = AirportValidator.NameValidation(name);

        // Assert
        Assert.True(validationResult);
    }


    [Theory]
    [InlineData(60)]
    [InlineData(70)]
    [InlineData(100)]
    public void CityValidation_RandomString_ShouldReturnTrue(int length)
    {
        // Arrange
        var city = RandomGeneratingFunctions.GenerateRandomString(length);

        // Act
        var validationResult = AirportValidator.CityValidation(city);

        // Assert
        Assert.True(validationResult);
    }

    [Theory]
    [InlineData(60)]
    [InlineData(70)]
    [InlineData(100)]
    public void CountryValidation_RandomString_ShouldReturnTrue(int length)
    {
        // Arrange
        var country = RandomGeneratingFunctions.GenerateRandomString(length);

        // Act
        var validationResult = AirportValidator.CountryValidation(country);

        // Assert
        Assert.True(validationResult);
    }

    [Fact]
    public void ExistingAirlines_CorrectInput_ShouldReturnTrue()
    {
        var airlineDict = new Dictionary<string, List<object>>
        {
            { "airlines", new List<object>
                {
                    Airline.CreateAirline("Delta"),
                    Airline.CreateAirline("Wizz"),
                    Airline.CreateAirline("Ryan")
                }
            }
        };
        var key = "Delta";
        Exist result;
        using (var console = new StringWriter())
        {
            result = Exist.ExistCommand(airlineDict, key);
        }

        Assert.True(result.ExistingAirline());
    }

    [Theory]
    [InlineData("BULD")]
    [InlineData("BULB")]
    [InlineData("AAAA")]
    public void ExistingAirlines_WrongInput_ShouldReturnFalse(string key)
    {
        var airlineDict = new Dictionary<string, List<object>>
        {
            { "airlines", new List<object>
                {
                    Airline.CreateAirline("Delta"),
                    Airline.CreateAirline("Wizz"),
                    Airline.CreateAirline("Ryan")
                }
            }
        };
        Exist result;
        using (var console = new StringWriter())
        {
            result = Exist.ExistCommand(airlineDict, key);
        }

        Assert.False(result.ExistingAirline());
    }

    [Fact]
    public void ValidateAirline_CorrectInput_ShouldReturnTrue()
    {
        var airlineDTO = new AirlineDTO("RYAN", new DateOnly(2000, 4, 4), 200, "aaaaaaaa");

        var result = AirlinesValidator.ValidateAirline(airlineDTO);

        Assert.True(result);
    }

    [Fact]
    public void ValidateAirline_WrongInput_ShouldReturnFalse()
    {
        var airlineDTO = new AirlineDTO("RYANMMM", new DateOnly(2000, 4, 4), 200, "aaaaaaaa");

        var result = AirlinesValidator.ValidateAirline(airlineDTO);

        Assert.False(result);
    }

    [Fact]
    public void ValidateFlight_CorrectInput_ShouldReturnTrue()
    {
        var arrival = new AirportDTO();
        var departure = new AirportDTO();
        var flightDTO = new FlightDTO("Fl123", "WIZZ", 1, 2, new DateTime(2024, 5, 5, 10, 0, 0), new DateTime(2024, 5, 5, 12, 0, 0), arrival, departure);


        var result = FlightValidator.ValidateFlight(flightDTO);

        Assert.True(result);
    }

    [Fact]
    public void ValidateFlight_WrongInput_ShouldReturnFalse()
    {
        var arrival = new AirportDTO();
        var departure = new AirportDTO();
        var flightDTO = new FlightDTO("AAAAAAAAAA", "WIZZ", 1, 2, new DateTime(2024, 5, 5, 10, 0, 0), new DateTime(2024, 5, 5, 12, 0, 0), arrival, departure);

        var result = FlightValidator.ValidateFlight(flightDTO);

        Assert.False(result);
    }

    /*    [Fact]
        public void ValidateAirport_CorrectInput_ShouldReturnTrue()
        {
            var flightDTO = new FlightDTO("Fl123", "WIZZ", 1, 2, new DateTime(2024, 5, 5, 10, 0, 0), new DateTime(2024, 5, 5, 12, 0, 0));


            var result = AirportValidator.ValidateAirport("JFK", "varna", "Bulgaria", "Boing");

            Assert.True(result);
        }

        [Fact]
        public void ValidateAirport_WrongInput_ShouldReturnFalse()
        {

            var result = AirportValidator.ValidateAirport("----", "varna", "Bulgaria", "Boing");

            Assert.False(result);
        }*/
}
