﻿using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.Repositories;
using Moq;
using Moq.EntityFrameworkCore;
using Airlines.Persistence.Basic.Models;

namespace Airlines.UnitTest;
public class AirlineMockTests
{
    private readonly Mock<RISEAirlinesContext> _contextMock;
    private readonly AirlineRepository _airlineRepository;

    public AirlineMockTests()
    {
        _contextMock = new Mock<RISEAirlinesContext>();
        _airlineRepository = new AirlineRepository(_contextMock.Object);
    }

    [Fact]
    public void GetAllAirlines_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        Airline airline2 = new Airline
        {
            Name = "Delta Air Lines",
            Founded = new DateOnly(1924, 3, 2),
            Fleet_size = 800,
            Description = "Delta Air Lines is a major American airline headquartered in Atlanta, Georgia."
        };

        airlines.Add(airline1);
        airlines.Add(airline2);

        List<Airline> expectedAirlines = airlines;

        var returnsResult = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);

        List<Airline> allAirlines = _airlineRepository.GetAllAirlines();

        Assert.Equal(expectedAirlines, allAirlines);
    }

    [Fact]
    public void GetAirlineByID_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        Airline airline2 = new Airline
        {
            ID = 2,
            Name = "Delta Air Lines",
            Founded = new DateOnly(1924, 3, 2),
            Fleet_size = 800,
            Description = "Delta Air Lines is a major American airline headquartered in Atlanta, Georgia."
        };

        airlines.Add(airline1);
        airlines.Add(airline2);

        List<Airline> expectedAirlines = airlines;

        var returnsResult = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);

        Airline? allAirlines = _airlineRepository.GetAirlineById(1);

        Assert.Equal(expectedAirlines[0], allAirlines);
    }

    [Fact]
    public void GetAirlineCount_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        Airline airline2 = new Airline
        {
            ID = 2,
            Name = "Delta Air Lines",
            Founded = new DateOnly(1924, 3, 2),
            Fleet_size = 800,
            Description = "Delta Air Lines is a major American airline headquartered in Atlanta, Georgia."
        };

        airlines.Add(airline1);
        airlines.Add(airline2);

        List<Airline> expectedAirlines = airlines;

        var returnsResult = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);

        var allAirlines = _airlineRepository.GetAirlineCount();

        Assert.Equal(expectedAirlines.Count, allAirlines);
    }

    [Fact]
    public void GetAirlineByName_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        Airline airline2 = new Airline
        {
            ID = 2,
            Name = "Delta Air Lines",
            Founded = new DateOnly(1924, 3, 2),
            Fleet_size = 800,
            Description = "Delta Air Lines is a major American airline headquartered in Atlanta, Georgia."
        };

        airlines.Add(airline1);
        airlines.Add(airline2);

        List<Airline> expectedAirlines = airlines;

        var returnsResult = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);

        var allAirlines = _airlineRepository.GetAirlinesByName("Emirates");

        Assert.Equal(expectedAirlines[0], allAirlines);
    }


    [Fact]
    public void GetAirlineBySize_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        Airline airline2 = new Airline
        {
            ID = 2,
            Name = "Delta Air Lines",
            Founded = new DateOnly(1924, 3, 2),
            Fleet_size = 800,
            Description = "Delta Air Lines is a major American airline headquartered in Atlanta, Georgia."
        };

        airlines.Add(airline1);
        airlines.Add(airline2);

        List<Airline> expectedAirlines = airlines;

        var returnsResult = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);

        var allAirlines = _airlineRepository.GetAirlinesBySize(270);

        Assert.Equal(expectedAirlines[0], allAirlines[0]);
    }

    [Fact]
    public void Exist_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        Airline airline2 = new Airline
        {
            ID = 2,
            Name = "Delta Air Lines",
            Founded = new DateOnly(1924, 3, 2),
            Fleet_size = 800,
            Description = "Delta Air Lines is a major American airline headquartered in Atlanta, Georgia."
        };

        airlines.Add(airline1);
        airlines.Add(airline2);

        List<Airline> expectedAirlines = airlines;

        var returnsResult = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);

        var allAirlines = _airlineRepository.Exist(1);

        Assert.True(allAirlines);
    }

    [Fact]
    public void AddNewAirline_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        Airline airline2 = new Airline
        {
            ID = 2,
            Name = "Delta Air Lines",
            Founded = new DateOnly(1924, 3, 2),
            Fleet_size = 800,
            Description = "Delta Air Lines is a major American airline headquartered in Atlanta, Georgia."
        };

        airlines.Add(airline1);

        List<Airline> expectedAirlines = airlines;
        _ = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);
        var returnsResult = _contextMock.Setup(context => context.Airlines.Add(It.IsAny<Airline>()))
         .Callback((Airline entity) =>
         {
             _ = _airlineRepository.AddNewAirline(entity);
         });

        var addAirline = _airlineRepository.AddNewAirline(airline2);

        Assert.True(addAirline);
    }

    [Fact]
    public void AddNewAirline_ShouldReturnFalse()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };


        airlines.Add(airline1);

        List<Airline> expectedAirlines = airlines;
        _ = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);
        var returnsResult = _contextMock.Setup(context => context.Airlines.Add(It.IsAny<Airline>()))
         .Callback((Airline entity) =>
         {
             _ = _airlineRepository.AddNewAirline(entity);
         });

        var addAirline = _airlineRepository.AddNewAirline(airline1);

        Assert.False(addAirline);
    }

    [Fact]
    public void UpdateAirline_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };
        Airline updated = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 300,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };


        airlines.Add(airline);

        _ = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);
        _ = _contextMock.Setup(context => context.Airlines.Update(It.IsAny<Airline>()))
            .Callback((Airline entity) =>
            {
                _ = _airlineRepository.UpdateAirline(entity);
            });

        var updateAirline = _airlineRepository.UpdateAirline(updated);

        Assert.True(updateAirline);
    }

    [Fact]
    public void DeleteAirlineById_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        airlines.Add(airline);

        _ = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);
        _ = _contextMock.Setup(context => context.Airlines.Remove(It.IsAny<Airline>()))
            .Callback((Airline entity) =>
            {
                _ = _airlineRepository.DeleteAirlineById(entity.ID);
            });

        var deleteAirline = _airlineRepository.DeleteAirlineById(1);

        Assert.True(deleteAirline);
    }

    [Fact]
    public void DeleteAirlineByName_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        airlines.Add(airline);

        _ = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);
        _ = _contextMock.Setup(context => context.Airlines.Remove(It.IsAny<Airline>()))
            .Callback((Airline entity) =>
            {
                _ = _airlineRepository.DeleteAirlineByName(entity.Name);
            });

        var deleteAirline = _airlineRepository.DeleteAirlineByName("Emirates");

        Assert.True(deleteAirline);
    }

    [Fact]
    public void GetAirlineByFilter_ShouldReturnTrue()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        Airline airline2 = new Airline
        {
            ID = 2,
            Name = "Delta Air Lines",
            Founded = new DateOnly(1924, 3, 2),
            Fleet_size = 800,
            Description = "Delta Air Lines is a major American airline headquartered in Atlanta, Georgia."
        };

        airlines.Add(airline1);
        airlines.Add(airline2);

        List<Airline> expectedAirlines = airlines;

        var returnsResult = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);

        var allAirlines = _airlineRepository.GetAirlinesByFilter("Name", "Emirates");

        Assert.Equal(expectedAirlines[0], allAirlines[0]);
    }

    [Fact]
    public void GetAirlineByFilter_ShouldReturnFalse()
    {

        List<Airline> airlines = [];

        Airline airline1 = new Airline
        {
            ID = 1,
            Name = "Emirates",
            Founded = new DateOnly(1985, 3, 25),
            Fleet_size = 270,
            Description = "Emirates is one of the world's largest airlines, based in Dubai, United Arab Emirates."
        };

        Airline airline2 = new Airline
        {
            ID = 2,
            Name = "Delta Air Lines",
            Founded = new DateOnly(1924, 3, 2),
            Fleet_size = 800,
            Description = "Delta Air Lines is a major American airline headquartered in Atlanta, Georgia."
        };

        airlines.Add(airline1);
        airlines.Add(airline2);

        List<Airline> expectedAirlines = airlines;

        var returnsResult = _contextMock.Setup(context => context.Airlines).ReturnsDbSet(airlines);

        var allAirlines = _airlineRepository.GetAirlinesByFilter("City", "Sofia");

        Assert.Equal([], allAirlines);
    }
}
