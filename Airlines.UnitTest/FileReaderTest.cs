﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileReaders;

namespace Airlines.UnitTest;
public class FileReaderTest
{
    [Fact]
    public void ReadAirport_ShouldReturnTrue()
    {
        var airports = FileReader.ReadAirportsFile(@"../../../FilesToRead/AirportTest.csv");

        Assert.True(airports.Count > 0);
    }

    [Fact]
    public void ReadAirlines_ShouldReturnTrue()
    {
        var airlines = FileReader.ReadAirlinesFile(@"../../../FilesToRead/AirlineTest.csv");

        Assert.True(airlines.Count > 0);
    }

    [Fact]
    public void ReadFlight_ShouldReturnTrue()
    {
        var flights = FileReader.ReadFlightsFile(@"../../../FilesToRead/FlightTest.csv");

        Assert.True(flights.Count > 0);
    }

    [Fact]
    public void ReadAircraft_ShouldReturnTrue()
    {
        var aircraft = FileReader.ReadAircraftsFile(@"../../../FilesToRead/AircraftTest.csv");

        Assert.True(aircraft.Count > 0);
    }

    [Fact]
    public void ReadFlightRoute_ShouldReturnTrue()
    {
        var flights = FileReader.ReadFlightsFile(@"../../../FilesToRead/FlightTest.csv");
        var tree = FileReader.ReadFlightsRouteFile(@"../../../FilesToRead/FlightRouteTest.csv", flights);

        Assert.True(tree != null);
    }
}
