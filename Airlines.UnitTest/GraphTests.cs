﻿using Airlines.Persistence;
using Airlines.Business.Commands.Route;
using Airlines.OLD.Persistence.Basic.Flight;

namespace Airlines.UnitTest;
public class GraphTests
{
    [Fact]
    public void AddEdge_TwoCorrectAirports_ShouldReturnTrue()
    {
        // Arrange
        var graph = new Graph();
        var arival = "JFK";
        var departure = "DCA";

        // Act
        graph.AddEdge(arival, departure, 10, 5);

        // Assert
        Assert.True(graph._vertexList.Count > 0);
    }

    [Fact]
    public void AddEdge_TwoInvalidAirports_ShouldReturnFalse()
    {
        // Arrange
        var graph = new Graph();
        var arival = "JFK";
        var departure = "DCA";

        // Act
        graph.AddEdge(arival, departure, 10, 5);
        graph.AddEdge(arival, departure, 10, 5);

        // Assert
        Assert.True(graph._vertexList.Count == 1);
    }

    [Fact]
    public void CheckAirport_TwoCorrectAirports_ShouldReturnTrue()
    {
        // Arrange
        var graph = new Graph();
        var arival = "JFK";
        var departure = "DCA";
        List<Flight> flights = [];

        // Act
        graph.AddEdge(arival, departure, 10, 5);
        var result = RouteCheck.CheckRouteCommand(flights, graph, arival, departure).CheckAirports();

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void CheckAirport_TwoInvalidAirports_ShouldReturnFalse()
    {
        // Arrange
        var graph = new Graph();
        var arival = "JFK";
        var departure = "DCA";
        List<Flight> flights = [];

        // Act
        graph.AddEdge("ABC", "CBA", 10, 5);
        var result = RouteCheck.CheckRouteCommand(flights, graph, arival, departure).CheckAirports();

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void CheckAirport_TwoAirportsEmptyGraph_ShouldReturnFalse()
    {
        // Arrange
        var graph = new Graph();
        var arival = "JFK";
        var departure = "DCA";
        List<Flight> flights = [];

        // Act
        var result = RouteCheck.CheckRouteCommand(flights, graph, arival, departure).CheckAirports();

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void PathFinderTime_TwoAirportsGraph_ShouldReturnTrue()
    {
        // Arrange
        var graph = new Graph();
        var arival = "JFK";
        var departure = "DCA";
        List<Flight> flights = [];

        // Act
        graph.AddEdge(arival, departure, 10, 5);
        graph.AddEdge(departure, "ORD", 10, 5.5);
        var result = RouteSearch.SearchRouteCommand(flights, graph, arival, departure, "time").FindRouteByTime();

        // Assert
        Assert.True(result.Count > 0);
    }

    [Fact]
    public void PathFinderPrice_TwoAirportsGraph_ShouldReturnTrue()
    {
        // Arrange
        var graph = new Graph();
        var arival = "JFK";
        var departure = "DCA";
        List<Flight> flights = [];

        // Act
        graph.AddEdge(arival, departure, 10, 5);
        graph.AddEdge(departure, "ORD", 10, 5.5);
        var result = RouteSearch.SearchRouteCommand(flights, graph, arival, departure, "price").FindRouteByPrice();

        // Assert
        Assert.True(result.Count > 0);
    }

    [Fact]
    public void PathFinderShortest_TwoAirportsGraph_ShouldReturnTrue()
    {
        // Arrange
        var graph = new Graph();
        var arival = "JFK";
        var departure = "DCA";
        List<Flight> flights = [];

        // Act
        graph.AddEdge(arival, departure, 10, 5);
        graph.AddEdge(departure, "ORD", 10, 5.5);
        var result = RouteSearch.SearchRouteCommand(flights, graph, arival, departure, "shortest").FindShortestRoute();

        // Assert
        Assert.True(result.Count > 0);
    }
}
