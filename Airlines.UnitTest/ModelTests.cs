﻿using Airlines.Business;
using Airlines.Persistence.Aircrafts;

namespace Airlines.UnitTest;
public class ModelTests
{
    [Fact]
    public void PrivateAircraft_ToString_ReturnsCorrectString()
    {
        // Arrange
        var name = "XYZ Corporation";
        var seats = 6;
        var model = "Model XYZ123";
        var privateAircraft = PrivateAircraft.CreatePrivateAircraft(name, seats, model);
        var expected = "XYZ Corporation with 6 seats";
        // Act
        var result = privateAircraft.ToString();

        // Assert
        Assert.Equal(expected, result);
    }

    [Fact]
    public void PrivateAircraft_ToString_ReturnsIncorrectString()
    {
        // Arrange
        var name = "XYZ Corporation";
        var seats = 6;
        var model = "Model XYZ123";
        var privateAircraft = PrivateAircraft.CreatePrivateAircraft(name, seats, model);
        var expected = "XYZ Corporation with 5 seats";
        // Act
        var result = privateAircraft.ToString();

        // Assert
        Assert.NotEqual(expected, result);
    }

    [Fact]
    public void CargoAircraft_ToString_ReturnsCorrectString()
    {
        // Arrange
        var name = "XYZ Corporation ";
        var weight = 6.0;
        var volume = 10.0;
        var model = "Model XYZ123";
        var cargoAircraft = CargoAircraft.CreateCargoAircraft(name, weight, volume, model);
        var expected = "XYZ Corporation  6 weight and 10 volume";
        // Act
        var result = cargoAircraft.ToString();

        // Assert
        Assert.Equal(expected, result);
    }


    [Fact]
    public void PassengerAircraft_ToString_ReturnsCorrectString()
    {
        // Arrange
        var name = "XYZ Corporation";
        var weight = 6.0;
        var volume = 10.0;
        var seats = 10;
        var model = "Model XYZ123";
        var cargoAircraft = PassengerAircraft.CreatePassengerAircraft(name, weight, volume, seats, model);
        var expected = "XYZ Corporation with 10 seats, 6 weight and 10 volume";
        // Act
        var result = cargoAircraft.ToString();

        // Assert
        Assert.Equal(expected, result);
    }

    [Fact]
    public void GetData_ShouldReturn()
    {
        var data = new GetData();

        Assert.True(data.listOfObjects.Count > 0);
    }
}
