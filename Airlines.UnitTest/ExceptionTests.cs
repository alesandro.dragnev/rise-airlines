﻿using Airlines.Persistence.Basic.Exceptions;

namespace Airlines.UnitTest;
public class ExceptionTests
{
    [Fact]
    public void InvalidAirline_WithMessage_SetsMessageCorrectly()
    {
        // Arrange
        string errorMessage = "Invalid airline name";

        // Act
        var exception = new InvalidAirlineNameException(errorMessage);

        // Assert
        Assert.Equal(errorMessage, exception.Message);
    }

    [Fact]
    public void InvalidAirport_WithMessage_SetsMessageCorrectly()
    {
        // Arrange
        string errorMessage = "Invalid airport";

        // Act
        var exception = new InvalidAirportException(errorMessage);

        // Assert
        Assert.Equal(errorMessage, exception.Message);
    }

    [Fact]
    public void MissingFile_WithMessage_SetsMessageCorrectly()
    {
        // Arrange
        string errorMessage = "Missing File";

        // Act
        var exception = new MissingFileException(errorMessage);

        // Assert
        Assert.Equal(errorMessage, exception.Message);
    }

    [Fact]
    public void InvalidFlightCode_WithMessage_SetsMessageCorrectly()
    {
        // Arrange
        string errorMessage = "Invalid flight code";

        // Act
        var exception = new InvalidFlightCodeException(errorMessage);

        // Assert
        Assert.Equal(errorMessage, exception.Message);
    }
}
