﻿using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.Repositories;
using Moq;
using Airlines.Persistence.Basic.Models;
using Moq.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Airlines.UnitTest;
public class AirportMockTest
{
    private readonly Mock<RISEAirlinesContext> _contextMock;
    private readonly AirportRepository _airportRepository;

    public AirportMockTest()
    {
        _contextMock = new Mock<RISEAirlinesContext>();
        _airportRepository = new AirportRepository(_contextMock.Object);
    }

    [Fact]
    public void GetAllAirports_ShouldReturnTrue()
    {

        List<Airport> airports = [];
        Airport airport1 = new Airport
        {
            Name = "Dubai International Airport",
            Country = "United Arab Emirates",
            City = "Dubai",
            Code = "DXB",
            Runways_count = 2,
            Founded = new DateOnly(1960, 9, 30)
        };
        Airport airport2 = new Airport
        {
            Name = "Hartsfield-Jackson Atlanta International Airport",
            Country = "United States",
            City = "Atlanta",
            Code = "ATL",
            Runways_count = 5,
            Founded = new DateOnly(1926, 10, 15)
        };

        airports.Add(airport1);
        airports.Add(airport2);

        List<Airport> expectedAirports = airports;

        var returnsResult = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);

        List<Airport> allAirports = _airportRepository.GetAllAirports();

        Assert.Equal(expectedAirports, allAirports);
    }

    [Fact]
    public void GetAirportByCode_ShouldReturnTrue()
    {
        var code = "JFK";
        List<Airport> airports = [];
        Airport expectedAirport = new Airport
        {
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        airports.Add(expectedAirport);

        var returnsResult = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);


        var airport = _airportRepository.GetAirportByCode(code);

        Assert.Contains(airport, airports);
    }

    [Fact]
    public void GetAirportById_ShouldReturnTrue()
    {
        var id = 1;
        List<Airport> airports = [];
        Airport expectedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        airports.Add(expectedAirport);

        var returnsResult = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);


        var airport = _airportRepository.GetAirportById(id);

        Assert.Equal(airports[0].Code, airport?.Code);
    }

    [Fact]
    public void GetAirportsCount_ShouldReturnTrue()
    {
        List<Airport> airports = [];

        var returnsResult = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);


        var airport = _airportRepository.GetAirportCount();

        Assert.Equal(airports.Count, airport);
    }

    [Fact]
    public void GetAirportByCity_ShouldReturnTrue()
    {
        var city = "New York City";
        List<Airport> airports = [];
        Airport expectedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        airports.Add(expectedAirport);

        var returnsResult = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);


        var airport = _airportRepository.GetAirportsByCity(city);

        Assert.Equal(airports[0].City, airport[0].City);
    }

    [Fact]
    public void GetAirportByCountry_ShouldReturnTrue()
    {
        var country = "United States";
        List<Airport> airports = [];
        Airport expectedAirport = new Airport

        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        airports.Add(expectedAirport);

        var returnsResult = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);


        var airport = _airportRepository.GetAirportsByCountry(country);

        Assert.Equal(airports[0].Country, airport[0].Country);
    }

    [Fact]
    public void AddNewAirport_ShouldReturnTrue()
    {
        List<Airport> airports = [];
        Airport existedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };
        Airport newAirport = new Airport
        {
            Name = "Hartsfield-Jackson Atlanta International Airport",
            Country = "United States",
            City = "Atlanta",
            Code = "ATL",
            Runways_count = 5,
            Founded = new DateOnly(1926, 10, 15)
        };

        airports.Add(existedAirport);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        var returnsResult = _contextMock.Setup(context => context.Airports.Add(It.IsAny<Airport>()))
                .Callback((Airport entity) =>
                {
                    _ = _airportRepository.AddNewAirport(entity);
                });



        var airport = _airportRepository.AddNewAirport(newAirport);


        Assert.True(airport);
    }

    [Fact]
    public void AddNewAirport_ShouldReturnFalse()
    {
        List<Airport> airports = [];
        Airport existedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        airports.Add(existedAirport);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Airports.Add(It.IsAny<Airport>()))
        .Callback((Airport entity) =>
        {
            _ = _airportRepository.AddNewAirport(entity);
        });


        var airport = _airportRepository.AddNewAirport(existedAirport);

        Assert.False(airport);
    }

    [Fact]
    public void UpdateAirport_ShouldReturnTrue()
    {
        List<Airport> airports = [];

        Airport existedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        Airport updatedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 3,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        airports.Add(existedAirport);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Airports.Update(It.IsAny<Airport>()))
                    .Callback((Airport entity) =>
                    {
                        _ = _airportRepository.UpdateAirport(entity);
                    });


        var result = _airportRepository.UpdateAirport(updatedAirport);


        Assert.True(result);
    }

    [Fact]
    public void DeleteAirportByCode_ShouldReturnTrue()
    {
        var code = "JFK";
        List<Airport> airports = [];
        Airport existedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        airports.Add(existedAirport);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        var result = _contextMock.Setup(context => context.Airports.Remove(It.IsAny<Airport>()))
            .Callback((Airport entity) =>
            {
                _ = _airportRepository.DeleteAirportByCode(entity.Code);
            });


        var airport = _airportRepository.DeleteAirportByCode(code);

        Assert.True(airport);
    }

    [Fact]
    public void DeleteAirportByCode_ShouldReturnFalse()
    {
        var code = "NFS";
        List<Airport> airports = [];
        Airport existedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        existedAirport.Code = "SOF";

        airports.Add(existedAirport);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Airports.Remove(It.IsAny<Airport>()))
            .Callback((Airport entity) =>
            {
                _ = _airportRepository.DeleteAirportByCode(entity.Code);
            });


        var airport = _airportRepository.DeleteAirportByCode(code);

        Assert.False(airport);
    }

    [Fact]
    public void DeleteAirportById_ShouldReturnTrue()
    {
        var id = 1;
        List<Airport> airports = [];
        Airport existedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };

        airports.Add(existedAirport);

        _ = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Airports.Remove(It.IsAny<Airport>()))
            .Callback((Airport entity) =>
            {
                _ = _airportRepository.DeleteAirportById(entity.ID);
            });

        var airport = _airportRepository.DeleteAirportById(id);

        Assert.True(airport);
    }

    [Fact]
    public void DeleteAirportById_ShouldReturnFalse()
    {
        var id = 2;
        List<Airport> airports = [];
        Airport existedAirport = new Airport
        {
            ID = 1,
            Name = "John F. Kennedy International Airport",
            Country = "United States",
            City = "New York City",
            Code = "JFK",
            Runways_count = 4,
            Founded = new DateOnly(1948, 7, 01),
            FlightArrivals = [],
            FlightDepartures = []
        };


        var returnsResult = _contextMock.Setup(context => context.Airports).ReturnsDbSet(airports);
        _ = _contextMock.Setup(context => context.Airports.Remove(It.IsAny<Airport>()))
            .Callback((Airport entity) =>
            {
                _ = _airportRepository.DeleteAirportById(entity.ID);
            });


        var airport = _airportRepository.DeleteAirportById(id);

        Assert.False(airport);
    }
}
