﻿using Airlines.Persistence;
using Airlines.Business.Commands.Route;
using Airlines.OLD.Persistence.Basic.Flight;

namespace Airlines.UnitTest;
public class TreeTest
{
    [Fact]
    public void Insert_NewFlightID_ShouldReturnTrue()
    {
        // Arrange
        var tree = new Tree();
        var flightId = "ABC123";

        // Act
        tree.Insert(flightId);

        // Assert
        Assert.Equal(flightId, tree?.Root?.Data);
    }

    [Fact]
    public void Insert_NewFlightID_ShouldReturnFalse()
    {
        // Arrange
        var tree = new Tree();
        var flightId = "------";

        // Act
        tree.Insert(flightId);

        // Assert
        Assert.NotEqual(flightId, tree?.Root?.Data);
    }

    [Fact]
    public void AlreadyInserted_NewFlightID_ShouldReturnFalse()
    {
        // Arrange
        var tree = new Tree();
        var flightId = "ABC123";
        var newFlightId = "ABC124";

        // Act
        tree.Insert(flightId);
        var result = tree.AlreadyInserted(newFlightId);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Insert_TwoFlightID_ShouldReturnTrue()
    {
        // Arrange
        var tree = new Tree();
        var flightId = "ABC123";
        var flightId2 = "ABC124";

        // Act
        tree.Insert(flightId);
        tree.Insert(flightId2);
        var result = tree.AlreadyInserted(flightId2);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void DoesFlightExist_ValidFlightID_ShouldReturnTrue()
    {
        // Arrange
        List<Flight> flights = [
            Flight.CreateFlight("FL123","JFK","LAX","Boing", 10, 5),
            Flight.CreateFlight("FL456","LAX","ORD","Boing", 10, 5)
        ];

        var flightId = "FL123";

        // Act
        var result = Tree.DoesFlightExist(flights, flightId);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void DoesFlightExist_InvalidFlightID_ShouldReturnFalse()
    {
        // Arrange
        List<Flight> flights = [
            Flight.CreateFlight("FL123","JFK","LAX","Boing", 10, 5),
            Flight.CreateFlight("FL456","LAX","ORD","Boing",10, 5)
        ];

        var flightId = "FL124";

        // Act
        var result = Tree.DoesFlightExist(flights, flightId);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void GetArrivalAirport_ValidFlightID_ShouldReturnTrue()
    {
        // Arrange
        List<Flight> flights = [
            Flight.CreateFlight("FL123","JFK","LAX","Boing",10, 5),
            Flight.CreateFlight("FL456","LAX","ORD","Boing",10, 5)
        ];

        var flightId = "FL123";
        var expected = "JFK";

        // Act
        var result = Tree.GetArrivalAirport(flights, flightId);

        // Assert
        Assert.Equal(expected, result);
    }

    [Fact]
    public void GetArrivalAirport_InvalidFlightID_ShouldReturnFalse()
    {
        // Arrange
        List<Flight> flights = [
            Flight.CreateFlight("FL123","JFK","LAX","Boing",10, 5),
            Flight.CreateFlight("FL456","LAX","ORD","Boing",10, 5)
        ];

        var flightId = "FL124";
        var expected = "";

        // Act
        var result = Tree.GetArrivalAirport(flights, flightId);

        // Assert
        Assert.Equal(expected, result);
    }

    [Fact]
    public void GetDepartureAirport_ValidFlightID_ShouldReturnTrue()
    {
        // Arrange
        List<Flight> flights = [
            Flight.CreateFlight("FL123","JFK","LAX","Boing", 10, 5),
            Flight.CreateFlight("FL456","LAX","ORD","Boing", 10, 5)
        ];

        var flightId = "FL123";
        var expected = "LAX";

        // Act
        var result = Tree.GetDepartureAirport(flights, flightId);

        // Assert
        Assert.Equal(expected, result);
    }

    [Fact]
    public void GetDepartureAirport_InvalidFlightID_ShouldReturnFalse()
    {
        // Arrange
        List<Flight> flights = [
            Flight.CreateFlight("FL123","JFK","LAX","Boing",10, 5),
            Flight.CreateFlight("FL456","LAX","ORD","Boing", 10, 5)
        ];

        var flightId = "FL124";
        var expected = "";

        // Act
        var result = Tree.GetDepartureAirport(flights, flightId);

        // Assert
        Assert.Equal(expected, result);
    }

    [Fact]
    public void GetNode_ValidFlightID_ShouldReturnTrue()
    {
        // Arrange
        var tree = new Tree();
        var flightId = "FL124";
        var getFlightId = "FL123";
        Node expected = new Node(getFlightId);

        // Act
        tree.Insert(flightId);
        tree.Insert(getFlightId);

        var result = tree.GetNode(getFlightId);

        // Assert
        Assert.Equal(expected.Data, result?.Data);
    }

    [Fact]
    public void GetNode_InvalidFlightID_ShouldReturnFalse()
    {
        // Arrange

        var tree = new Tree();
        var flightId = "FL124";
        var getFlightId = "FL125";

        // Act
        tree.Insert(flightId);

        var result = tree.GetNode(getFlightId);

        // Assert
        Assert.Null(result);
    }

    [Fact]
    public void RouteFind_ValidFlightID_ShouldReturnTrue()
    {
        // Arrange
        List<Flight> flights = [
            Flight.CreateFlight("FL123","JFK","LAX","Boing",10, 5),
            Flight.CreateFlight("FL456","LAX","ORD","Boing", 10, 5)
            ];
        var tree = new Tree();
        var flightId = "FL124";
        var getFlightId = "FL125";

        // Act
        tree.Insert(flightId);
        tree.Insert(getFlightId);

        var result = RouteFind.FindRouteCommand(flights, tree, flightId).FindPathToNode(flightId);

        // Assert
        Assert.True(result[0].Data == flightId);
    }
}
