﻿using Airlines.Business.Commands.Exist;
using Airlines.Business.Commands.Sort;
using Airlines.Business.Commands.List;
using Airlines.Business.Commands.Search;
using Airlines.Business.Commands.Route;
using Airlines.Business.Commands.Batch;
using Airlines.OLD.Persistence.Basic.Flight;
using Airlines.Persistence.Basic.Airlines;
using Airlines.Persistence.Basic.Airports;
using FileReaders;


namespace Airlines.UnitTest;
public class CommandTests
{
    [Fact]
    public void ExistingAirlines_CorrectInput_ShouldReturnTrue()
    {
        var airlineDict = new Dictionary<string, List<object>>
        {
            { "airlines", new List<object>
                {
                    Airline.CreateAirline("Delta"),
                    Airline.CreateAirline("Wizz"),
                    Airline.CreateAirline("Ryan")
                }
            }
        };
        var key = "Delta";
        Exist result;
        using (var consoleOutput = new StringWriter())
        {
            result = Exist.ExistCommand(airlineDict, key);
        }
        Assert.True(result.ExistingAirline());
    }

    [Fact]
    public void SortCommand_SortsAirportListAscending()
    {
        // Arrange
        var airports = new List<object>
            {
                Airport.CreateAirport( "ABC","Airport A","varna","bulgaria" ),
                Airport.CreateAirport( "CBA","Airport B","varna","bulgaria" ),
                Airport.CreateAirport( "BCA","Airport C","varna","bulgaria" ),
            };
        var dataDict = new Dictionary<string, List<object>>
            {
                { "airports", airports }
            };

        // Act
        Sort.SortCommand(dataDict, "airports", "ascending").Execute();

        // Assert
        Assert.Equal("ABC", ((Airport)dataDict["airports"][0]).ID);
    }

    [Fact]
    public void SortCommand_SortsAirportListDescending()
    {
        // Arrange
        var airports = new List<object>
            {
                Airport.CreateAirport( "ABC","Airport A","varna","bulgaria" ),
                Airport.CreateAirport( "CBA","Airport B","varna","bulgaria" ),
                Airport.CreateAirport( "BCA","Airport C","varna","bulgaria" ),
            };
        var dataDict = new Dictionary<string, List<object>>
            {
                { "airports", airports }
            };

        // Act

        Sort.SortCommand(dataDict, "airports", "descending").Execute();

        // Assert
        Assert.Equal("CBA", ((Airport)dataDict["airports"][0]).ID);
    }

    [Fact]
    public void SortCommand_SortsAirlinesListDescending()
    {
        // Arrange
        var airlines = new List<object>
            {
                Airline.CreateAirline( "ABC" ),
                Airline.CreateAirline("CBA"),
                Airline.CreateAirline("BCA"),
            };
        var dataDict = new Dictionary<string, List<object>>
            {
                { "airlines", airlines }
            };

        // Act

        Sort.SortCommand(dataDict, "airlines", "descending").Execute();

        // Assert
        Assert.Equal("CBA", ((Airline)dataDict["airlines"][0]).Name);
    }

    [Fact]
    public void SortCommand_SortsAirlinesListAscending()
    {
        // Arrange
        var airlines = new List<object>
            {
                Airline.CreateAirline( "ABC" ),
                Airline.CreateAirline("CBA"),
                Airline.CreateAirline("BCA"),
            };
        var dataDict = new Dictionary<string, List<object>>
            {
                { "airlines", airlines }
            };

        // Act

        Sort.SortCommand(dataDict, "airlines", "ascending").Execute();

        // Assert
        Assert.Equal("BCA", ((Airline)dataDict["airlines"][1]).Name);
    }

    [Fact]
    public void ListAirport_ByCity_ReturnsMatchingAirports()
    {
        // Arrange
        var airports = new List<Airport>
    {
        Airport.CreateAirport("1", "Airport1", "New York", "USA"),
        Airport.CreateAirport("2", "Airport2", "London", "UK"),
        Airport.CreateAirport("3", "Airport3", "Paris", "France")
    };
        var searchTerm = "london";
        var searchKey = "city";

        // Act

        var result = List.ListCommand(airports, searchTerm, searchKey).ListAirport();


        // Assert
        Assert.Equal("Airport2", result);
    }

    [Fact]
    public void ListAirport_ByCountry_ReturnsMatchingAirports()
    {
        // Arrange
        var airports = new List<Airport>
    {
        Airport.CreateAirport("1", "Airport1", "New York", "USA"),
        Airport.CreateAirport("2", "Airport2", "London", "UK"),
        Airport.CreateAirport("3", "Airport3", "Paris", "France")
    };
        var searchTerm = "UK";
        var searchKey = "country";

        // Act

        var result = List.ListCommand(airports, searchTerm, searchKey).ListAirport();


        // Assert
        Assert.Equal("Airport2", result);
    }

    [Fact]
    public void ListAirport_ByInvalidInput_ReturnsTrue()
    {
        // Arrange
        var airports = new List<Airport>
    {
        Airport.CreateAirport("1", "Airport1", "New York", "USA"),
        Airport.CreateAirport("2", "Airport2", "London", "UK"),
        Airport.CreateAirport("3", "Airport3", "Paris", "France")
    };
        var searchTerm = "UK";
        var searchKey = "state";

        // Act

        var result = List.ListCommand(airports, searchTerm, searchKey).ListAirport();


        // Assert
        Assert.Equal("Not existing list!", result);
    }

    [Fact]
    public void Search_FoundInAirport_ShouldReturnTrue()
    {
        // Arrange
        Dictionary<string, List<object>> airportData = new Dictionary<string, List<object>>
    {
        { "Airport1", new List<object> { "Airline1", "Flight1" } },
        { "Airport2", new List<object> { "Airline2", "Flight2" } },
        { "Airport3", new List<object> { "Airline3", "Flight3" } }
    };

        var searchString = "Airline1";
        // Act
        var result = Search.SearchCommand(airportData, searchString).SearchInput();

        // Assert
        Assert.Equal($"Airport1", result);
    }


    [Fact]
    public void Search_FoundInAirport_ShouldReturnFalse()
    {
        // Arrange
        Dictionary<string, List<object>> airportData = new Dictionary<string, List<object>>
    {
        { "Airport1", new List<object> { "Airline1", "Flight1" } },
        { "Airport2", new List<object> { "Airline2", "Flight2" } },
        { "Airport3", new List<object> { "Airline3", "Flight3" } }
    };

        // Act
        string searchString = "Airline6";

        var result = Search.SearchCommand(airportData, searchString).SearchInput();

        // Assert
        Assert.Equal(string.Empty, result);
    }

    [Fact]
    public void RouteAdd_FoundInAirport_ShouldReturnFalse()
    {
        // Arrange
        var flights = new List<Flight> {
        Flight.CreateFlight("1", "JFK", "KSA", "Boing", 10, 5),
        Flight.CreateFlight("2", "KSA", "AAA", "Boing", 10, 5),
        Flight.CreateFlight("3", "AAA", "BCA", "Boing", 10, 5)
        };

        // Act
        string flightToADD = "2";

        RouteAddCommand.AddRouteCommand(flightToADD, flights).AddRoute();

        // Assert
        Assert.False(Route.routeManagement.Count > 0);
    }

    [Fact]
    public void RouteNew_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight> {
        Flight.CreateFlight("1", "JFK", "KSA", "Boing", 10, 5),
        Flight.CreateFlight("2", "KSA", "AAA", "Boing", 10, 5),
        Flight.CreateFlight("3", "AAA", "BCA", "Boing", 10, 5)
        };

        // Act
        string flightToADD = "2";
        RouteAddCommand.AddRouteCommand(flightToADD, flights).AddRoute();
        RouteNewCommand.NewRouteCommand(flights).Execute();

        // Assert
        Assert.True(Route.routeManagement.Count == 0);
    }

    /* [Fact]
     public void RouteRemove_ShouldReturnTrue()
     {
         // Arrange
         var flights = new List<Flight> {
         Flight.CreateFlight("1", "JFK", "KSA", "Boing", 10, 5),
         Flight.CreateFlight("2", "KSA", "AAA", "Boing", 10, 5),
         Flight.CreateFlight("3", "AAA", "BCA", "Boing", 10, 5)
         };

         // Act
         string flightToADD = "2";
         RouteAddCommand.AddRouteCommand(flightToADD, flights).AddRoute();
         RouteRemoveCommand.RemoveRouteCommand(flights).Execute();

         // Assert
         Assert.True(Route.routeManagement.Count == 0);
     }*/

    [Fact]
    public void Batch_AddCommand_ShouldReturnTrue()
    {

        // Act
        string command = "some command";
        Batch.AddCommand(command);

        // Assert
        Assert.True(Batch.BatchQueue.Count > 0);
    }

    [Fact]
    public void Batch_CancelCommand_ShouldReturnTrue()
    {
        _ = new Batch();

        // Act
        Batch.CancelBatch();

        // Assert
        Assert.True(Batch.BatchQueue.Count == 0);
    }
}
