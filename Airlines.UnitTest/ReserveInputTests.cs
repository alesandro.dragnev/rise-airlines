﻿using Airlines.Persistence.Aircrafts;

using Airlines.Business.Commands.Reserve;
using Airlines.OLD.Persistence.Basic.Flight;
namespace Airlines.UnitTest;

public class ReserveInputTests
{

    [Fact]
    public void BaggageCalculation_DoubleInputs_ReturnsTrue()
    {
        // Arrange
        var firstDouble = 3.0;
        var secondDouble = 10.5;


        // Act
        var result = Reserve.BaggageCalculation(firstDouble, secondDouble);

        // Assert
        Assert.True(result);
    }
    [Fact]
    public void BaggageCalculation_WrongInputs_ReturnsFalse()
    {
        // Arrange
        var firstDouble = 30.0;
        var secondDouble = 100.5;


        // Act
        var result = Reserve.BaggageCalculation(firstDouble, secondDouble);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void IsAircraftCargoPossible_FlightsCargoObjects_ReturnsTrue()
    {
        // Arrange
        List<Flight> flights = [
                Flight.CreateFlight("FL123","JFK","LAX","Boing",10, 5),
                Flight.CreateFlight("FL456","LAX","ORD","Boing",10, 5)
        ];

        List<CargoAircraft> cargoAircrafts = [
        CargoAircraft.CreateCargoAircraft("Boing", 10000, 500, "cargo"),
        CargoAircraft.CreateCargoAircraft("Airbus", 15000, 600, "cargo"),
        ];

        var flightID = "FL123";
        var aircraftType = "cargo";
        var cargoWeight = 8000.0;
        var cargoVolume = 400.0;

        // Act
        ReserveCargo result;
        using (var consoleOutput = new StringWriter())
        {
            result = ReserveCargo.ReserveCargoCommand(flights, flightID, cargoAircrafts, cargoWeight, cargoVolume, aircraftType);
        }

        // Assert
        Assert.True(result.IsAircraftCargoPossible());

    }

    [Fact]
    public void IsAircraftCargoPossible_FlightsCargoObjects_ReturnsFalse()
    {
        // Arrange
        List<Flight> flights = [
                Flight.CreateFlight("FL123","JFK","LAX","Boing",10, 5),

                Flight.CreateFlight("FL456","LAX","ORD","Boing",10, 5),

                Flight.CreateFlight("FL456","LAX","ORD","Boing",10, 5)

        ];

        List<CargoAircraft> cargoAircrafts = [
            CargoAircraft.CreateCargoAircraft("Airbus", 10000, 500, "cargo"),

            CargoAircraft.CreateCargoAircraft("Airbus", 15000, 600, "cargo"),
            ];



        var flightID = "FL123";
        var aircraftType = "cargo";
        var cargoWeight = 8000.0;
        var cargoVolume = 400.0;

        // Act
        var result = ReserveCargo.ReserveCargoCommand(flights, flightID, cargoAircrafts, cargoWeight, cargoVolume, aircraftType);

        // Assert
        Assert.False(result.IsAircraftCargoPossible());
    }
    [Fact]
    public void IsAircraftPassengerPossible_WrongFlightsCargoObjects_ReturnsFalse()
    {
        // Arrange
        List<Flight> flights = [
                Flight.CreateFlight("FL123","JFK","LAX","Boing", 10, 5),
                Flight.CreateFlight("FL456","LAX","ORD","Boing",10, 5)
        ];

        List<PassengerAircraft> passengerAircrafts = [
         PassengerAircraft.CreatePassengerAircraft("Boing", 10000, 500, 100, "cargo"),
         PassengerAircraft.CreatePassengerAircraft("Airbus", 15000, 600, 100, "cargo"),
        ];

        var flightID = "FL123";
        var aircraftType = "passenger";
        var smallBaggageCount = 12000.0;
        var largeBaggageCount = 15000.0;
        var seats = 10;

        // Act

        var result = ReserveTicket.ReserveTicketCommand(flights, flightID, passengerAircrafts, smallBaggageCount, largeBaggageCount, seats, aircraftType);

        // Assert
        Assert.False(result.IsAircraftPassengerPossible());

    }

    [Fact]
    public void IsPassengerTicketPossible_FlightsCargoObjects_ReturnsTrue()
    {
        // Arrange
        List<Flight> flights = [
                Flight.CreateFlight("FL123","JFK","LAX","Boing",10, 5),

                 Flight.CreateFlight("FL456","LAX","ORD","Boing",10, 5),

                Flight.CreateFlight("FL456","LAX","ORD","Boing",10, 5)

        ];

        List<PassengerAircraft> passengerAircrafts = [
         PassengerAircraft.CreatePassengerAircraft("Boing", 10000, 500, 100, "passenger"),

          PassengerAircraft.CreatePassengerAircraft("Airbus", 15000, 600, 100, "cargo"),
         ];

        var flightID = "FL123";
        var aircraftType = "passenger";
        var smallBaggageCount = 12;
        var largeBaggageCount = 15;
        var seats = 10;

        // Act
        ReserveTicket result;
        using (var consoleOutput = new StringWriter())
        {
            result = ReserveTicket.ReserveTicketCommand(flights, flightID, passengerAircrafts, seats, smallBaggageCount, largeBaggageCount, aircraftType);
        }

        // Assert
        Assert.True(result.IsAircraftPassengerPossible());
    }
}
