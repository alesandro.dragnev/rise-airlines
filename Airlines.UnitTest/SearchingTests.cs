﻿using System.Diagnostics;
using Xunit.Abstractions;
using Airlines.Business;

namespace Airlines.UnitTest;
public class SearchingTests(ITestOutputHelper output)
{

    private readonly ITestOutputHelper _output = output;

    // Binary Search Tests
    [Fact]

    public void BinarySearch_SortedArrayIncludingKey_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["aaaa", "abc", "aft", "asdr", "cba", "zva"];
        var key = "aaaa";

        // Act
        var validationResult = SearchingExtension.BinarySearch(array, key);

        // Assert
        Assert.Equal(validationResult, key);
    }

    [Fact]
    public void BinarySearch_EmptyArrayIncludingKey_ShouldReturnTrue()
    {
        // Arrange
        string[] array = [];
        var key = "aaaa";

        // Act
        var validationResult = SearchingExtension.BinarySearch(array, key);

        // Assert
        Assert.NotEqual(validationResult, key);
    }

    [Fact]
    public void BinarySearch_SortedArrayNotIncludingKey_ShouldReturnFalse()
    {
        // Arrange
        string[] array = ["aaaa", "abc", "aft", "asdr", "cba", "zva"];
        var key = "aaaac";

        // Act
        var validationResult = SearchingExtension.BinarySearch(array, key);

        // Assert
        Assert.NotEqual(validationResult, key);
    }

    [Fact]
    public void BinarySearch_UnsortedArrayIncludingKey_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["abc", "cba", "zva", "aft", "asdr", "aaaa"];
        var key = "aaaa";

        // Act
        var validationResult = SearchingExtension.BinarySearch(array, key);

        // Assert
        Assert.NotEqual(validationResult, key);
    }

    [Fact]
    public void BinarySearch_SortedDuplicatedValuesArrayIncludingKey_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["abc", "cba", "zva", "aaaa", "aft", "asdr", "aaaa"];
        Array.Sort(array);
        var key = "aaaa";

        // Act
        var validationResult = SearchingExtension.BinarySearch(array, key);

        // Assert
        Assert.Equal(validationResult, key);
    }


    // Linear Search Tests
    [Fact]
    public void LinearSearch_SortedArrayNotIncludingKey_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["aaaa", "abc", "aft", "asdr", "cba", "zva"];
        var key = "aaaa";

        // Act
        var validationResult = SearchingExtension.LinearSearch(array, key);

        // Assert
        Assert.Equal(validationResult, key);
    }

    [Fact]
    public void LinearSearch_SortedArrayNotIncludingKey_ShouldReturnFalse()
    {
        // Arrange
        string[] array = ["aaaa", "abc", "aft", "asdr", "cba", "zva"];
        var key = "aaaac";

        // Act
        var validationResult = SearchingExtension.LinearSearch(array, key);

        // Assert
        Assert.NotEqual(validationResult, key);
    }

    [Fact]
    public void LinearSearch_UnsortedArrayIncludingKey_ShouldReturnFalse()
    {
        // Arrange
        string[] array = ["abc", "cba", "zva", "aft", "asdr", "aaaa"];
        var key = "aaaa";

        // Act
        var validationResult = SearchingExtension.LinearSearch(array, key);

        // Assert
        Assert.Equal(validationResult, key);
    }

    [Fact]
    public void LinearSearch_UnsortedDuplicatedValuesArrayIncludingKey_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["abc", "cba", "zva", "aaaa", "aft", "asdr", "aaaa"];
        var key = "aaaa";

        // Act
        var validationResult = SearchingExtension.BinarySearch(array, key);

        // Assert
        Assert.Equal(validationResult, key);
    }

    [Fact]
    public void LinearSearch_EmptyArrayIncludingKey_ShouldReturnTrue()
    {
        // Arrange
        string[] array = [];
        var key = "aaaa";

        // Act
        var validationResult = SearchingExtension.BinarySearch(array, key);

        // Assert
        Assert.NotEqual(validationResult, key);
    }

    [Fact]
    public void LinearSearch_UnsortedArrayNotIncludingKey_ShouldReturnFalse()
    {
        // Arrange
        string[] array = ["abc", "cba", "zva", "aft", "asdr", "aaaa"];
        var key = "aaaac";

        // Act
        var validationResult = SearchingExtension.LinearSearch(array, key);

        // Assert
        Assert.NotEqual(validationResult, key);
    }

    [Fact]
    public void LinearSearch_BinarySearch_Comparison_SmallSortedArrayIncludingKey_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["aaaa", "abc", "aft", "asdr", "cba", "zva", "xyz", "def", "ghi", "jkl", "mno", "pqr"];
        Array.Sort(array);
        var key = "def";

        // Act
        var stopwatchLinearSearch = Stopwatch.StartNew();
        _ = SearchingExtension.LinearSearch(array, key);
        stopwatchLinearSearch.Stop();

        var stopwatchBinarySearch = Stopwatch.StartNew();
        _ = SearchingExtension.BinarySearch(array, key);
        stopwatchBinarySearch.Stop();

        // Assert
        Assert.False(stopwatchBinarySearch.ElapsedMilliseconds < stopwatchLinearSearch.ElapsedMilliseconds);
    }

    [Fact]
    public void LinearSearch_BinarySearch_Comparison_BigSortedArrayIncludingKey_ShouldReturnTrue()
    {
        // Arrange
        var array = RandomGeneratingFunctions.GenerateRandomStringArray(10000, 3);
        Array.Sort(array);
        var key = array[400];

        // Act
        var stopwatchLinearSearch = Stopwatch.StartNew();
        _ = SearchingExtension.LinearSearch(array, key);
        stopwatchLinearSearch.Stop();

        var stopwatchBinarySearch = Stopwatch.StartNew();
        _ = SearchingExtension.BinarySearch(array, key);
        stopwatchBinarySearch.Stop();

        _output.WriteLine($"Binary Search: {stopwatchBinarySearch.Elapsed}");
        _output.WriteLine($"Linear Search: {stopwatchLinearSearch.Elapsed}");
        // Assert
        Assert.True(stopwatchLinearSearch.Elapsed > stopwatchBinarySearch.Elapsed);
    }
}
